"""
Solving the five largest and smallest eigenvalues and the corresponding
eigenvectors of the stationary state Schrödinger equation by using the 
power method and the inverse power method. Scipy's eigsh solver is also 
used for comparison to test the accuracy. Both results are
visualized.
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def power_method(A,tol=1e-15):
    """
    Calculating the largest eigenvalue and the corresponding eigenvector 
    of matrix A using the power method.

    A : matrix
    tol : float, optional, tolerance for the error of the result
    """
    l=A.shape[0]

    # creating a randomized initial guess vector
    v=np.matrix(np.random.rand(l)).T
    v=v/np.linalg.norm(v)
    error=1000
    eig_value=1000
    eig_value_old=1000
    
    # repeating the power method until the wanted accuracy is achieved
    while error>tol: 
      v=np.dot(A,v)
      eig_value=np.linalg.norm(v)
      v=v/eig_value
      error=abs(eig_value-eig_value_old)
      eig_value_old=eig_value     

    # turning the eigenvector into an array so that simpson integral
    # can be used on it later
    eig_vector=np.array(v.T)[0]
    return eig_value, eig_vector

def inv_power_method(A,rm,tol=1e-15):
    """
    Calculating the smallest eigenvalue and the corresponding eigenvector 
    of matrix A using the inverse power method. By finding the largest
    eigenvalue for the inverse of A, we can get the smallest eigenvalue for A.

    A : matrix
    rm : float, guess for the possible eigenvalue
    tol : float, optional, tolerance for the error of the result
    """
    #A = A.toarray()
    l=A.shape[0]
    I=np.identity(l)
    M=np.linalg.inv(A-rm*I)

    # creating a randomized initial guess vector
    v=np.matrix(np.random.rand(l)).T
    v=v/np.linalg.norm(v)
    error=1000
    eig_value=1000
    eig_value_old=1000
    
    # repeating the power method until the wanted accuracy is achieved
    while error>tol:
      v=np.dot(M,v)
      eig_value=np.linalg.norm(v)
      v=v/eig_value
      error=abs(eig_value-eig_value_old)
      eig_value_old=eig_value     

    # turning the eigenvector into an array so that simpson integral
    # can be used on it later
    eig_vector=np.array(v.T)[0]
    # calculating the eigenvalue for A from the inverse's eigenvalue
    eig_value=rm+1/eig_value
    return eig_value, eig_vector

def main():
    
    # initializing a linear grid
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    # forming the matrix form of the Hamilton operator in the stationary
    # state Schrödinger equation
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    # turning the sparse matrix into a numpy matrix
    A = H0.todense()
    # calculating the interval where the eigenvalues are
    rm=np.max(sum(np.abs(H0.toarray())),0)
    
    # Scipy's estimates for the first five largest and smallest 
    # eigenvalues and eigenvectors
    eigs, evecs = sla.eigsh(H0, k=5, which='LA')
    eigs2, evecs2 = sla.eigsh(H0, k=5, which='SA')

    N=5 # number of calculated eigenvalues and vectors
    round_text=''
    A_tilde=1.0*A
    A_tilde2=1.0*A
    l=l2=0.0
    vec=vec2=np.zeros(A.shape[0])
    for i in range(1,N+1):   
        if i==2:
          round_text='Second'
        elif i==3:
          round_text='Third'
        elif i==4:
          round_text='Fourth'
        elif i==5:
          round_text='Fifth' 
        
        # calculating the largest eigenvalue and its eigenvector
        # using the power method
        if i==1:
          l,vec=power_method(A)
          print('Largest eig estimate:', l)
          print('Scipy eigsh largest eig:', eigs[N-i])
          # printing the mean absolute error to check the accuracy
          print('MAE for largest eig:',np.mean(np.abs(vec-evecs[:,-i])))
        
        # calculating the next largest eigenvalue and its eigenvector
        # by expanding the power method
        else:
          vec=np.matrix((vec)).T
          # setting the biggest eigenvalue to zero
          A_tilde=A_tilde-l*(vec.dot(vec.T)/((vec.T).dot(vec)))
          # calculating the largest eigvalue and eigvector for the newly formed
          # matrix
          l3,vec3=power_method(A_tilde)
          vec3=np.matrix((vec3)).T
          # eigvalue is the same as A's but eigvector needs to be changed
          vec3=vec3-(l/l3)*(((vec.T).dot(vec3)/(vec.T).dot(vec))[0,0]*(vec))
          vec=np.array(vec3.T)[0]
          l=1.0*l3
          print(round_text+' largest eig estimate:',l3)  
          print('Scipy eigsh '+round_text+' largest eig:', eigs[N-i])
          # printing the mean absolute error to check the accuracy
          print('MAE for '+round_text+' largest eig:',np.mean(np.abs(vec-evecs[:,-i])))
        
        print()
        
        psi0=evecs[:,-i]
        norm_const=simps(abs(psi0)**2,x=grid)
        psi0=psi0/norm_const

        psi0_ = vec*1.0
        norm_const=simps(abs(psi0_)**2,x=grid)
        psi0_=psi0_/norm_const

        # calculating the smallest eigenvalue and its eigenvector
        # using the inverse power method
        if i==1:
          l2,vec2=inv_power_method(A,-rm)
          print('Smallest eig estimate:', l2)
          print('Scipy eigsh smallest eig:', eigs2[i-1]) 
          print('MAE for smallest eig:',np.mean(np.abs(vec2-evecs2[:,i-1])))  
        
        # calculating the next smallest eigenvalue and its eigenvector
        # by expanding the inverse power method
        else:
          vec2=np.matrix((vec2)).T
          # setting the smallest eigenvalue to zero
          A_tilde2=A_tilde2-l2*(vec2.dot(vec2.T)/((vec2.T).dot(vec2)))
          # calculating the smallest eigvalue and eigvector for the newly formed
          # matrix
          l4,vec4=inv_power_method(A_tilde2,l2+0.2)
          vec4=np.matrix((vec4)).T
          # eigvalue is the same as A's but eigvector needs to be changed
          vec4=vec4-(l2/l4)*(((vec2.T).dot(vec4)/(vec2.T).dot(vec2))[0,0]*(vec2))
          vec2=np.array(vec4.T)[0]
          l2=1.0*l4
          print(round_text+' smallest eig estimate:',l2)
          print('Scipy eigsh '+round_text+' smallest eig:', eigs2[i-1]) 
          print('MAE for: '+round_text+' smallest eig:',np.mean(np.abs(vec2-evecs2[:,i-1])))

        print()
        
        psi1=evecs2[:,i-1]
        norm_const=simps(abs(psi1)**2,x=grid)
        psi1=psi1/norm_const

        psi1_ = vec2*1.0
        norm_const=simps(abs(psi1_)**2,x=grid)
        psi1_=psi1_/norm_const

        # for showing the eigenvalue estimates in the figure
        # PM=power method
        # IPM=inverse power method
        textstr = '\n'.join((
        r'$PM=%.4f$' % (l),
        r'$Scipy=%.4f$' % (eigs[-i])))
        textstr2 = '\n'.join((
        r'$IPM=%.4f$' % (l2),
        r'$Scipy=%.4f$' % (eigs2[i-1])))
    
        # visualizing the results
        fig=plt.figure()
        ax=fig.add_subplot(121)
        ax.title.set_text(str(round_text)+' largest')
        ax.plot(grid,abs(psi0)**2,'b',label='Scipy')
        ax.plot(grid,abs(psi0_)**2,'r--',label='Own solver')
        ax.text(0.01,0.99,textstr, ha='left', va='top',transform = ax.transAxes, fontsize=10)
        ax.legend(loc=0)
        ax2=fig.add_subplot(122)
        ax2.title.set_text(str(round_text)+' smallest')
        ax2.plot(grid,abs(psi1)**2,'b',label='Scipy')
        ax2.plot(grid,abs(psi1_)**2,'r--',label='Own solver')
        ax2.text(0.01,0.99,textstr2, ha='left', va='top',transform = ax2.transAxes, fontsize=10)
        ax2.legend(loc=0)
        plt.show()

if __name__=="__main__":
    main()