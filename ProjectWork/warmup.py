"""
Calculating the 2D integral of a function over the area specified
by lattice vectors using scipy's simpson rule and visualizing the
function as well as the specified area.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import simps,nquad

def integral():
    """
    Function calculating estimates for the 2d integral using 
    scipy's Simpsons integral and nquad 
    functions. Estimates are calculated using different amounts of
    intervals.
    """
    
    # amount of the grid points
    N=11
    # matrix that contains the lattice vectors
    A=np.array([[1.3,0.45],[0.2,1.0]])
    detA=np.linalg.det(A) 
    # initializing the alpha space since the original space is not separable
    alpha=np.linspace(0,1,N)

    # F is for storing the function values
    F=np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            a=[alpha[i],alpha[j]]
            # mapping the wanted coordinates from alpha space to 
            # the original space so that the function values can be calculated
            r=A.dot(a)
            F[i,j]=fun(r[0],r[1])
    
    # calculating the 2D integral
    I_s=simps(simps(F, alpha, axis=0),alpha)*detA
    
    print('Integral:', I_s)

    # forming the lines of the area omega that is going to be visualized
    x=np.linspace(0,2,N)
    y=np.linspace(0,2,N)
    X, Y = np.meshgrid(x,y)
    a1x=np.linspace(0.0,1.3,N)
    a1y=np.linspace(0.0,0.2,N)
    a2x=np.linspace(0.0,0.45,N)
    a2y=np.linspace(0.0,1.0,N)
    a1x2=np.linspace(0.45,1.75,N)
    a1y2=np.linspace(1.0,1.2,N)
    a2x2=np.linspace(1.3,1.75,N)
    a2y2=np.linspace(0.2,1.2,N)

    # visualizing the integrand and the are
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.contourf(X,Y,fun(X,Y))
    ax.plot(a1x,a1y,a2x,a2y,a1x2,a1y2,a2x2,a2y2,color='red')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    plt.show()
    

def fun(x,y):
     # 2D function for testing the integral function
    return (x+3*y)*np.exp(-1/2*np.sqrt(x**2+y**2))

def main():
    integral()

if __name__=="__main__":
    main()