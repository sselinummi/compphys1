import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse


def make_matrix(x):
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    return A/h/2

def main():
    N = 50
    grid = np.linspace(0,np.pi,N)
    x = np.sin(grid)
    
    A = make_matrix(grid)
    
    # calculate here b=Ax as described in the problem setup

    plt.plot(grid,b,grid,x,'--')
    plt.show()

if __name__=="__main__":
    main()



