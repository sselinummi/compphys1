""" computational physics1, exercise1, problem5
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Plots the absolute error of the numerically obtained estimate as a function of grid spacing dx
 """
import numpy as np
import matplotlib.pyplot as plt
from numerical_integration import riemannSum, trapezoid, simpsonInt, monte_carlo_integration, analytical_int
from num_calculus import first_derivative

def myPlot(x,y,e,name):
    """ Plots given values and error

    Args:
        x : values for x-axis, now length of the interval
        y : values for y-axis, either integral value or 1st derivative
        e : values for y-axis, now error in y compared to analytical value
        name : name of the used method
    """    

    plt.rcParams['legend.handlelength'] = 2
    plt.rcParams['legend.numpoints'] = 1
    #plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 12
    fig = plt.figure()
    fig.suptitle(name)

    ax = fig.add_subplot(111)

    # plot and add label if legend desired
    ax.plot(x,y,label=name)
    ax.plot(x,e,'--',label='Absolute error')
    # include legend (with best location, i.e., loc=0)
    ax.legend(loc=0)
    # set axes labels and limits
    ax.set_xlabel('dx')
    ax.set_ylabel('f(x)')
    ax.set_xlim(x[0], x[-1])
    fig.tight_layout(pad=1)
    # save figure as pdf with 200dpi resolution
    fig.savefig(name+".pdf",dpi=200)
    plt.show()


def main():
    # Test function and analytical integration and differentation 
    f = lambda x: np.sin(x)
    fa_int = lambda x: -1*np.cos(x)
    fa_diff = lambda x: np.cos(x)

    # integration interval
    a = 0
    b = np.pi/2

    # number of intervals
    N = np.array([i for i in range(3,10,1)])
    N_intervals = len(N)

    # initalizing
    y1 = np.zeros(N_intervals)
    y2 = np.zeros(N_intervals)
    e1 = np.zeros(N_intervals)
    e2 = np.zeros(N_intervals)
    dx = np.zeros(N_intervals)

    a_int = analytical_int(fa_int, a, b)

    for i, n in enumerate(N):
        x = np.linspace(a,b,n)
        # length of interval
        dx[i] = np.abs(x[1]-x[0])
        # trapezoid value
        y1[i] = trapezoid(f(x),x)
        # abs error
        e1[i] = np.abs(y1[i] - a_int)
        # numerical 1st derivative
        y2[i] = first_derivative(f, 5, dx[i])
        # abs error on derivative
        e2[i] = np.abs(y2[i]-fa_diff(5))

    # plotting
    myPlot(dx, y1, e1, 'Trapezoid')
    myPlot(dx, y2, e2, 'First_derivative')


if __name__=="__main__":
    main()