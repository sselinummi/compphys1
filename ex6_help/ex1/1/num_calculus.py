""" computational physics1, exercise1, problem3
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Calculates first and second derivative numerically and compares result to analytic result """
import numpy as np


def first_derivative( function, x, dx ):
    """ Calculates first derivative numerically, with error h^2

    Args:
        function : function to be detivated
        x : point in which derivated
        dx : length of interval

    Returns:
        numerical value for first derivative
    """    
    return (function(x+dx)-function(x-dx))/(2*dx)


def test_first_derivative(value = 1, interval = 0.5):
    """ Tests first_derivative function against analytical solution
    """    
    fun_names = ("sin(x)","5x^3+2","exp(x)","ln(|x|)")
    fun_1der = {lambda x: np.sin(x) : lambda x:np.cos(x), lambda x: 5*x**3+2 : lambda x: 15*x**2,
     lambda x: np.exp(x) : lambda x: np.exp(x), lambda x: np.log(np.abs(x)) : lambda x: 1/x}
    
    i=0
    for fun, der in fun_1der.items():
        numerical = first_derivative(fun,value,interval)
        analytical = der(value)
        print("Function: {} \n Derivatives: numerical: {}, analytical: {}, error: {} \n".format(fun_names[i],numerical,analytical, np.abs(numerical-analytical)))
        i += 1



def second_derivative( function, x, dx ):
    """ Calculates second derivative numerically, with error h^2

    Args:
        function : function to be detivated
        x : point in which derivated
        dx : length of interval

    Returns:
        numerical value for second derivative
    """    
    return (function(x+dx)+function(x-dx)-2*function(x))/(dx**2)


def test_second_derivative(value = 1, interval = 0.5):
    """ Tests second_derivative function against analytical solution
    """    
    fun_names = ("sin(x)","5x^3+2","exp(x)","ln(|x|)")
    fun_2der = {lambda x: np.sin(x) : lambda x:-1*np.sin(x), lambda x: 5*x**3+2 : lambda x: 30*x,
     lambda x: np.exp(x) : lambda x: np.exp(x), lambda x: np.log(np.abs(x)) : lambda x: -1/(x**2)}
    
    i=0
    for fun, der in fun_2der.items():
        numerical = second_derivative(fun,value,interval)
        analytical = der(value)
        print("Function: {} \n Second derivatives: numerical: {}, analytical: {}, error: {} \n".format(fun_names[i],numerical,analytical, np.abs(numerical-analytical)))
        i += 1


def main():
    value = input("Give a point(not zero because of ln()) on which derivative is calculated: ")
    while value == "0" and value !="q":
        value = input("Give a point(not zero) on which derivative is calculated")
    value = float(value)

    interval = input("Give a distance: ")
    while interval == "0" and interval !="q":
        value = input("Give a distance: ")
    
    
    interval = float(interval)
    print("Testing first derivatives")
    test_first_derivative(value,interval)
    print("Testing second derivatives")
    test_second_derivative(value,interval)

if __name__=="__main__":
    main()
