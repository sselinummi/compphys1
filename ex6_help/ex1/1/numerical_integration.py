""" computational physics1, exercise1, problem4
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Uses different numerical methods (riemann, trapezoid, simpson, monte carlo) to calculate
integrals and compares results to analytic results """
import numpy as np

def riemannSum(f,x):
    #left-hand Riemann sum, uniform grid
    dx = np.abs(x[1]-x[0])
    sum = 0
    for i, fv in enumerate(f[0:-1]):
        sum+=fv*dx
    return sum

def trapezoid(f,x):
    dx = np.abs(x[1]-x[0])
    sum = 0
    for i, fv in enumerate(f[0:-1]):
        sum+=(fv+f[i+1])*dx
    return sum/2


def simpsonInt(f,x):
    # uniform grid
    dx = np.abs(x[1]-x[0])
    # number of intervals
    N = len(f)-1
    I = 0
    dI = 0
    # if odd number of intervals separate the last part
    if N % 2 != 0:
        dI = dx/12*(-f[-3]+8*f[-2]+5*f[-1])
        N = N-1
    # even number of intervals
    for n in range(int(N/2)):
        I += f[2*n]+4*f[2*n+1]+f[2*n+2]

    return (dx/3*I+dI)



def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
    """ Calcualtes 1D Monte Carlo integration.

    Args:
        fun : function to be integrated
        xmin : lower limit of the integration
        xmax : upper limit of integration
        blocks : number of iteration blocks. Defaults to 10.
        iters : number of iterations. Defaults to 100.

    Returns:
        I : integration value
        dI : error estimate
    """    
    block_values=np.zeros((blocks,))
    # integration interval length
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            # random valuen from uniform distribution [0,1) and scales it to range [xmin,xmax)
            x = xmin+np.random.rand()*L 
            # sum of function values at given random point
            block_values[block]+=fun(x)
        # get's mean of the value
        block_values[block]/=iters
    # integral value is expectation value (mean) times interval
    I = L*np.mean(block_values)
    # error estimate for the integral
    dI = L*np.std(block_values)/np.sqrt(blocks)

    return I,dI

def analytical_int(F,a,b):
    return (F(b)-F(a))


def func(x):
    return np.sin(x)

def main():
    # test functions
    fun_names = ("sin(x)","5x^3+2","exp(x)")
    fun_int = {lambda x: np.sin(x) : lambda x:-1*np.cos(x), lambda x: 5*x**3+2 : lambda x: 5/4*x**4+2*x,
     lambda x: np.exp(x) : lambda x: np.exp(x)}
    # values for integration
    a = 0
    b = np.pi/2
    N = 4
    x = np.linspace(a,b,N)

    i = 0
    for f, fa in fun_int.items():
        print("Tested function: {} \n".format(fun_names[i]))
        i+=1

        print("Riemann: {}".format(riemannSum(f(x),x)))
        print("Trapezoidal: {}".format(trapezoid(f(x),x)))
        print("Simpson: {}".format(simpsonInt(f(x),x)))

        I,dI=monte_carlo_integration(f,0.,np.pi/2,10,100)
        print("Monte Carlo: {0:0.5f} +/- {1:0.5f}".format(I,2*dI))
        print("Analytical: {} \n".format(analytical_int(fa,a,b)))


if __name__=="__main__":
    main()