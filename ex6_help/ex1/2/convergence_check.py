""" Plot absolute errors as a function of grid spacing """

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# Load data for plotting, differentiation data
file = ".data/differentiation_errors.csv"
df = pd.read_csv(file, sep=",")
log_data = df[df["Function"] == "Logarithmic"]
log_data_1st = log_data[log_data["Order of gradient"] == 1]
log_data_1st_error = log_data_1st["Error"][::-1]
data_h = log_data_1st["Grid spacing"][::-1]

log_data_2nd = log_data[log_data["Order of gradient"] == 2]
log_data_2nd_error = log_data_2nd["Error"][::-1]

# Second function
pol_data = df[df["Function"] == "Polynomial"]
pol_data_1st = pol_data[pol_data["Order of gradient"] == 1]
pol_data_1st_error = pol_data_1st["Error"][::-1]
pol_data_2nd = pol_data[pol_data["Order of gradient"] == 2]
pol_data_2nd_error = pol_data_2nd["Error"][::-1]


# Same but for integration data
file = ".data/integration_errors.csv"
df = pd.read_csv(file, sep=",")
log_data = df[df["Function"] == "Logarithmic"]
log_data_rie = log_data[log_data["Method"] == "Riemann"]
log_data_rie_error = log_data_rie["Error"]
data_h_int = log_data_rie["Grid spacing"]

log_data = df[df["Function"] == "Logarithmic"]
log_data_sim = log_data[log_data["Method"] == "Simpson"]
log_data_sim_error = log_data_sim["Error"]

log_data = df[df["Function"] == "Logarithmic"]
log_data_tra = log_data[log_data["Method"] == "Trapezoid"]
log_data_tra_error = log_data_tra["Error"]

# Polynomial
log_data = df[df["Function"] == "Polynomial"]
log_data_rie = log_data[log_data["Method"] == "Riemann"]
pol_data_rie_error = log_data_rie["Error"]

log_data = df[df["Function"] == "Polynomial"]
log_data_sim = log_data[log_data["Method"] == "Simpson"]
pol_data_sim_error = log_data_sim["Error"]

log_data = df[df["Function"] == "Logarithmic"]
log_data_tra = log_data[log_data["Method"] == "Trapezoid"]
pol_data_tra_error = log_data_tra["Error"]


height = 9
width=height*(np.sqrt(5.0)+1.0)/2.0
fig = plt.figure(figsize=(width, height))
# - or, e.g., fig = plt.figure(figsize=(width, height))
# - so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0
# - width and height are given in inches (1 inch is roughly 2.54 cm)
ax = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)
axs = [ax, ax2, ax3, ax4]

# plot and add label if legend desired
ax.plot(data_h, log_data_1st_error, label="$f(x)=\log(x)-2$, 1st order D")
ax.plot(data_h, log_data_2nd_error, label="$f(x)=\log(x)-2$, 2nd order D")
ax2.plot(data_h, pol_data_1st_error, label="$f(x)=3x^3-4x^2$, 1st order D")
ax2.plot(data_h, pol_data_2nd_error, label="$f(x)=3x^3-4x^2$, 2nd order D")

ax3.plot(data_h_int, log_data_rie_error, label="$f(x)=\log(x)-2$, Riemann")
ax3.plot(data_h_int, log_data_tra_error, label="$f(x)=\log(x)-2$, Trapezoid")
ax3.plot(data_h_int, log_data_sim_error, label="$f(x)=\log(x)-2$, Simpson")

ax4.plot(data_h_int, pol_data_rie_error, label="$f(x)=3x^3-4x^2$, Riemann")
ax4.plot(data_h_int, pol_data_tra_error, label="$f(x)=3x^3-4x^2$, Trapezoid")
ax4.plot(data_h_int, pol_data_sim_error, label="$f(x)=3x^3-4x^2$, Simpson")

# include legend (with best location, i.e., loc=0)
ax.set_xlim(data_h.min(), data_h.max())
ax2.set_xlim(data_h.min(), data_h.max())
ax3.set_xlim(data_h_int.min(), data_h_int.max())
ax4.set_xlim(data_h_int.min(), data_h_int.max())
fig.tight_layout(pad=1)

for a in axs:


    a.legend(loc=0)
    #ax2.legend()
    # set axes labels and limits
    x_label = "Grid spacing h"
    y_label = "Absolute error"
    a.set_xlabel(x_label)
    a.set_ylabel(y_label)
    #ax2.set_xlabel(x_label)
    #ax2.set_ylabel(y_label)
    #ax2.set_xlim(data_h.min(), data_h.max())
    a.invert_xaxis()
    #ax2.invert_xaxis()


# save figure as pdf with 200dpi resolution
fig.savefig("error_plot.pdf", dpi=200)
plt.show()
