""" Monte Carlo integration and appropriate testing. """
import numpy as np


def monte_carlo_integration(fun, xmin, xmax, blocks=10, iters=100):
    """
    1D Monte Carlo integration, no importance sampling.
    :param fun: python function
    :param xmin: lower-bound of the interval
    :param xmax: upper-bound of the interval
    :param blocks: [ximn, xmax] is divided into n blocks
    :param iters: number of iterations to calculate a mean of sampled value
    :return:
    """
    block_values = np.zeros((blocks,))
    L = xmax-xmin

    for block in range(blocks):
        for i in range(iters):
            # random.rand returns random samples from a uniform distribution [0, 1[
            # Therefore x gets values between from [xmin, xmax[ (uniform)
            x = xmin+np.random.rand()*L
        block_values[block]+=fun(x)
    block_values[block]/=iters          # calculate average over number of iterations
    I = L*np.mean(block_values)         # calculate integral
    dI = L*np.std(block_values)/np.sqrt(blocks)     # calculate error as standard deviations with a confidence 68%
    return I, dI


def func(x):
    """ Test function """
    return np.sin(x)


def test_montecarlo(threshold=0.01):
    """
    Test accuracy of montecarlo integration with different amount of blocks.

    :param threshold: error and absolute value error threshold
    :return: number of blocks that satisfies the threshold
    """
    for pows in range(1, 10):
        nblocks = pow(10, pows)
        I, dI = monte_carlo_integration(func, 0., np.pi / 2, nblocks, 10)

        print("Integrated value: {:0.5f} +/- {:0.5f} with {} iterations.".format(I, 2 * dI, nblocks))
        real = 1

        if dI < threshold and abs(real - I) < threshold:
            return nblocks

    return 0


def main():
    """
    Evaluate Monte Carlo integral and return number of blocks needed for a wanted accuracy.
    :return:
    """
    print("Integral of sin(x) from 0 to pi/2 is 1.")
    threshold = 0.01
    nblocks = test_montecarlo(threshold)
    print("Suitable number of blocks: {}".format(nblocks))


if __name__=="__main__":
    main()
