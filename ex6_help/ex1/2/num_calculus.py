"""
    Problem 3: numerical derivatives

    For the problem 6, see the folder "num_calculus" which is functionally same as this file but
    is a python package.
"""
import argparse
import numpy as np


def first_derivative( function, x, h ):
    """
    Gradient of a function with an error term O(h^2).

    :param function: normal single valued python function
    :param x: evaluation points, np.array
    :param h: estimation step, float
    :return: estimated gradients at the points x
    """
    return ( function(x+h)-function(x-h) ) / (2*h)


def second_derivative(f, x, h):
    """
    2nd gradient of a function f with an error term O(h^2).

    :param f: normal single valued python function
    :param x: evaluation points, np.array
    :param h: estimation step, float
    :return: estimated gradients at the points x
    """

    return ( f(x+h) + f(x-h) - 2*f(x) ) / (h**2)


# Test function 1 and its derivatives, basic polynomial
def _test_func1(x): return 3*np.power(x, 3) - 4*np.power(x, 2)
def _test_func1_derivative(x): return 9*np.power(x, 2) - 8*x
def _test_func1_2nd_derivative(x): return 18*x - 8


#### Test function 2 and its derivatives ####
def _test_func2(x):
    assert not np.any(x <= 0), "Error, negative values to logarithm. Change x and/or h."
    return np.log(x)-2


def _test_func2_derivative(x):
    assert not np.any(x == 0), "Error, division by zero"
    return 1/x


def _test_func2_2nd_derivative(x):
    assert not np.any(x == 0), "Error, division by zero"
    return -1/np.power(x, 2)
#### end ####


def test_derivative(eval_grad, test_func, h):
    """
    Test numerical derivation and print results.

    :param eval_grad: (int) 1 to evaluate 1st order gradient and 2 to evaluate 2nd order gradient
    :param test_func: (int) 1 to use _test_func1 and 2 to use _test_func2
    :param h: (float) estimation step
    :return: None
    """

    # Select right functions
    test_funcs = {1: _test_func1, 2: _test_func2}

    if test_func == 1:
        funcs = {1: [first_derivative, _test_func1_derivative],
                 2: [second_derivative, _test_func1_2nd_derivative]}
    elif test_func == 2:
        funcs = {1: [first_derivative, _test_func2_derivative],
                 2: [second_derivative, _test_func2_2nd_derivative]}

    approximator = funcs[eval_grad][0]
    target_func = funcs[eval_grad][1]

    # Generate evaluation points and do calculations
    x = np.linspace(1, 10, num=10)
    derv_value = approximator(test_funcs[test_func], x, h)
    target_value = target_func(x)

    # Relative error
    error = np.abs((target_value - derv_value) / target_value)
    mean_error = np.mean(error)

    if mean_error < 0.01:
        print("Mean relative error ({}) is under 0.01, h is suitable.".format(mean_error))
    else:
        print("Error ({}) is large, please lower h.".format(mean_error))

    return


def main(eval_grad, test_func, h):
    """
    Run numerical derivation test.

    :param eval_grad: (int) 1 to evaluate 1st order gradient and 2 to evaluate 2nd order gradient
    :param test_func: (int) 1 to use _test_func1 and 2 to use _test_func2
    :param h: (float) estimation step
    :return: None
    """

    test_derivative(eval_grad, test_func, h)
    return


if __name__=="__main__":
    """
        Parse commandline arguments.        
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gradient', '-g', type=int, help="Which gradient to evaluate; 1 for 1st order gradient and 2 for 2nd order gradient",
        default=1)
    parser.add_argument(
        '--function', '-f', type=int, help="Which function to use (1 or 2)",
        default=1)
    parser.add_argument(
        '--step_size', '-s', type=float, help='Step size for numerical estimation', default=0.1)

    args = parser.parse_args()

    main(args.gradient, args.function, args.step_size)
