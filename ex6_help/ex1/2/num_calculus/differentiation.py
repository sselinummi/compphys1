def first_derivative( function, x, h ):
    """
    Gradient of a function with an error term O(h^2).

    :param function: normal single valued python function
    :param x: evaluation points, np.array
    :param h: estimation step, float
    :return: estimated gradients at the points x
    """
    # Not stable numerically when dealing with small numbers, cancellation can occur.
    return ( function(x+h)-function(x-h) ) / (2*h)


def second_derivative(f, x, h):
    """
    2nd gradient of a function f with an error term O(h^2).

    :param f: normal single valued python function
    :param x: evaluation points, np.array
    :param h: estimation step, float
    :return: estimated gradients at the points x
    """
    # Not stable numerically when dealing with small numbers, cancellation can occur.
    return ( f(x+h) + f(x-h) - 2*f(x) ) / (h**2)

