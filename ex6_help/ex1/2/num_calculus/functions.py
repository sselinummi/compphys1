""" Test functions for numerical testing """
import numpy as np

# Test function 1 and its derivatives and integral, basic polynomial
def func1(x): return 3*np.power(x, 3) - 4*np.power(x, 2)
def func1_derivative(x): return 9*np.power(x, 2) - 8*x
def func1_2nd_derivative(x): return 18*x - 8
def func1_integral(x): return 3/4*np.power(x, 4) - 4/3 * np.power(x, 3)


#### Test function 2 and its derivatives ####
def func2(x):
    assert not np.any(x <= 0), "Error, negative values to logarithm. Change evaluation points and/or h."
    return np.log(x)-2


def func2_derivative(x):
    assert not np.any(x == 0), "Error, division by zero"
    return 1/x


def func2_2nd_derivative(x):
    assert not np.any(x == 0), "Error, division by zero"
    return -1/np.power(x, 2)


def func2_integral(x):
    assert not np.any(x <= 0), "Error, negative values to logarithm. Change evaluation points and/or h."
    return x * np.log(x) - x - 2*x
