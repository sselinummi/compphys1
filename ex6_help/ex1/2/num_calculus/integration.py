import numpy as np

def riemann(f, x):
    """ Left-hand Riemann sum """
    dx = abs(x[1] - x[0])
    return np.sum(f * dx)


def trapezoid(f, x):
    dx = abs(x[1] - x[0])
    return 0.5 * dx * np.sum((f[:-1] + f[1:]))


def simpson(f, x):
    """ Simpson rule, not so accurate when one has even number of grid values """
    dx = abs(x[1] - x[0])

    # odd number off grid points
    if len(f) % 2 != 0:
        a = f[2:-2:2]
        b = f[1:-1:2]
        return dx / 3 * (f[0] + 2*np.sum(a) + 4*np.sum(b) + f[-1])
    else:
        # calculate even and add the correction term
        g = f[:-1]
        a = g[2:-2:2]
        b = g[1:-1:2]

        i_even = dx / 3 * (f[0] + 2*np.sum(a) + 4*np.sum(b) + f[-1])
        return i_even + dx / 12 * (-f[-3] + 8*f[-2] + 5*f[-1])



