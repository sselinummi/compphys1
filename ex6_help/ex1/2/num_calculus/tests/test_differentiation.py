import numpy as np
import unittest
import sys
import logging
from num_calculus.differentiation import first_derivative
from num_calculus.differentiation import second_derivative
from num_calculus.functions import *
import os
import csv

def _error_estimation(log, target_value, approximation, h, func, grad):
    """
    Calculate mean relative error and print result.
    :param log: logger
    :param target_value: real known value
    :param approximation: result of numerical approximation
    :param h: step size of numerical estimation
    :param func: Index of used function
    :param grad: Index of evaluated gradient
    :return:
    """
    # Relative error
    error = np.abs((target_value - approximation) / target_value)
    mean_error = np.mean(error)

    pr = "{}, {} order gradient, step size h={:.5f}, Mean relative error {:.7f}".format(
        "Polynomial" if func == 1 else "Logarithmic",
        "1st" if grad == 1 else "2nd",
        h, mean_error)

    log.debug(pr)
    print(pr)

    return


def make_path(path, full_path=True):
    """
    Generate directory if it is not found.
    :param path: directory path to file or without destination file
    :param full_path: if destination file is not in the path, then full_path=False
    :return:
    """
    if full_path:
        if path and not os.path.exists(os.path.dirname(path)):
            try:
                os.makedirs(os.path.dirname(path))
            except:
                print("Problem with creating directory")
    else:
        if path and not os.path.exists(path):
            try:
                os.makedirs(path)
            except:
                print("Problem with creating directory")


def _save_errors(target_value, approximation, h, func, grad):
    """
    Save data for later use, e.g. plotting.

    :param target_value: real known value
    :param approximation: result of numerical approximation
    :param h: step size of numerical estimation
    :param func: Index of used function
    :param grad: Index of evaluated gradient
    :return:
    """

    absolute_error = np.mean(abs(target_value-approximation))
    data_list = ["Polynomial" if func == 1 else "Logarithmic", grad, h, absolute_error]
    save_stats = ".data_nce1/differentiation_errors.csv"
    make_path(save_stats, full_path=True)

    # Write header at the first epoch
    if not os.path.exists(save_stats):
        header = ["Function", "Order of gradient", "Grid spacing", "Error"]

        with open(save_stats, "w") as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(header)

    with open(save_stats, "a") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(data_list)


class TestDifferentiation(unittest.TestCase):
    def test_derivative(self):
        """
        Test numerical derivation with two different functions and different step sizes.
        :return: None
        """

        log = logging.getLogger("TestDifferentiation,test_derivative")
        log.debug("")

        # Generate evaluation points
        x = np.linspace(5, 15, num=10)

        # Select right functions
        test_funcs = {1: func1, 2: func2}

        for i in [1, 2]:        # test func1 and test func2
            for j in [1, 2]:    # 1st and 2nd order gradient
                if i == 1:
                    funcs = {1: [first_derivative, func1_derivative],
                             2: [second_derivative, func1_2nd_derivative]}
                elif i == 2:
                    funcs = {1: [first_derivative, func2_derivative],
                             2: [second_derivative, func2_2nd_derivative]}

                approximator = funcs[j][0]
                target_func = funcs[j][1]
                target_value = target_func(x)

                for h in [3, 1, 0.5, 0.1, 0.001]:      # test with different step size

                    derv_value = approximator(test_funcs[i], x, h)
                    _error_estimation(log, target_value, derv_value, h, i, j)
                    _save_errors(target_value, derv_value, h, i, j)
        return


