import unittest
from num_calculus.integration import riemann
from num_calculus.integration import trapezoid
from num_calculus.integration import simpson
from num_calculus.functions import *
from num_calculus.tests.test_differentiation import make_path
import sys
import logging
import csv
import os


def _error_estimation(log, target_value, approximation, func, method, h):
    """
    Calculate mean relative error and print result.
    :param log: logger
    :param target_value: real known value
    :param approximation: result of numerical approximation
    :param func: Index of used function
    :param method: name of the method, str
    :param h: grid spacing
    :return:
    """
    # Relative error
    error = np.abs((target_value - approximation) / target_value)
    mean_error = np.mean(error)

    pr = "\nUsing {} function and {} method the mean relative error is {} with {} h.".format(
        "Polynomial" if func == 1 else "Logarithmic",
        method,
        mean_error, h)

    log.debug(pr)
    print(pr)


def _save_errors(target_value, approximation, h, func, method):
    """
    Save data for later use, e.g. plotting.

    :param target_value: real known value
    :param approximation: result of numerical approximation
    :param h: step size of numerical estimation
    :param func: Index of used function
    :param method: integration method, str
    :return:
    """

    absolute_error = np.mean(abs(target_value - approximation))
    data_list = ["Polynomial" if func == 1 else "Logarithmic", method, h, absolute_error]
    save_stats = ".data_nce1/integration_errors.csv"
    make_path(save_stats, full_path=True)

    # Write header at the first epoch
    if not os.path.exists(save_stats):
        header = ["Function", "Method", "Grid spacing", "Error"]

        with open(save_stats, "w") as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(header)

    with open(save_stats, "a") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(data_list)


class TestIntegration(unittest.TestCase):
    """
    Test numerical integration with a polynomial function and logarithmic function.
    """

    logging.basicConfig(stream=sys.stderr)
    log = logging.getLogger()

    test_funcs = {1: func1, 2: func2}
    target_funcs = {1: func1_integral, 2: func2_integral}

    def test_riemann(self):
        for func in [1, 2]:
            test_func = self.test_funcs[func]
            target_func = self.target_funcs[func]
            for npoints in [11, 51, 101, 1001]:
                x = np.linspace(5, 15, npoints)
                h = x[1] - x[0]
                f = test_func(x)

                riemann_result = riemann(f, x)
                target_value = target_func(x[-1]) - target_func(x[0])

                _error_estimation(self.log, target_value, riemann_result, func, "Riemann", h)
                _save_errors(target_value, riemann_result, h, func, "Riemann")
        return

    def test_trapezoid(self):
        for func in [1, 2]:
            test_func = self.test_funcs[func]
            target_func = self.target_funcs[func]

            for npoints in [11, 51, 101, 1001]:
                x = np.linspace(5, 15, npoints)
                h = x[1] - x[0]

                f = test_func(x)

                trapezoid_result = trapezoid(f, x)
                target_value = target_func(x[-1]) - target_func(x[0])

                _error_estimation(self.log, target_value, trapezoid_result, func, "Trapezoid", h)
                _save_errors(target_value, trapezoid_result, h, func, "Trapezoid")
        return

    def test_simpson(self):
        for func in [1, 2]:
            test_func = self.test_funcs[func]
            target_func = self.target_funcs[func]

            for npoints in [11, 51, 101, 1001]:
                x = np.linspace(5, 15, npoints)
                h = x[1] - x[0]

                f = test_func(x)

                simpson_result = simpson(f, x)
                target_value = target_func(x[-1]) - target_func(x[0])

                _error_estimation(self.log, target_value, simpson_result, func, "Simpson", h)
                _save_errors(target_value, simpson_result, h, func, "Simpson")
        return
