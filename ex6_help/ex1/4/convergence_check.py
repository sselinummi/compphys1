""" Illustration of the convergence of the numerical calculus methocs from num_calculus.py"""

from cProfile import label
from numbers import Integral
import num_calculus as nc
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import derivative

plt.rcParams["legend.handlelength"] = 2
plt.rcParams["legend.numpoints"] = 1
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 12
fig = plt.figure()
# - or, e.g., fig = plt.figure(figsize=(width, height))
# - so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0

N = 100
h = np.linspace(0.5, 0.01, N)
derivative_err = np.zeros(N)
second_derivative_err = np.zeros(N)
f = np.sin
x0 = 3/8*np.pi
xmin = 0
xmax = np.pi/2
riemann_int_err = np.zeros(N)
simpson_int_err = np.zeros(N)
MC_int_err = np.zeros(N)

for i in range(5,N):
    derivative_err[i] = abs(nc.first_derivative(f,x0,h[i]) - np.cos(x0))/np.cos(x0)
    second_derivative_err[i] = abs(nc.second_derivative(f,x0,h[i]) + np.sin(x0))/np.cos(x0)

    x = np.linspace(xmin, xmax, i)
    y = f(x)
    riemann_int = nc.left_riemann_int(y, x)
    riemann_int_err[i] = abs((1 - riemann_int))
    simpson_int = nc.simpson_int(y, x)
    simpson_int_err[i] = abs(1 - simpson_int)
    MC_int = nc.monte_carlo_integration(f, xmin, xmax, 10, i)
    MC_int_err[i] = abs(1 - MC_int[0])

ax = fig.add_subplot(3,1,1)
x = np.linspace(0,np.pi/2,100)
f = np.sin(x)
g = np.exp(-x)
# plot and add label if legend desired
ax.plot(x,f,label=r'$f(x)=\sin(x)$')
ax.plot(x,g,"--",label=r'$f(x)=\exp(-x)$')
# include legend (with best location, i.e., loc=0)
ax.legend(loc=0)
# set axes labels and limits
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$f(x)$')
ax.set_xlim(x.min(), x.max())
fig.tight_layout(pad=1)
# save figure as pdf with 200dpi resolution

ax2 = fig.add_subplot(3,1,2)
ax2.plot(h, derivative_err, label="First derivative")
ax2.plot(h, second_derivative_err, label="Second derivative")
ax2.set_xlim(h[5], h[N-1])
plt.title(r'Covergence of numerical derivatives of $\sin(x)$ at $x = \frac{3}{8} \pi$', fontsize='small')
ax2.set_xlabel(r'h')
ax2.set_ylabel("Relative error")
ax2.set_yscale('log')
ax2.legend(loc=0, fontsize='small')

ax3 = fig.add_subplot(3,1,3)
ax3.plot(riemann_int_err, label="Riemann")
ax3.plot(simpson_int_err, label="Simpson")
ax3.plot(MC_int_err, label="Monte Carlo, 10 blocks")
ax3.set_xlim(5, N)
plt.title(r'Covergence of numerical integrals of $\sin(x), x = [0,\pi/2]$', fontsize='small')
ax3.set_xlabel("Number of intervals")
ax3.set_ylabel("Relative error")
ax3.set_yscale('log')
ax3.legend(loc=0, fontsize='small')

fig.savefig("testfile.pdf",dpi=200)
