""" Numerical derivative calculation, ex1"""

import numpy as np

def first_derivative( function, x, dx ):
    # Numerical first derivitave calculation with O(h^2) error term
    derivative = (function(x+dx)-function(x-dx))/(2*dx)
    return derivative

def test_first_derivative():
    # Testing numerical derivitave estimation with an example function and printing the error
    fun = lambda x : 2*x**3 + x**2 - x
    x = 3
    dx = 0.01

    estimate = first_derivative(fun, x, dx)
    analytical = 6*x**2 + 2*x - 1
    error = abs(estimate - analytical)

    print("First derivative value: {0:0.5f} error {1:0.5f}".format(estimate, error))

def second_derivative(function, x, dx):
    # Numerical second derivitave calculation with O(h^2) error term
    derivative = (function(x+dx)+function(x-dx)-2*function(x))/(dx**2)
    return derivative

def test_second_derivative():
    # Testing numerical second derivitave estimation with an example function and printing the error
    fun = lambda x : 2*x**3 + x**2 - x
    x = 3
    dx = 0.01

    estimate = second_derivative(fun, x, dx)
    analytical = 12*x + 2
    error = abs(estimate - analytical)

    print("Second derivative value: {0:0.5f} error {1:0.5f}".format(estimate, error))

def left_riemann_int(f, x):
    """
     Left-handed Riemann integral
     f : Array of the function values
     x : Array of corresponding x values
    """
    integral = 0
    for i in range(0,len(f)-2):
        integral += f[i]*(x[i+1]-x[i])
    return integral

def test_riemann_int():
    # Test function of Riemann integral
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)

    analytical = np.cos(0)-np.cos(np.pi/2)
    estimation = left_riemann_int(f,x)
    error = abs(analytical-estimation)

    print("Riemann integrated value: {0:0.5f} error {1:0.5f}".format(estimation, error))

def simpson_int(f, x):
    """
     Simpson integration
     f : Array of the function values
     x : Array of corresponding x values
    """
    integral = 0
    N = len(x)
    if N % 2 == 0:        
        for i in range(0,round(N/2-1)):
            h = x[i+1]-x[i]
            integral += h/3*(f[2*i]+4*f[2*i+1]+f[2*i+2])
    if N % 2 == 1:
        h = 0
        for i in range(0,round((N-1)/2-1)):
            h = x[i+1]-x[i]
            integral += h/3*(f[2*i]+4*f[2*i+1]+f[2*i+2])
        integral += h/12*(-f[N-3]+8*f[N-2]+5*f[N-1])
    return integral

def test_simpson_int():
    # Test function of Simpson integral
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)

    analytical = np.cos(0)-np.cos(np.pi/2)
    estimation = simpson_int(f,x)
    error = abs(analytical - estimation)

    print("Simpson integrated value: {0:0.5f} error {1:0.5f}".format(estimation, error))

def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
    """
     Monte Carlo integration
     fun : Function handle of the function to be integrated
     xmin : Lower limit of the integration
     xmax : Upper limit of the integration
     blocks : Number of integration blocks
     iters : Number of iterations
    """
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI

def test_MC_int():
    # Test fonction of the Monte Carlo integration
    func = lambda x : np.sin(x)
    I,dI = monte_carlo_integration(func,0.,np.pi/2,10,100)
    print("Integrated value: {0:0.5f} +/- {1:0.5f}".format(I,2*dI))


def main():
    # necessary commenting and testing here
    test_first_derivative()
    test_second_derivative()
    test_riemann_int()
    test_simpson_int()
    test_MC_int()

if __name__=="__main__":
    main()
