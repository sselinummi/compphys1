import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative
from num_calculus import rieman_int

def fun(x):
    return np.sin(x)
plt.rcParams['legend.handlelength'] = 2
plt.rcParams['legend.numpoints'] = 1
#plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 12

fig = plt.figure()
#or, e.g., fig = plt.figure(figsize=(width, height))
#so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0
#width and height are given in inches (1 inch is roughly 2.54 cm)
ax = fig.add_subplot(111)
n = np.arange(5, 1001, 5)
dx = np.linspace(0,0,len(n))
er = np.linspace(0,0,len(dx))
for i in range(0,len(n)):    
    x1 = np.linspace(0, np.pi/2,n[i])
    dx[i] = x1[1]-x1[0]
    f = np.sin(x1)
    er[i] = np.abs(rieman_int(f,x1)-(np.cos(0)-np.cos(np.pi/2)))

x2 = 0.5
ed = np.linspace(0,0,len(dx))
i=0
for dxx in dx:
    ed[i] = np.abs(first_derivative(fun,x2,dxx)-np.cos(x2))
    i=i+1

#plot and add label if legend desired
ax.plot(dx,er,label=r'Absolute error of Riemann integral')
ax.plot(dx,ed,'--',label=r'Absolute error of first derivative')

# include legend (with best location, i.e., loc=0)
ax.legend(loc=0)

# set axes labels and limits
ax.set_xlabel(r'$dx$')
ax.set_ylabel(r'$Absolute-error$')
ax.set_xlim(dx.min(), dx.max())
fig.tight_layout(pad=1)

# save figure as pdf with 200dpi resolution
fig.savefig('testfile.pdf',dpi=200)
plt.show()
