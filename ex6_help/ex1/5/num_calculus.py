"""Python functions that numerically estimate the first derivative, second derivative and integral values using Riemann, Simpson and Trapezoid integration methods.
   Each function has its own testing function.
   Variables used: 
   function - function value(s)
   x - independent variable
   dx - independent varibale increment  """
 
#import necessary libraries
import numpy as np

#define first derivative
""" Define a python function first_derivative which takes in three arguments(function, x, dx).
    This function returns the estimated value of the first derivative."""
def first_derivative(function, x, dx):
    result = (function(x+dx)-function(x-dx))/(2*dx)
    return result

#define second derivative
""" Define a python function second_derivative which takes in three arguments(function, x, dx). 
    This function returns the estimated value of the second derivative. """
def second_derivative(function, x, dx):
    result = (function(x+2*dx)+function(x-2*dx)-(2*function(x)))/(4*dx*dx)
    return result;
    
#define rieman int
""" This function returns the estimated integral value using the Riemann left-hand sum approximation.
    x is an array of previously defined values for an independent variable on the integration interval."""
def rieman_int(function, x):
    dx = x[1]-x[0]
    l = len(x)
    result = dx*sum(function[:l-1])
    return result

#define trapezoid_int
""""This function returns the estimated integral value with Trapezoid integratio method.
    x is an array of previously defined values for an independent variable on the integration interval.."""
def trapezoid_int(function, x):
    dx=x[1]-x[0]
    l=len(x)-1
    result = (1/2)*function[0]*dx+sum(function[:l])*dx+(1/2)*function[l]*dx
    return result

#define simpson_int
""" This function returns the estimated integral value with Simpson integration method.
    x is an array of previously defined values for an independent variable on the integration interval. """
def simpson_int(function, x):
    dx=x[1]-x[0]
    l = len(x)
    """ if/else function  to check if there are odd or even numbers of testing points.
    (even number of testing points = odd number of testing intervals and vice-versa) """
    if(l%2==0):
        I = (dx/3)*(function[0]+4*np.sum(function[1:l-1:2])+2*np.sum(function[0:l-2:2])+function[l-1])
        Id= (dx/12)*(-function[l-3] +8*function[l-2] +5*function[l-1])
        result = I+Id
        return result
    else:
        result =  (dx/3)*(function[0]+4*sum(function[1:l-1:2])+2*sum(function[0:l-2:2])+function[l-1])
        return result

#define Monte Carlo integration
""" This function returns the estimated integral value with Monte Carlo integration method. """
def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI

"""Define the test functions/ in this case sin(x) and cos(x) functions"""
def test_fun(x): 
    return np.sin(x)
""" Analytical derivative of test_fun"""
def test_fun_1(x):
    return np.cos(x)
""" Analytical integral of test_fun"""
def test_fun_2(x):
    return -np.cos(x)
            
"""Define the python function to test the first derivative function"""
def test_first_derivative():
    x = 0.5
    dx = 0.000001
    der_estimate = first_derivative(test_fun, x, dx)                                #calling the derivative function in testing function
    calculate_f = test_fun_1(x)                                                     #analytical solution of the function
    error = abs(der_estimate-calculate_f)/abs(calculate_f)*100                      #relative estimation error 
    success = False
    if(error < 0.001):                                                              #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test of the first derivative was successful.")
        success = True
    else:
        print("Estimated calculation test of the first derivative was not successful.")

    print("First derivative estimate:",der_estimate)
    print("Analytical result:", calculate_f)
    print("Error:", error)
    return success

"""define the python function to test the second derivative"""
def test_second_derivative():
    x=0.5
    dx = 0.0001
    der_estimate = second_derivative(test_fun, x, dx)                               #calling the derivative function in testing function
    calculate_f = -test_fun(x)                                                      #analytical solution of the function
    error = abs(der_estimate-calculate_f)/abs(calculate_f)*100                      #relative estimation error
    success = False
    if(error < 0.001):                                                              #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test of the second derivative was sucessful.")
        success = True
    else:
        print("Estimated calculation test of the second derivative was not successful.")

    print("Second derivative estimate:",calculate_f)
    print("Analytical result:", calculate_f)
    print("Error:", error)
    return success

""" define a python function to test the Riemann sum integration"""
def riemann_integration_test():
    x = np.linspace(0, np.pi/2, 100)                                                
    l = len(x)                                                                      #take out the lenght of the array x(number of integration nodes)
    calculate_f = test_fun_2(x[l-1])-test_fun_2(x[0])                               #analytically obtained value of the integral
    f = test_fun(x)
    I = rieman_int(f, x)                                                            #calling the Riemann integration function in testing function
    error = abs(I-calculate_f)/abs(calculate_f)*100                                 #relative estimation error
    success = False
    if(error < 1):                                                                  #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test of Riemann integration was sucessful.")
        success = True
    else:
        print("Estimated calculation test of Riemann integration was not sucessful.")

    print("Riemann integration estimate:",I)
    print("Analytical result:", calculate_f)
    print("Error:", error)
    return success
        

""" define the Trapezoid test function """
def trapezoid_integration_test():
    x = np.linspace(0, np.pi/2, 100)                                               
    l = len(x)                                                                     #take out the lenght of the array x(number of integration nodes)
    calculate_f= test_fun_2(x[l-1])-test_fun_2(x[0])                               #analytically obtained value of the integral
    f=test_fun(x)
    I = trapezoid_int(f, x)                                                        #calling the Trapezoid integration function in testing function
    error = abs(I-calculate_f)/abs(calculate_f)*100                                #relative estimation error
    success = False
    if(error < 1):                                                                 #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test of Trapezoid integration was sucessful.")
        success = True
    else:
        print("Estimated calculation test of Trapezoid integration was not sucessful.")

    print("Trapezoid integration estimate:",I)
    print("Analytical result:", calculate_f)
    print("Error:", error)
    return success

"""define the Simpson test function"""
def sipmson_integration_test():
    x = np.linspace(0, np.pi/2, 100)                                               
    l = len(x)                                                                     #take out the lenght of the array x(number of integration nodes)
    calculate_f= test_fun_2(x[l-1])-test_fun_2(x[0])                               #analytically obtained value of the integral
    f=test_fun(x)
    I = simpson_int(f, x)                                                          #calling the Simpson integration function in testing function
    error = abs(I-calculate_f)/abs(calculate_f)*100                                #relative estimation error
    success = False
    if(error < 1):                                                                 #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test of Simpson integration was sucessful.")
        success = True
    else:
        print("Estimated calculation test of Simpson integration was not sucessful.")
    
    print("Simpson integration estimate:",I)
    print("Analytical result:", calculate_f)
    print("Error:", error)
    return success

#define Monte_Carlo test function
def monte_carlo_test():
    I,dI=monte_carlo_integration(test_fun,0.,np.pi/2,10,100)                      #calling the Monte Carlo integration function in testing function
    success = False
    if(dI < 1):                                                                   #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation of Monte Carlo was sucessful.")
        success = True
    else:
        print("Estimated calculation of Monte Carlo was not sucessful.")
    print(' Integrated value: {0:0.5f} +/- {1:0.5f}'.format(I,2*dI))
    
    return success


#define the main function in which to call the test function
def main():
    """ Calling testing functions to check if the integration and 
    diferentiation are working """
    test_first_derivative()
    test_second_derivative()
    riemann_integration_test()
    trapezoid_integration_test()
    sipmson_integration_test()
    monte_carlo_test()
    
#call main() function and execute the program
if __name__=="__main__":
    main()

