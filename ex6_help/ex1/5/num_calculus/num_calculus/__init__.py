from num_calculus.differentiation import first_derivative
import numpy as np

def test_fun(x):
    return np.sin(x)

def main():
    x = float(input("Enter the variable:"))
    dx = float(input("Enter the distance(the smaller the better)"))
    der = first_derivative(test_fun, x, dx)
    print("The first derivative of function sin(x) is:", der)

if __name__ == "__main__":
    main()
