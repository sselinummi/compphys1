#define first derivative
""" Define a python function first_derivative which takes in three arguments(function, x, dx).
    This function returns the estimated value of the first derivative."""
def first_derivative(function, x, dx):
    result = (function(x+dx)-function(x-dx))/(2*dx)
    return result
