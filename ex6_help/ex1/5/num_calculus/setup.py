from num_calculus.differentiation import first_derivative
from setuptools import setup

setup(name = 'num_calculus',
      version = '0.2',
      description = 'Numerical diferentiation and integration',
      author = 'Lena Matijasevic',
      author_email = 'lena.matijasevic@tuni.fi',
      licence = 'TUNI',
      packages = ['num_calculus'],
      zip_safe = False)




    






