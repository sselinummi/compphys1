""" Calculating integrals using nquad, quad, simps and trapz from Scipy library.
    1D, 2D and 3D integration. Testing functions are from exercise2"""

#import necessary libraries
import numpy as np
from scipy import integrate
import math
import scipy as sp

"""define testing functions"""
#function 1a.
def f(x):
    t = 2*x
    num = math.exp(-t)
    return x**2*num*np.sin(t)
#function 1b.
def f1(x):
    return np.sin(x)/x
#function 1c.
def f2(x):
    return math.exp(math.sin(x**3))
#function 2
def f3(x,y):
    return x*np.exp(-np.sqrt(x**2+y**2))
#function 3
def f4(x, y):
    return x*np.exp(-np.sqrt(x**2+y**2))

def f5(x, y, z):
    return ( 1./np.sqrt(np.pi)*( np.exp(-np.sqrt((x+1)**2+y**2+z**2)) + np.exp(-np.sqrt((x-1)**2+y**2+z**2)) ) )**2
#range used in nquad function
def range_1():
    return [0, np.inf]
def range_2():
    return [0, 1]
def range_3():
    return [0, 5]

""" define solutions to the functions """
#calculate integrals of function 1a using scipy.integration
def function_1a():
    b = np.inf  #upper integral bound
    Iq,Iqerr = integrate.quad(f, 0, b)  #quad integration in range 0 to inf
    Inq,Inqerr = integrate.nquad(f,[range_1]) #nquad integration in range 0 to inf
    f1 = lambda x : x**2*np.exp(-2*x)*np.sin(2*x) #lambda function
    print('1_a')
    print('{0:14} {1:14} {2:14}'.format('simpson', 'trapezoid', 'num points')) 
    arr1 = np.array([10, 20, 40, 100, 200, 400])   #array of  different numbers of integration nodes
    for a in arr1:   #go through the array and callculate integral for different number of integration nodes
        x1 = np.linspace(0,10,a)
        Is1= integrate.simps(f1(x1), x1) #call scipy simps 
        It1= integrate.trapz(f1(x1), x1) #call scipy trapz 
        print('{0:12.10f} {1:12.10f} {2:12}'.format(Is1, It1, a))
    print('quad = {0:0.4f} +/- {1:0.10f} '.format(Iq, Iqerr))
    print('nquad = {0:0.4f} +/- {1:0.10f} '.format(Inq, Inqerr))

#define solution for function 1b using scipy.integration
def function_1b():
    Iq, Iqerr = integrate.quad(f1,0,1) #quad integration in range 0 to 1
    Inq, Inqerr = integrate.nquad(f1,[range_2]) #nquad integration inrange 0 to 1
    fun2 = lambda x : np.sin(x)/x #lambda function
    arr1 = np.array([10, 20, 40, 100, 200, 400])#array of  different numbers of integration nodes
    print('1_b')
    print('{0:14} {1:14} {2:14}'.format('simpson', 'trapezoid', 'num points')) 
    for a in arr1:  #go through the array and callculate integral for different number of integration nodes
        x2 = np.linspace(1e-12,1,a) # use numeric zero for interval 0 to 1
        Is2 = integrate.simps(fun2(x2), x2) #call scipy simps
        It2 = integrate.trapz(fun2(x2), x2) #call scipy trapz
        print('{0:12.10f} {1:12.10f} {2:12}'.format(Is2, It2, a))
    print('quad = {0:0.4f} +/- {1:0.16f} '.format(Iq, Iqerr))
    print('nquad = {0:0.4f} +/- {1:0.16f} '.format(Inq, Inqerr))
    
#define solution for function 1c using scipy.integration
def function_1c():
    Iq, Iqerr = integrate.quad(f2,0,5) #quad integration inrange from 0 to 5
    Inq, Inqerr = integrate.nquad(f2, [range_3])#nquad integration in range 0 to 5
    fun3 = lambda x : np.exp(np.sin(x**3))    # lambda function
    arr1 = np.array([10, 20, 40, 100, 200, 400])#array of  different numbers of integration nodes
    print('1_c')
    print('{0:14} {1:14} {2:14}'.format('simpson', 'trapezoid', 'num points')) 
    for a in arr1: #go through the array and callculate integral for different number of integration nodes
        x3 = np.linspace(0,5,a)
        Is3 = integrate.simps(fun3(x3), x3) #call scipy simps
        It3 = integrate.trapz(fun3(x3), x3) #call scipy trapz
        print('{0:12.10f} {1:12.10f} {2:12}'.format(Is3, It3, a))
    print('quad = {0:0.4f} +/- {1:0.10f} '.format(Iq, Iqerr))
    print('nquad = {0:0.4f} +/- {1:0.10f} '.format(Inq, Inqerr))
    
#define soultion for 2d function using scipy.integration
def function_2():    
    f = lambda y, x: x*np.exp(-np.sqrt(x**2+y**2)) #lambda function
    I2d, I2derr = integrate.dblquad(f, 0, 2, lambda x: (-2), lambda x: 2)#quad 2d soultion with dblquad
    print('2')
    print('{0:14} {1:14} {2:14}'.format('simpson', 'trapezoid', 'num points')) 
    arr1 = np.array([10, 20, 40, 100, 200, 400]) #array of  different numbers of integration nodes
    for a in arr1: #go through the array and callculate integral for different number of integration nodes
        x = np.linspace(0, 2, a) #x variable
        y = np.linspace(-2, 2, a) #y variable
        #the ingreation order is not important here.
        #the integration was carried in order dx, dy
        Isx = integrate.simps(f4(x[:,None], y[None,:]), x) #callculate simps for x
        Is = integrate.simps(Isx, y) #callculate simps for y
        Itx = integrate.trapz(f4(x[:,None], y[None,:]), x) #callculate trapz for x
        It = integrate.simps(Itx, y) #callculate trapz for y
        print('{0:12.10f} {1:12.10f} {2:12}'.format(Is, It, a))
    print('nquad = {0:0.4f} +/- {1:0.10f}  '.format(I2d, I2derr))
    
#define solution for 3d function using scipy integration   
def function_3():
    f1 = lambda z, y, x: ( 1./math.sqrt(math.pi)*( math.exp(-math.sqrt((x+1)**2+y**2+z**2)) + math.exp(-math.sqrt((x-1)**2+y**2+z**2)) ) )**2 #lambda function
    I3d, I3derr = integrate.tplquad(f1, -10, 10, lambda x: -10, lambda x: 10, #quad solution usin tplquad
                  lambda x, y: -10, lambda x, y: 10)
    print('3')
    print('{0:14} {1:14} {2:14}'.format('simpson', 'trapezoid', 'num points'))
    arr1 = np.array([10, 20, 40, 100, 200, 400]) #array of  different numbers of integration nodes(execution times for a = 200 and a = 400 are relatively long 
    for a in arr1: #go through the array and callculate integral for different number of integration nodes
        x = np.linspace(-10, 10, a) #x variable in range from -10 to 10
        y = np.linspace(-10, 10, a) #y variable in range from -10 to 10
        z = np.linspace(-10, 10, a) #z variable in range from -10 to 10
        #the ingreation order is not important here.
        #the integration was carried in order dx, dy, dz
        Isx = integrate.simps(f5(x[:,None,None], y[None,:, None], z[None,None,:]), x) #callculate simps for x
        Isy = integrate.simps(Isx, y) #callculate simps for y
        Isz = integrate.simps(Isy, z) #callculate simps for z and get the integral calculaion
        Itx = integrate.trapz(f5(x[:,None,None], y[None,:, None], z[None,None,:]), x) #callculate trapz for x
        Ity = integrate.simps(Itx, y) #callculate trapz for y
        Itz = integrate.simps(Ity, z) #callculate trapz for z and get the integral calculation
        """
        Alternative formulated solution for 3D function
        #calculate f(x,y,z) values on a 3D x-y-z grid with a*a*a elements
        f = np.zeros(((a,a,a))) #zero the vector of integration nodes
        i=0;
        for xx in x:
            j=0;
            for yy in y:
                k=0;
                for zz in z:
                    f[i,j,k] = (1./np.sqrt(np.pi)*( np.exp(-np.sqrt((xx+1)**2+yy**2+zz**2)) + np.exp(-np.sqrt((xx-1)**2+yy**2+zz**2)) ) )**2
                    k=k+1;
                j=j+1;
            i=i+1;
        
        Itz = np.trapz(np.trapz(np.trapz(f,z),y),x)  #call scipy trapz
        Isz = integrate.simps(integrate.simps(integrate.simps(f,z),y),x)  #call scipy simps
      
        """
        print('{0:12.10f} {1:12.10f} {2:12}'.format(Isz, Itz,a))
    print('nquad = {0:0.4f} +/- {1:0.10f} '.format(I3d, I3derr))

#define main function to call the calculations
def main():
    function_1a()
    print()
    function_1b()
    print()
    function_1c()
    print()
    function_2()
    print()
    function_3()
#call main function
main()

