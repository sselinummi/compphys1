"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to Computational Physics 1
exercise 2 assignments at TAU.

By Ilkka Kylanpaa on January 2019
(modified Jan 2022)
"""

#import needed libraries
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


"""
Add basis functions l1 and l2 here
"""
#added basis functions per lecture notes
def l1(t):
    return 1-t 
def l2(t):
    return t

"""Define class linear interpolation """
class linear_interp:
    #initialization of class linear_interp 
    def __init__(self,*args,**kwargs): 
        self.dims=kwargs['dims']
        if (self.dims==1):
            self.x=kwargs['x']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
        elif (self.dims==2):
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
            self.hy=np.diff(self.y)
        elif (self.dims==3):
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.z=kwargs['z']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
            self.hy=np.diff(self.y)
            self.hz=np.diff(self.z)
        else:
            print('Either dims is missing or specific dims is not available')
            
    #calcualte interpolated values of a 1D function f(x) given by dataset (xi,fi)
    def eval1d(self,x):
        if np.isscalar(x): #check if the type of x is scalar type
            x=np.array([x]) #array of x values
        N=len(self.x)-1 #last index in the x array
        f=np.zeros((len(x),)) #initialize f(could be anything, not 0)
        ii=0
        #for all interpolation nodes find interpolated function values using linear interpolation method
        for val in x: 
            i=np.floor(np.where(self.x<=val)[0][-1]).astype(int) #return the largest int type of x
            if i==N: 
                f[ii]=self.f[i] #avoid extrapolation
            else:
                t=(val-self.x[i])/self.hx[i] #calculate factor t where t = (x − xi)/(xi+1 − xi)
                f[ii]=self.f[i]*l1(t)+self.f[i+1]*l2(t) #calculate approximated function value
            ii+=1 
        return f
    
    #calcualte interpolated values of a 2D function f(x,y) given by dataset (xi,yi,fi) 
    def eval2d(self,x,y):
        if np.isscalar(x): #check if the type of x is scalar type
            x=np.array([x]) #array of xi
        if np.isscalar(y): #check if the type of y is scalar type
            y=np.array([y]) #array of yi
        Nx=len(self.x)-1  #last index in the x array
        Ny=len(self.y)-1   #last index in the y array
        f=np.zeros((len(x),len(y))) #initialize f(could be anything, not 0)
        A=np.zeros((2,2)) #initialize matrix A
        ii=0
        #for all interpolation nodes find interpolated function values using linear interpolation method
        for valx in x:
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int) #return the largest type of int x
            if (i==Nx): #avoid extrapolation
                i-=1 
            jj=0 
            for valy in y: 
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int)#return the largest type of int y
                if (j==Ny): #avoid extrapolation
                    j-=1 
                tx = (valx-self.x[i])/self.hx[i] #calculate factors tx
                ty = (valy-self.y[j])/self.hy[j] #calculate factors ty
                ptx = np.array([l1(tx),l2(tx)]) #array of basis function of tx
                pty = np.array([l1(ty),l2(ty)]) #array of basis function of ty
                A[0,:]=np.array([self.f[i,j],self.f[i,j+1]]) #calculate matrix Ak first row
                A[1,:]=np.array([self.f[i+1,j],self.f[i+1,j+1]])#calculate matrix Ak second row
                f[ii,jj]=np.dot(ptx,np.dot(A,pty))#calculate approximated function value
                jj+=1 
            ii+=1 
        return f
    #end eval2d
    #calcualte interpolated values of a 3D function f(x,y,z) given by dataset (xi,yi,zi,fi)
    def eval3d(self,x,y,z):
        if np.isscalar(x): #check if x is a scalar type
            x=np.array([x])#array of xi
        if np.isscalar(y): #check if y is a scalar type 
            y=np.array([y])#array of yi
        if np.isscalar(z): #check if z is a scalar type
            z=np.array([z])#array of zi
        Nx=len(self.x)-1  #last index in the x array
        Ny=len(self.y)-1   #last index in the y array
        Nz=len(self.z)-1  #last index in the z array
        f=np.zeros((len(x),len(y),len(z))) #initialize f(could be anything, not 0)
        A=np.zeros((2,2)) #initialize matrix A
        B=np.zeros((2,2)) #initialize matrix B
        ii=0
        #for all interpolation nodes find interpolated function values using linear interpolation method
        for valx in x:
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int) #return the largest type of int x
            if (i==Nx): #avoid extrapolation
                i-=1
            jj=0 
            for valy in y:
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int) #return the largest type of int y
                if (j==Ny): #avoid extrapolation
                    j-=1
                kk=0 
                for valz in z:
                    k=np.floor(np.where(self.z<=valz)[0][-1]).astype(int) #return the largest type of int z
                    if (k==Nz):#avoid extrapolation
                        k-=1
                    #calculate factors tx, ty, tz
                    tx = (valx-self.x[i])/self.hx[i] 
                    ty = (valy-self.y[j])/self.hy[j]
                    tz = (valz-self.z[k])/self.hz[k]
                    #create an array of basis function l1 and l2
                    ptx = np.array([l1(tx),l2(tx)])
                    pty = np.array([l1(ty),l2(ty)])
                    ptz = np.array([l1(tz),l2(tz)])
                    # callculate the matrixes Ak and Ak+1
                    B[0,:]=np.array([self.f[i,j,k],self.f[i,j,k+1]]) #calculate matrix Ak first row
                    B[1,:]=np.array([self.f[i+1,j,k],self.f[i+1,j,k+1]])#calculate matrix Ak second row
                    A[:,0]=np.dot(B,ptz)
                    B[0,:]=np.array([self.f[i,j+1,k],self.f[i,j+1,k+1]]) #calculate matrix Ak+1 first row
                    B[1,:]=np.array([self.f[i+1,j+1,k],self.f[i+1,j+1,k+1]])#calculate matrix Ak+1 second row
                    A[:,1]=np.dot(B,ptz)
                    f[ii,jj,kk]=np.dot(ptx,np.dot(A,pty))#calculate approximated function value
                    kk+=1
                jj+=1
            ii+=1
        return f
    #end eval3d
# end class linear interp

    
def main():
    #plot the functions in 1D, 2D and 3D
    plt.rcParams['pcolor.shading']='auto'

    # 1d example
    fig1d = plt.figure() #create a figure object
    ax1d = fig1d.add_subplot(111)#create 1x1 grid
    x=np.linspace(0.,2.*np.pi,10) #define x
    y=np.sin(x) #define the test function
    lin1d=linear_interp(x=x,f=y,dims=1) #call linear interpolation class
    xx=np.linspace(0.,2.*np.pi,100) #define interpolation interval
    ax1d.plot(xx,np.sin(xx),'b',label='exact')#plot exact value of function 
    ax1d.plot(x,y,'o',label='points')#plot the points
    ax1d.plot(xx,lin1d.eval1d(xx),'r--',label='lin. interp')#plot linear intrepolaton
    ax1d.set_title('linear interpolation in 1D') #set title
    ax1d.legend()

    # 2d example
    fig2d = plt.figure()  #create a figure object
    ax2d = fig2d.add_subplot(221, projection='3d') #create 2x2 first subplot
    ax2d2 = fig2d.add_subplot(222, projection='3d')#create 2x2 second subplot
    ax2d3 = fig2d.add_subplot(223)#create 2x2 third subplot
    ax2d4 = fig2d.add_subplot(224)#create 2x2 fourth subplot

    x=np.linspace(-2.0,2.0,11) #define x variable for true value of f
    y=np.linspace(-2.0,2.0,11) #define y variable for true value of f
    X,Y = np.meshgrid(x,y)#returns 2-D grid coordinates based on the coordinates contained in vectors x and y
    Z = X*np.exp(-1.0*(X*X+Y*Y)) #define test function
    ax2d.plot_wireframe(X,Y,Z)# takes a grid of values and project it 
    ax2d3.pcolor(X,Y,Z)
    ax2d.set_title('original') #set the title for the true value plot

    lin2d=linear_interp(x=x,y=y,f=Z,dims=2) #call class linear interpolation
    #plot the interpolated value
    x=np.linspace(-2.0,2.0,51) 
    y=np.linspace(-2.0,2.0,51)
    X,Y = np.meshgrid(x,y)
    Z = lin2d.eval2d(x,y)
     
    ax2d2.plot_wireframe(X,Y,Z)
    ax2d4.pcolor(X,Y,Z)
    ax2d2.set_title('interpolated')#set title for interpolated plot
    
    # 3d example
    x=np.linspace(0.0,3.0,10) #x variable for true value of f
    y=np.linspace(0.0,3.0,10) #y variable for true value of f
    z=np.linspace(0.0,3.0,10) #z variable for true value of f
    X,Y,Z = np.meshgrid(x,y,z) #returns 3-D grid coordinates based on the coordinates contained in vectors x,y and z
    F = (X+Y+Z)*np.exp(-1.0*(X*X+Y*Y+Z*Z)) #define test function
    X,Y = np.meshgrid(x,y) #returns 2-D grid coordinates based on the coordinates contained in vectors x and y
    fig3d=plt.figure() #create a figure object
    ax=fig3d.add_subplot(121) #create 1x2 first subplot
    ax.pcolor(X,Y,F[...,int(len(z)/2)])
    ax.set_title('original (from 3D data)') #set title for original 3D

    lin3d=linear_interp(x=x,y=y,z=z,f=F,dims=3) #call linear interpolation class
    #plot 3d linear intrepolation
    x=np.linspace(0.0,3.0,50)
    y=np.linspace(0.0,3.0,50)
    z=np.linspace(0.0,3.0,50)
    X,Y = np.meshgrid(x,y)
    F=lin3d.eval3d(x,y,z)
    ax2=fig3d.add_subplot(122)
    ax2.pcolor(X,Y,F[...,int(len(z)/2)])
    ax2.set_title('linear interp. (from 3D data)') #set title for interpolated plot

    plt.show() #show plots
#end main

#call main function
if __name__=="__main__":
    main()
