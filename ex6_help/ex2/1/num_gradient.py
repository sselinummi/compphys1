"""Implement an N -dimensional numerical gradient. """

#import needed libraries
import numpy as np
from numpy import linalg as LA

#function to use to calculate the gradiant
#test function 1 from exercise2
def f(x,N):
    fun = np.sin(x[0])+np.cos(x[N-1])
    for i in range(1,N-1):
        fun = fun+x[i]**2
    return fun
#test function 2-random test function 
def f1(x,N):
    fun = x[2]*np.sin(x[0])+x[1]*np.cos(x[2])+x[0]*x[1]**2*x[2]
    return fun
#function to calculate the true value of test function 1 gradient
def f_true(x,N):
    fun = np.zeros(N)
    fun[0] = np.cos(x[0])
    fun[N-1] = -np.sin(x[N-1])
    for i in range(1,N-1):
        fun[i] = x[i]*2
    return fun
#function to calculate the true value of test function 2 gradient
def f1_true(x, N):
    fun = np.zeros(N)
    fun[0] = x[2]*np.cos(x[0]) + x[2]*x[1]**2
    fun[1] = np.cos(x[2])+2*x[1]*x[0]*x[2]
    fun[2] = np.sin(x[0]) - np.sin(x[2])*x[1]+x[0]*x[1]**2
    return fun

"""Define gradient function with values for function, N-number of dimensions, x0 - gradient calculation point, dx - variable increment(same for all x) """
def gradient(fun, N,x0, dx):
    df = np.zeros(N) #initialize vector df
    for i in range(0,N):
        ddx= np.zeros(N) #initialize vector of increments
        ddx[i] = dx #increment only  i-th independent variable
        df[i] = (fun(x0+ddx,N)-fun(x0-ddx,N))/(2*dx) #calculate partial derivative per i-th independent variable
    return df 

"""Define the test function and calculate the relative error of estimation """
def test_gradient():
    N = 3  #define number of dimensions
    arr = np.linspace(1,10,N) #array of dimensions of function f whose gradient is calculated 
    res_estimated1 = gradient(f, N, arr, 0.01) #call gradient function and calculate the estimated error
    res_true1 = f_true(arr, N) #calculate the true values of the test function gradient
    rel_error1 = LA.norm(res_true1-res_estimated1)/LA.norm(res_true1)*100 #calculate the relative error
    res_estimated2 = gradient(f1, N, arr, 0.01)
    res_true2 = f1_true(arr, N)
    rel_error2 = LA.norm(res_true2-res_estimated2)/LA.norm(res_true2)*100
    success = False
    if(rel_error1 < 0.01):  #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test was successful.")
        success = True
    else:
        print("Estimated calculation test was not successful.")
    if(rel_error2 < 0.01):  #check if the error is lower than the given tolerance and return a bool value
        print("Estimated calculation test was successful.")
        success = True
    else:
        print("Estimated calculation test was not successful.")
        
#define main function to call the tests
def main():
    test_gradient()

#call main function
main()
