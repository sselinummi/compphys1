"""Steepest/gradient descent function """
#import needed libraries
import numpy as np
from numpy import linalg as LA

"""
Function for which we are calculating min
def f(x, N):
    fun = 0
    for i in range(0, N):
        fun = fun + x[i]**2
    return fun
"""
#define function returning gradient
def grad(x,N):
    g = np.zeros(N) #initialize gradient vector
    for i in range(0,N):
        g[i] = 2*x[i] #calculate gradient
    return g
#define steepest descent function
def steepest(N, x0, a):
    ng = 1000; #initial gradient norm(purposely large)
    tol=1e-7 #stopping tolerance
    xN=x0 #initialize guess for solution
    while(ng>tol): #descend in the minus gradient direction as long as gradient_norm > tolerance
        g1 = grad(xN,N)
        ng = LA.norm(g1) 
        gamma = a/(ng+1) #calculate factor which determines speed of descending
        xN1 = xN-gamma*g1 #update solution
        xN=xN1
    return xN
#define test function to calculate the minimum of f(x)=0 and related x   
def test_steepest():
    arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1] #vector
    N = 10 #number of dimensions
    xmin = steepest(N, arr, 0.001) #call steepest function 
    print("Minimum of f(x) is located at:")
    print(xmin)
    
#deine main function to call test function 
def main():
    test_steepest()
    
#call main
main()
