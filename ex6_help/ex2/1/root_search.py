""" Implementing the bisection method to find between which interpolation nodes the current x* belongs.
    The result will be index i, meaning that x[i] <= x* < x[i+1] """
#import libraries
import numpy as np
import math

#define search function
def search(v,x):
    r = len(x)-1 #the max search index
    while(v > x[r] or v < 0): # check if the value is out of grid and call the program again
        print("Error! Search value out of grid!")
        v = float(input("Enter the value to search:"))    
    l = 0 #left min search index
    i = -1 #index value 
    while(l<=r): #check if the index is between min and max
        m = math.floor((l+r)/2.) #bisection method to lower the search grid
        if(x[m]<v): #checking if the index is in the grid
            l = m+1
        else:
            r = m-1
        if(x[m] <=v and v < x[m+1]):#if index in grid, index is found
           i = m
    return i #return index

#define linear grid using linspace and search the index of a given value
def problem_1():
    x = np.linspace(0,100,100)
    v = float(input("Enter the value to search(linear grid):"))
    i = search(v,x)
    print("Index is:", i)

#define logarithmic grid and search the index of a given value
def problem_2():
    dim = 100
    r = np.zeros(dim)
    rmax = 100
    r0 = 1e-5    
    h = math.log(rmax /r0 + 1)/(dim-1)
    for i in range(1, dim):
        r[i]=r0*(np.exp(i*h)-1)
    v = float(input("Enter the value to search(logarithmic grid):"))
    print("Index is",i)

#define main function to call the problem functions
def main():
    problem_1()
    problem_2()
#call main
main()
