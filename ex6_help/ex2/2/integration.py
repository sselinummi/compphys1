"""
Performing calculations of 1-,2- and 3-dimensional integrals using 
trapz, simps, quad and nquad functions from scipy.integrate.
"""

import numpy as np

from scipy.integrate import trapz
from scipy.integrate import simps
from scipy.integrate import quad
from scipy.integrate import nquad

def one_dimensional_int():
    """
    Function calculating estimates for the 1d integral in 3 different 
    cases using trapezoid rule, Simpsons integral and scipy's nquad 
    function. Estimates are calculated using different amounts of
    intervals.
    """
    
    num_points=[10,20,40,100,200,400]

    print('1a')
    print('for the first function')
    print('{0:20} {1:15} {2:15}'.format('trapezoid','simpson','num_points'))
    for num_p in num_points:
        """
        function goes close to zero with larger values so to get better
        results with low num_points, 10 is used instead of inf
        """
        r1=np.linspace(0,10,num_p) 
        I_t=trapz(fun1(r1), r1)
        I_s=simps(fun1(r1),r1)
        print('{0:15.10f} {1:15.10f} {2:15}'.format(I_t,I_s,num_p))
    
    I_quad=quad(fun1,0,np.inf)
    print('quad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_quad[0],I_quad[1]))
    print()

    
    print('for the second function')
    print('{0:20} {1:15} {2:15}'.format('trapezoid','simpson','num_points'))
    for num_p in num_points:
        x1=np.linspace(1e-12,1,num_p) # using 1e-12 to approximate zero
        I_t=trapz(fun2(x1), x1)
        I_s=simps(fun2(x1),x1)
        print('{0:15.10f} {1:15.10f} {2:15}'.format(I_t,I_s,num_p))
    
    I_quad=quad(fun2,0,1)
    print('quad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_quad[0],I_quad[1]))
    print()


    print('for the third function')
    print('{0:20} {1:15} {2:15}'.format('trapezoid','simpson','num_points'))
    for num_p in num_points:
        x2=np.linspace(0,5,num_p)
        I_t=trapz(fun3(x2),x2)
        I_s=simps(fun3(x2),x2)
        print('{0:15.10f} {1:15.10f} {2:15}'.format(I_t,I_s,num_p))
    
    I_quad=quad(fun3,0,5)
    print('quad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_quad[0],I_quad[1]))
    print()



def two_dimensional_int():
    """
    Function calculating estimates for the 2d integral using 
    trapezoid rule, Simpsons integral and scipy's nquad 
    function. Estimates are calculated using different amounts of
    intervals.
    """
    
    num_points=[10,20,40,100,200,400]
    print('1b')
    print('{0:20} {1:15} {2:15}'.format('trapezoid','simpson','num_points'))
    for num_p in num_points:
        x=np.linspace(0,2,num_p)
        y=np.linspace(-2,2,num_p)
        X, Y = np.meshgrid(x,y)
        I_t=trapz(trapz(fun4(X,Y), x),y)
        I_s=simps(simps(fun4(X,Y), x),y)
        print('{0:15.10f} {1:15.10f} {2:15}'.format(I_t,I_s,num_p))
    
    I_nquad=nquad(fun4,[[0,2],[-2,2]])
    print('nquad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_nquad[0],I_nquad[1]))
    print()


def three_dimensional_int():
    """
    Function calculating estimates for the 3d integral using 
    trapezoid rule, Simpsons integral and scipy's nquad 
    function. Estimates are calculated using different amounts of
    intervals.
    """
    
    num_points=[10,20,40,100,200,400]
    print('1c')
    print('{0:20} {1:15} {2:15}'.format('trapezoid','simpson','num_points'))
    for num_p in num_points:
        x=np.linspace(-10,10,num_p)
        y=np.linspace(-10,10,num_p)
        z=np.linspace(-10,10,num_p)
        X, Y, Z = np.meshgrid(x,y,z)
        I_t=trapz(trapz(trapz(fun5(X,Y,Z), x),y),z)
        I_s=simps(simps(simps(fun5(X,Y,Z), x),y),z)
        print('{0:15.10f} {1:15.10f} {2:15}'.format(I_t,I_s,num_p))
    
    I_nquad=nquad(fun5,[[-10,10],[-10,10],[-10,10]])
    print('nquad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_nquad[0],I_nquad[1]))
    print()

def fun1(r):
    # 1-d function 1 for part a
    return (r**2)*np.exp(-2*r)*np.sin(2*r)

def fun2(x):
    # 1-d function 2 for part a
    return np.sin(x)/x

def fun3(x):
    # 1-d function 3 for part a
    return np.exp(np.sin(x**3))

def fun4(x,y):
    # 2-d function for part b
    return x*np.exp(-np.sqrt(x**2+y**2))

def fun5(x,y,z):
    # 3-d function for part c
    return np.abs(1/np.sqrt(np.pi)*(np.exp(-np.sqrt((x+1)**2+y**2+z**2))+np.exp(-np.sqrt((x-1)**2+y**2+z**2))))**2


def main():
    # main function that calls the integral functions
    one_dimensional_int()
    two_dimensional_int()
    three_dimensional_int()
    
if __name__=="__main__":
    main()