"""
Calculating the numerical gradient of n-dimensional function.
Comparing the numerical value against the exact value in 3d case.
"""

import numpy as np

def gradient( f, x, h ):
    """
    N-dimensional numerical gradient using equation
    f'(x)=(f(x+h)-f(x))/h for calculating the partial derivative
    for each component and forming the gradient
    f : N-variable input function
    x : array
    h : displacement

    """
    N=len(x)
    result=[None]*(N)
    temp_x=x.copy()
    for i in range(0,N):
        temp_x[i]+=h
        result[i]=(f(temp_x)-f(x))/h
        temp_x[i]-=h

    return result

def test_gradient():
    # testing numerical value of the gradient against the exact
    x=np.array([0.9,0.9,0.9])
    h=0.001
    result=gradient(fun,x,h)
    exact=three_n_gradient(x)
    print('Numerical value')
    print(result)
    print('Exact value')
    print(exact)


def fun(x):
    # function used for testing
    N=len(x)-1
    return np.sin(x[0])+np.cos(x[N])+sum(i**2 for i in x[1:N])

def three_n_gradient(x):
    # known gradient of f=sin(x)+cos(z)+y²
    return [np.cos(x[0]),2*x[1],-np.sin(x[2])]


def main():
    # main function that calls the test
    test_gradient()
    
if __name__=="__main__":
    main()