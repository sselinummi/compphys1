import logging

import numpy as np


def grad_descent(f, xs, intial_guess=np.array([]), etol=10e-8, max_iter=10000, lr=0.001, log=logging.Logger):
    """
    Classical gradient descent. Find minimum of a function f.

    :param f: np.array function values
    :param xs: steps that were used to generate f
    :param intial_guess: where to initialize x
    :param etol: error tolerance
    :param max_iter: maximal number of updates
    :param lr: learning rate
    :param log: logger for printing
    :return:
    """

    # If initial guesses were not provided
    if intial_guess.size == 0:
        intial_guess = np.zeros_like(f)

    grads = np.gradient(f)

    dims = f.ndim
    error = 100
    max_count = 0
    x = intial_guess
    while error > etol and max_count < max_iter:

        inds = []
        # find indices
        for i in range(dims):

            # coordinate is in the array
            if x[i] in xs[i]:
                ind = np.where(xs[i] == x[i])
                ind = int(ind[0])

            # find the closest index
            else:
                ind = np.digitize(x[i], xs[i], right=True)

            inds.append(ind)

        # find gradients respectively to indices
        grad_vals = np.zeros_like(x)
        for i, grad_map in enumerate(grads):
            iter_mesh = grad_map
            for ind in inds:
                iter_mesh = iter_mesh[ind]
            grad_val = iter_mesh
            grad_vals[i] = grad_val

        print(x)
        log.debug(x)

        # find new x
        xnew = x - lr*grad_vals

        error = np.sum(abs(xnew - x))
        x = xnew

        max_count += 1

    return x
