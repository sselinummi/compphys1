import numpy as np
import logging
import unittest
from gradient_descent.steepest_descent import grad_descent


def _func(x):
    x = np.power(x, 2)
    x = np.sum(x, axis=-1)
    return x


class TestGradientDescent(unittest.TestCase):
    def test_gradient_descent3d(self):
        """ Test with 3 dimensional function """

        log = logging.getLogger("TestGradientDescent,test_gradient_descent")
        log.debug("")

        x1 = np.linspace(-2, 2, 50, dtype=np.float16)
        xs = [x1, x1, x1]

        xx1, xx2, xx3 = np.meshgrid(
            x1, x1, x1, indexing='ij')

        xN = np.stack((xx1, xx2, xx3), axis=-1)

        f = _func(xN)
        init = np.ones(xN.shape[-1])

        minimum_estimate = grad_descent(f, xs, init, etol=10e-4, max_iter=10000, log=log, lr=0.05)

        if np.sum(abs(minimum_estimate)) < 0.2:
            print("Test Pass")
            log.debug("Test Pass")
        else:
            print("Test fail")
            log.debug("Test fail")


    def test_gradient_descent2d(self):
        """ Test with 2 dimensional function """

        log = logging.getLogger("TestGradientDescent,test_gradient_descent2d")
        log.debug("")

        x1 = np.linspace(-3, 3, 100)
        x2 = np.linspace(-3, 3, 100)
        xs = [x1, x2]

        xx1, xx2 = np.meshgrid(x1, x2, indexing='ij')

        x12 = np.stack((xx1, xx2), axis=-1)
        xc = x12[..., 0]    # x1 mesh grid
        ff = x12[0, 0]      # first element (0, 0) of the matrix

        f = _func(x12)
        init = np.ones(x12.shape[-1])
        minimum_estimate = grad_descent(f, xs, init, etol=10e-6, max_iter=500, log=log)

        if np.sum(abs(minimum_estimate)) < 0.2:
            print("Test Pass")
            log.debug("Test Pass")
        else:
            print("Test fail")
            log.debug("Test fail")

        print()
