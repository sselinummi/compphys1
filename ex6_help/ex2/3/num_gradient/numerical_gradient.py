import numpy as np


def estimate_gradient(f, x=np.array([]), axis=0, h=1):
    """
    Estimate gradient numerically. The method works only wiht a fixed step size. The method has some issues,
    see tests.

    :param f: np.array of function values
    :param x: steps if available
    :param axis: axis on which to calculate on f
    :param h: step size
    :return: estimated gradient as np.array
    """

    #f_forward = f[2:]
    #f_back = f[:-2]

    if x.ndim > 1:
        x = x.reshape(x.shape[0])

    #result = np.zeros_like(f)

    # Use slicing to select correct elements from f
    N = f.ndim
    slice1 = [slice(None)]*N
    slice2 = [slice(None)]*N
    slice3 = [slice(None)]*N
    slice4 = [slice(None)]*N
    slice5 = [slice(None)] * N
    slice6 = [slice(None)] * N
    slice7 = [slice(None)] * N
    slice8 = [slice(None)] * N

    slice1[axis] = slice(1, -1)
    slice2[axis] = slice(None, -2)
    slice3[axis] = slice(1, -1)
    slice4[axis] = slice(2, None)
    slice5[axis] = slice(0, 1)          # first element
    slice6[axis] = slice(-1, None)      # last element
    slice7[axis] = slice(1, 2)          # second element
    slice8[axis] = slice(-2, -1)        # second last element


    out = np.empty_like(f, dtype=np.float64)

    if x.size == 0:

        # mid part, error O(h^2)
        out[tuple(slice1)] = (f[tuple(slice4)] - f[tuple(slice2)]) / (2 * h)
        # edges, error O(h)
        out[tuple(slice5)] = (f[tuple(slice7)] - f[tuple(slice5)]) / (2 * h)
        out[tuple(slice6)] = (f[tuple(slice6)] - f[tuple(slice8)]) / (2 * h)

        # This works only for 1d
        #dfdt_mid = (f_forward - f_back) / (2 * h)
        #dfdt_start = (f[1] - f[0]) / (2*h)
        #dfdt_end = (f[-2] - f[-1]) / (2*h)

    # What if we have information about grid spacing ?
    """
    else:
        dx1 = x[0:-1]
        dx2 = x[1:]
        a = -(dx2) / (dx1 * (dx1 + dx2))
        b = (dx2 - dx1) / (dx1 * dx2)
        c = dx1 / (dx2 * (dx1 + dx2))
        # fix the shape for broadcasting
        shape = np.ones(N, dtype=int)
        shape[axis] = -1
        a.shape = b.shape = c.shape = shape
        # 1D equivalent -- out[1:-1] = a * f[:-2] + b * f[1:-1] + c * f[2:]
        aa = f[tuple(slice2)]
        bb = f[tuple(slice3)]
        cc = f[tuple(slice4)]
        out[tuple(slice1)] = a * f[tuple(slice2)] + b * f[tuple(slice3)] + c * f[tuple(slice4)]

        dfdt_mid = (f_forward - f_back) / (x[2:, axis] - x[:-2, axis] )
        dfdt_start = (f[1] - f[0]) / (x[1, axis] - x[0, axis])
        dfdt_end = (f[-1] - f[-2]) / (x[-1, axis] - x[-2, axis])
    """
    #result[0] = dfdt_start
    #result[-1] = dfdt_end
    #result[1:-1] = dfdt_mid


    return out

