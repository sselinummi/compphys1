import numpy as np
import logging
import unittest
from num_gradient import numerical_gradient


def _func(x):
    return np.sin(x[..., 0]) + np.cos(x[..., -1]) + np.sum(np.power(x[..., 1:-1], 2))


def _func_gradient_x1(x):
    if x.ndim == 1:
        return np.cos(x) - np.sin(x)
    else:
        return np.cos(x[..., 0])

def _func_gradient_xN(x): return -np.sin(x[..., -1])
def _func_gradient_general(x, dim=1): return 2*x[..., dim]


class TestGradient(unittest.TestCase):
    """
    Test numerical gradient with a function with different dimensions.
    """

    def test_num_gradient3d(self):

        log = logging.getLogger("TestGradient,test_num_gradient")
        log.debug("")

        x1 = np.linspace(0, 6 * np.pi, 100)
        x2 = np.linspace(0, 6 * np.pi, 100)
        x3 = np.linspace(0, 6 * np.pi, 100)
        x = np.stack((x1, x2, x3), axis=-1)

        # Meshgrid
        xx, yy, zz = np.meshgrid(x1, x2, x3)
        X = np.stack((xx, yy, zz), axis=-1)
        ff = _func(X)


        true_gradient_x1 = _func_gradient_x1(X)
        true_gradient_x3 = _func_gradient_general(X, dim=1)
        true_gradient_x2 = _func_gradient_xN(X)

        step_size = x1[1]-x2[0]

        estimate_x1 = numerical_gradient.estimate_gradient(ff, axis=1, h=step_size)
        estimate_x2 = numerical_gradient.estimate_gradient(ff, axis=0, h=step_size)
        estimate_x3 = numerical_gradient.estimate_gradient(ff, axis=2, h=step_size)

        error1 = abs(true_gradient_x1 - estimate_x1)
        error_mean1 = np.mean(error1)
        print("Absolute mean error x1: {}".format(error_mean1))
        log.debug("Absolute mean error x1: {}".format(error_mean1))

        error2 = abs(true_gradient_x2 - estimate_x2)
        error_mean2 = np.mean(error2)
        print("Absolute mean error x2: {}".format(error_mean2))
        log.debug("Absolute mean error x2: {}".format(error_mean2))

        error2 = abs(true_gradient_x3 - estimate_x3)
        error_mean2 = np.mean(error2)
        print("Absolute mean error x3: {}".format(error_mean2))
        log.debug("Absolute mean error x3: {}".format(error_mean2))

    def test_num_gradient1d(self):

        log = logging.getLogger("TestGradient,test_num_gradient1d")
        log.debug("")

        x = np.linspace(0, 6 * np.pi, 100).reshape((100, 1))
        f = _func(x)
        true_gradient = _func_gradient_x1(x)
        step_size = x[1] - x[0]

        estimate = numerical_gradient.estimate_gradient(f, axis=0, h=step_size)

        error = abs(true_gradient - estimate)
        error_mean = np.mean(error)
        print("Absolute mean error: {}".format(error_mean))
        log.debug("Absolute mean error: {}".format(error_mean))

    def test_num_gradient2d(self):

        log = logging.getLogger("TestGradient,test_num_gradient2d")
        log.debug("")

        x1 = np.linspace(0, 6 * np.pi, 100)
        x2 = np.linspace(0, 6 * np.pi, 100)
        x = np.stack((x1, x2), axis=-1)

        # Meshgrid
        xx, yy = np.meshgrid(x1, x2)
        X = np.stack((xx, yy), axis=-1)
        ff = _func(X)

        true_gradient_respect_to_x1 = _func_gradient_x1(X)
        true_gradient_respect_to_x2 = _func_gradient_xN(X)

        step_size = x1[1] - x2[0]

        estimate_x1 = numerical_gradient.estimate_gradient(ff, axis=1, h=step_size)
        estimate_x2 = numerical_gradient.estimate_gradient(ff, axis=0, h=step_size)

        error1 = abs(true_gradient_respect_to_x1 - estimate_x1)
        error_mean1 = np.mean(error1)
        print("Absolute mean error x1: {}".format(error_mean1))
        log.debug("Absolute mean error x1: {}".format(error_mean1))

        error2 = abs(true_gradient_respect_to_x2 - estimate_x2)
        error_mean2 = np.mean(error2)
        print("Absolute mean error x2: {}".format(error_mean2))
        log.debug("Absolute mean error x2: {}".format(error_mean2))


