""" Calculate numerically integrals using scipy library """
from scipy.integrate import simps, trapz, quad, nquad
import numpy as np
from numba import njit, jit


def print_func(problem, results, npoints):
    """
    Print results.

    :param problem: problem set number
    :param results: results as np array (func, method, list of results)
    :param npoints: number of evaluation points
    :return:
    """
    print(problem)
    spacing = 16
    print("{:<{spacing}}{:<{spacing}}{:<{spacing}}".format("trapezoid", "simpson", "num points", spacing=spacing))
    for ind, func in enumerate(results):
        print("Integral number {}".format(ind + 1))
        for ind2, npoint in enumerate(npoints):
            print("{:<{spacing}}{:<{spacing}}{:<{spacing}}".format(round(func[1][ind2], 10), round(func[0][ind2], 10),
                                                                   npoint, spacing=spacing))
        print("quad = {:<{spacing}} +/- {:<{spacing}}".format(round(func[2][0], 10), round(func[2][1], 10),
                                                              spacing=spacing))


def one_dimensional(npoints):
    """
    Define integrable and integration limits and proceed to calculate 1D integrals

    :param npoints: number of evaluation points
    :return:
    """

    def func1(x): return np.power(x, 2)*np.exp(-2*x)*np.sin(2*x)
    def func2(x): return np.sin(x)/(x+1e-9)
    def func3(x): return np.exp(np.sin(np.power(x, 3)))
    funcs = [func1, func2, func3]

    eval_points = {"x1": [], "x2": [], "x3": []}
    for npoint in npoints:
        x1 = np.linspace(0, 100, npoint)
        x2 = np.linspace(0, 1, npoint)
        x3 = np.linspace(0, 5, npoint)

        eval_points["x1"].append(x1)
        eval_points["x2"].append(x2)
        eval_points["x3"].append(x3)

    results = calculate_integrals(funcs, eval_points, dims=1)
    print_func("1a", results, npoints)


def two_dimensional(npoints):
    """
    Define integrable and integration limits and proceed to calculate 2D integrals

    :param npoints: number of evaluation points
    :return:
    """

    def func1(x, y): return x*np.exp(-np.sqrt(np.power(x, 2) + np.power(y, 2)))
    funcs = [func1]

    eval_points = {"x1": []}
    for npoint in npoints:
        x1 = np.linspace(0, 2, npoint)
        y1 = np.linspace(-2, 2, npoint)

        coords = np.vstack((x1, y1)).transpose()
        eval_points["x1"].append(coords)

    results = calculate_integrals(funcs, eval_points, dims=2)
    print_func("1b", results, npoints)


def three_dimensional(npoints):
    """
    Define integrable and integration limits and proceed to calculate 3D integrals

    :param npoints: number of evaluation points
    :return:
    """

    def func(x, y, z):
        a = np.sqrt(np.power(x+1, 2) + np.power(y, 2) + np.power(z, 2))
        b = np.sqrt(np.power(x-1, 2) + np.power(y, 2) + np.power(z, 2))
        phi = 1/np.sqrt(np.pi) * (np.exp(-a) + np.exp(-b))
        return np.power(phi, 2)
    funcs = [func]

    eval_points = {"x1": []}
    for npoint in npoints:
        x1 = np.linspace(-10, 10, npoint)
        coords = np.vstack((x1, x1, x1)).transpose()

        eval_points["x1"].append(coords)

    results = calculate_integrals(funcs, eval_points, dims=3)
    print_func("1c", results, npoints)


def calculate_integrals(funcs, eval_points, dims=1):
    """
    Calculate integrals given the functions and evaluation points

    :param funcs: list of python functions
    :param eval_points: dictionary of evaluation points
    :param dims: number of dimensions in integrals
    :return: result array as np.array
    """

    keys = list(eval_points.keys())

    results = np.zeros((len(funcs), 3, len(eval_points[keys[0]])))

    for ind, func in enumerate(funcs):
        eval_list = eval_points["x{}".format(ind+1)]
        simp_list = []
        trap_list = []

        # simpson and trapezoid method
        for x in eval_list:
            if dims == 1:
                simp = simps(func(x), x)
                trap = trapz(func(x), x)
            elif dims == 2:

                # Generate grid values
                z = np.zeros((x.shape[0], x.shape[0]))
                for i, y in enumerate(x[:, 1]):
                    z[i] = func(x[:, 0], y)

                simp = simps(simps(z, x[:, 0]), x[:, 1])
                trap = trapz(trapz(z, x[:, 0]), x[:, 1])

            elif dims == 3:

                # Generate grid values
                z = np.zeros((x.shape[0], x.shape[0], x.shape[0]))
                for i, y1 in enumerate(x[:, 1]):
                    for j, y2 in enumerate(x[:, 2]):
                        z[i][j] = func(x[:, 0], y1, y2)

                simp = simps(simps(simps(z, x[:, 0]), x[:, 1]), x[:, 2])
                trap = trapz(trapz(trapz(z, x[:, 0]), x[:, 1]), x[:, 2])


            simp_list.append(simp)
            trap_list.append(trap)

        fortran_list = [0]*len(simp_list)

        # integration method from fortran
        if dims == 1:
            fortran_res, f_error = quad(func, eval_list[0][0], eval_list[0][-1])
            fortran_list[0] = fortran_res
            fortran_list[1] = f_error
        elif dims == 2:
            limit1 = [eval_list[0][0][0], eval_list[0][-1][0]]
            limit2 = [eval_list[0][0][1], eval_list[0][-1][1]]
            fortran_res, f_error = nquad(func, [limit1, limit2])
            fortran_list[0] = fortran_res
            fortran_list[1] = f_error

        elif dims == 3:
            limit1 = [eval_list[0][0][0], eval_list[0][-1][0]]
            limit2 = [eval_list[0][0][1], eval_list[0][-1][1]]
            limit3 = [eval_list[0][0][2], eval_list[0][-1][2]]
            options = {"epsabs": 1e-5, "epsrel": 1e-5}
            fortran_res, f_error = nquad(func, [limit1, limit2, limit3], opts=options)
            fortran_list[0] = fortran_res
            fortran_list[1] = f_error

        results[ind][0] = simp_list
        results[ind][1] = trap_list
        results[ind][2] = fortran_list

    return results


def main():

    # Number of grid points used for an integral evaluation
    npoints = [10, 20, 40, 100, 200, 400]
    one_dimensional(npoints)
    print("")
    two_dimensional(npoints)
    print("")
    three_dimensional(npoints)

if __name__ == '__main__':
    main()
