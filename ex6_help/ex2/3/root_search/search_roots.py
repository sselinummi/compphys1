
def secant_root_search(f, initial_guess=0, dx=1, etol=1e-8, max_iters=1000, xmin=-1, xmax=1):
    """
    Find a root of f from the range [xmin, xmax] wih the secant root finding algorithm
    :param f: python function
    :param initial_guess: self-explanatory
    :param dx: initial step size
    :param etol: error tolerance
    :param max_iters: maximum number of iterations
    :param xmin: minimum x value for the root
    :param xmax: maximum x value for the root
    :return: the root and flag that indicates if the root was in the given region
    """
    error = 1000
    xn = initial_guess
    xm = xn + dx
    i = 0
    while error > etol or i > max_iters:
        xn_new = xn - (xn - xm) / (f(xn) - f(xm)) * f(xn)
        xm = xn
        xn = xn_new
        error = abs(xn-xm)

        i += 1

    if xmin <= xn <= xmax:
        flag = 1
    else:
        flag = 0

    return xn, flag
