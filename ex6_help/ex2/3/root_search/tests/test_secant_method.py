import unittest

import numpy as np
import logging
from root_search import  search_roots


def _sinfunc(x):
    return np.sin(x)


class TestRootSearch(unittest.TestCase):

    def test_constant_grid(self):
        """ Test secant root finding method with sin-function """

        log = logging.getLogger("TestRootSearch, test_constant_grid")
        log.debug("")

        x = np.linspace(0., 6. * np.pi, 100)
        f = _sinfunc(x)

        true_roots = np.array([0, np.pi, 2*np.pi, 3*np.pi, 4*np.pi, 5*np.pi, 6*np.pi])

        niters = 100
        initial_guesses = np.random.uniform(low=0, high=6*np.pi, size=niters)
        roots = set()
        for iguess in initial_guesses:
            dx = 0.1
            root, flag = search_roots.secant_root_search(_sinfunc, iguess, dx=dx, max_iters=10000, etol=10e-14, xmin=0,
                                                   xmax=7*np.pi)
            if flag != 0:
                root = round(root, 2)

                roots.add(root)

        pass_str = "Test passed"
        fail_str = "Test failed"

        roots = np.array(list(sorted(roots)))
        if len(true_roots) != len(roots):
            log.debug(fail_str)
            print(fail_str)
        error = true_roots - roots
        if np.sum(error) < 10e-3:
            log.debug(pass_str)
            print(pass_str)
        else:
            log.debug(fail_str)
            print(fail_str)
