from setuptools import setup

setup(name="root_search",
      version='0.1',
      description="Numerical differentiation and integration",
      url="https://gitlab.com/IiroAhokainen/computationalphysics1",
      author="Iiro Ahokainen",
      author_email="iiro.ahokainen@tuni.fi",
      license="MIT",
      packages=["root_search", "num_gradient", "gradient_descent"],
      install_requires=["numpy"],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'])
