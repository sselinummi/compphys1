"""
N-dimensional numerical gradient and test function. CompPhys1 ex 2 problem 4
"""

import numpy as np

def num_gradient(f, x, dx):
    """
    Returns numerical gradient of N-dimensional function f at point x.

    f : function f(x)
    x : fixed point of gradient evaluation
    dx : estimation interval
    """
    if np.isscalar(x):
        x=np.array([x])
    gradient = np.zeros(np.shape(x))
    for i in range(np.shape(x)[0]):
        dxi = np.zeros(np.shape(x))
        dxi[i] = dx
        gradient[i] = (f(x+dxi)-f(x-dxi))/(2*dx)
    return gradient

def test_num_gradient():
    """
    Test function of num_gradient(). Two cases: 3D and 1D
    """

    test_f = lambda x,N: np.sin(x[0])+np.cos(x[N-1])+np.sum(x[2:N-1]**2)
    print("N = 3:")
    print(num_gradient(lambda x:test_f(x,3), [1, 1, 1], 0.01))

    print("N = 1")
    print(num_gradient(lambda x:test_f(x,1), 1, 0.01))
