"""
Script for some numerical integrations. CompPhys2 excercise 2 problem 1
"""

import numpy as np
from scipy.integrate import simps
from scipy.integrate import trapz
from scipy.integrate import quad
from scipy.integrate import nquad


# a) One-dimensional integrations
# a1
f = lambda r: r**2*np.exp(-2*r)*np.sin(2*r)

result_quad = quad(f, 0, np.inf)
print("quad = " + str(result_quad[0]) + " +/- " + str(result_quad[1]))

def print_results_1D(result_trapz, result_simps, result_quad, num_points):
    print("trapezoid    simpson     num points")
    for i in range(len(result_simps)):
        print(str(result_trapz[i]) + " " + str(result_simps[i]) + "       "+ str(num_points[i]))
    
# a2
f = lambda x: np.sinc(x/np.pi)
xmin = 0
xmax = 1

num_points = [10, 20, 40, 100, 200, 400]
print("trapezoid     simpson      num points")

for n in num_points:
    x = np.linspace(xmin, xmax, n)
    y = f(x)
    print("{:15.10f} {:15.10f} {:10.0f}".format(trapz(y,x), simps(y,x), n))

result_quad = quad(f, xmin, xmax)
print("quad = "+str(result_quad[0])+" +/- "+ str(result_quad[1]))

# a3
f = lambda x: np.exp(np.sin(x**3))
xmin = 0
xmax = 5

num_points = [10, 20, 40, 100, 200, 400]
i = 0
print("trapezoid     simpson      num points")

for n in num_points:
    x = np.linspace(xmin, xmax, n)
    y = f(x)
    print("{:15.10f} {:15.10f} {:10.0f}".format(trapz(y,x), simps(y,x), n))
    
result_quad = quad(f, xmin, xmax)
print("quad = "+str(result_quad[0])+" +/- "+ str(result_quad[1]))

# b) Two dimensions

f = lambda x,y:x*np.exp(-np.sqrt(x**2+y**2))
xrange = [0, 2]
yrange = [-2, 2]

print("trapezoid     simpson      num points")

# Trapezoid and simpson separately for both dimension
for n in num_points:
    x = np.linspace(xrange[0], xrange[1], n)
    y = np.linspace(yrange[0], yrange[1], n)
    z = np.zeros((n,n))
    Y_trapz = np.zeros(n)
    Y_simps = np.zeros(n)
    for i in range(n):
        z[i] = f(x,y[i])
        Y_trapz[i] = trapz(z[i], x)
        Y_simps[i] = simps(z[i], x)
    print("{:15.10f} {:15.10f} {:10.0f}".format(trapz(Y_trapz,y), simps(Y_simps,y), n))


result_nquad = nquad(f,  [xrange, yrange])
print("nquad = "+str(result_nquad[0])+" +/- "+ str(result_nquad[1]))

# c) Three dimensions

psi = lambda r: 1/np.sqrt(np.pi)*(np.exp(-np.linalg.norm(r-np.array([-1, 0, 0])))+np.exp(-np.linalg.norm(r-np.array([1 ,0, 0]))))
f = lambda x,y,z: abs(psi(np.array([x, y, z])))**2

limits = [-10, 10]

print("trapezoid     simpson      num points")

# Trapezoid and simpson separately for every dimension
for n in num_points:
    x_ = np.linspace(limits[0], limits[1], n)
    Y_trapz = np.zeros(n)
    Y_simps = np.zeros(n)
    for i in range(n):
        Z_trapz = np.zeros((n, n))
        Z_simps = np.zeros((n,n))
        for j in range(n):
            Z_trapz[i,j] = trapz(f(x_[i], x_[j], x_), x_)
            Z_simps[i,j] = simps(f(x_[i], x_[j], x_), x_)
        Y_trapz[i] = trapz(Z_trapz[i], x_)
        Y_simps[i] = trapz(Z_trapz[i], x_)
    print("{:15.10f} {:15.10f} {:10.0f}".format(trapz(Y_trapz,x_), simps(Y_simps,x_), n))

result_nquad = nquad(f,  [limits, limits, limits])
print("nquad = "+str(result_nquad[0])+" +/- "+ str(result_nquad[1]))