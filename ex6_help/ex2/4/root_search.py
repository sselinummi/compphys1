"""
Simple index-search function and its testing. CompPhys1 ex 2 problem 3
"""

from turtle import st
import numpy as np

def search_indeces(grid, x):
    """
    Function for index search in one-dimensional grid. Returns index of the largest value that is <= x[i]
    
    grid : coordinate grid (np.array)
    x : x-values to be searched (np.array)
    
    return
    I : array of corresponding indeces
    """
    if np.isscalar(x):
        x=np.array([x])
    I = np.zeros(np.shape(x))
    for i in range(np.shape(x)[0]):
        I[i]=np.floor(np.where(grid<=x[i])[0][-1]).astype(int)
        if I[i] == np.shape(grid)[0]:
            I[i] -=1
    return I


def make_test_grid(r_0, h, dim):
    """
    Function for logaritmic test grid
    """
    r = np.zeros(dim)
    r[0]=0.

    for i in range(1,dim):
        r[i]=r_0*(np.exp(i*h)-1)
    return r

def test_search_indeces():
    """
    Test function for search_indices(). Two test cases: linear and logaritmic grids.
    """

    x = np.array([5, 50])
    print("Linear case: Search values 5 and 50")
    linear = np.linspace(0, 100, 100)
    i = search_indeces(linear, x)
    print("Indices: " + str(i[0]) + " and " + str(i[1]) + ". Corresponding intervals: "+ str(linear[int(i[0])]) +
        "-" + str(linear[int(i[0]+1)]) + " and " + str(linear[int(i[1])]) + "-" + str(linear[int(i[1]+1)]))


    print("Logaritmic case")
    r_max = 100
    r_0 = 1e-5
    dim = 100
    h = np.log(r_max/r_0+1)/(dim-1)
    log = make_test_grid(r_0, h, dim)
    i = search_indeces(log, x)
    print("Indices: " + str(i[0]) + " and " + str(i[1]) + ". Corresponding intervals: "+ str(log[int(i[0])]) +
        "-" + str(log[int(i[0]+1)]) + " and " + str(log[int(i[1])]) + "-" + str(log[int(i[1]+1)]))


def main():
    test_search_indeces()

if __name__=="__main__":
    main()
