"""
Simple steepest decent algorithm. CompPhys1 ex 2 problem 5
"""

import numpy as np
from numerical_gradient import num_gradient

def steepest_decent(f, x0, a, it):
    """
    Simple steepest decent algorithm for extremum search of N-dimensional function f. 

    f : function f(x)
    x0 : initial guess
    a : small number for convergence rate adjustment
    it : number of iterations

    Returns
    x : extreme point of f
    """
    x = x0
    gamma = a
    for i in range(it):
        gradient = num_gradient(f, x, gamma)
        gamma = a/(np.linalg.norm(gradient)+1)
        x = x - gamma*gradient
    return x

def test_steepest_decent():
    """
    Test function for steepest_decent()
    """
    test_f = lambda x: np.sum(x**2)

    x0 = np.ones(10)

    print(steepest_decent(test_f, x0, 0.01, 100))

def main():
    test_steepest_decent()
    
main()