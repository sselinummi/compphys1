"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to Computational Physics 1
exercise 2 assignments at TAU.

By Ilkka Kylanpaa on January 2019
(modified Jan 2022)
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


"""
Add basis functions l1 and l2 here
"""
l1 = lambda t: 1-t
l2 = lambda t: t


class linear_interp:

    def __init__(self,*args,**kwargs):
        """contains points and corresponding function values for the function to be interpolated
        """        
        self.dims=kwargs['dims']
        if (self.dims==1):
            self.x=kwargs['x']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
        elif (self.dims==2):
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
            self.hy=np.diff(self.y)
        elif (self.dims==3):
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.z=kwargs['z']
            self.f=kwargs['f']
            self.hx=np.diff(self.x)
            self.hy=np.diff(self.y)
            self.hz=np.diff(self.z)
        else:
            print('Either dims is missing or specific dims is not available')
      
    def eval1d(self,x):
        """1D linear interpolation

        Args:
            x : points on which to interpolate

        Returns:
            f : interpolated function values
        """    
        # converts x to array if x a single value    
        if np.isscalar(x):
            x=np.array([x])

        
        N=len(self.x)-1
        # intialize array for interpolation values
        f=np.zeros((len(x),))

        ii=0
        for val in x:
            # finds index for nearest x value below:
            # np.where returns tuple that contains an array of indeces of occurances as it's first element
            # it's last element is the index to value
            i=np.floor(np.where(self.x<=val)[0][-1]).astype(int)
            # if last elemet, interpolated value is same as last value
            if i==N:
                f[ii]=self.f[i]
            # else the interpolated value is "weigted average" of 2 nearest points
            else:
                t=(val-self.x[i])/self.hx[i]
                f[ii]=self.f[i]*l1(t)+self.f[i+1]*l2(t)
            ii+=1
        # interpolated values are returned
        return f

    def eval2d(self,x,y):
        # converts x to array if x a single value 
        if np.isscalar(x):
            x=np.array([x])
        if np.isscalar(y):
            y=np.array([y])

        Nx=len(self.x)-1
        Ny=len(self.y)-1

        # intialize array for interpolation values
        f=np.zeros((len(x),len(y)))
        # intialize matrix for basis functions
        A=np.zeros((2,2))
        ii=0
        for valx in x:
            # finds index for nearest x value below
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int)
            if (i==Nx):
                i-=1
            jj=0
            for valy in y:
                # finds index for nearest y value below
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int)
                if (j==Ny):
                    j-=1
                # values for basis functions
                tx = (valx-self.x[i])/self.hx[i]
                ty = (valy-self.y[j])/self.hy[j]
                # vectors containing basis functions
                ptx = np.array([l1(tx),l2(tx)])
                pty = np.array([l1(ty),l2(ty)])
                # matrices containg known function values
                A[0,:]=np.array([self.f[i,j],self.f[i,j+1]])
                A[1,:]=np.array([self.f[i+1,j],self.f[i+1,j+1]])
                # calculating S(x,y)=[l1x,l2x]A[l1y,l2y]' to obtain interpolated values
                f[ii,jj]=np.dot(ptx,np.dot(A,pty))
                jj+=1
            ii+=1
        # interpolated values are returned
        return f
    #end eval2d

    def eval3d(self,x,y,z):
        # converts x, y, z to array if (x,y,z) is a single point 
        if np.isscalar(x):
            x=np.array([x])
        if np.isscalar(y):
            y=np.array([y])
        if np.isscalar(z):
            z=np.array([z])

        Nx=len(self.x)-1
        Ny=len(self.y)-1
        Nz=len(self.z)-1

        # intialize array for interpolation values
        f=np.zeros((len(x),len(y),len(z)))
        # intialize matrix for basis functions
        A=np.zeros((2,2))
        B=np.zeros((2,2))
        ii=0
        for valx in x:
            # finds index for nearest x value below
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int)
            if (i==Nx):
                i-=1
            jj=0
            for valy in y:
                # finds index for nearest y value below
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int)
                if (j==Ny):
                    j-=1
                kk=0
                for valz in z:
                    # finds index for nearest z value below
                    k=np.floor(np.where(self.z<=valz)[0][-1]).astype(int)
                    if (k==Nz):
                        k-=1
                    # values for basis functions
                    tx = (valx-self.x[i])/self.hx[i]
                    ty = (valy-self.y[j])/self.hy[j]
                    tz = (valz-self.z[k])/self.hz[k]
                    # vectors containing basis functions
                    ptx = np.array([l1(tx),l2(tx)])
                    pty = np.array([l1(ty),l2(ty)])
                    ptz = np.array([l1(tz),l2(tz)])
                    # matrices containg known function values
                    B[0,:]=np.array([self.f[i,j,k],self.f[i,j,k+1]])
                    B[1,:]=np.array([self.f[i+1,j,k],self.f[i+1,j,k+1]])
                    A[:,0]=np.dot(B,ptz)
                    B[0,:]=np.array([self.f[i,j+1,k],self.f[i,j+1,k+1]])
                    B[1,:]=np.array([self.f[i+1,j+1,k],self.f[i+1,j+1,k+1]])
                    A[:,1]=np.dot(B,ptz)
                    # calculating S(x,y)=[l1x,l2x]A[l1y,l2y]' to obtain interpolated values
                    f[ii,jj,kk]=np.dot(ptx,np.dot(A,pty))
                    kk+=1
                jj+=1
            ii+=1
        # interpolated values are returned
        return f
    #end eval3d
# end class linear interp

    
def main():

    plt.rcParams['pcolor.shading']='auto'

    # 1D EXAMPLE
    # plotting 
    fig1d = plt.figure()
    ax1d = fig1d.add_subplot(111)
    # points for function to be calculated, linear grid N=10 [0,2pi]
    x=np.linspace(0.,2.*np.pi,10)
    # function values, sin(x)
    y=np.sin(x)

    # adding x,y to class defining linear interpolation for 1d case
    lin1d=linear_interp(x=x,f=y,dims=1)

    # points on wich to interpolate, N=100 [0,2pi]
    xx=np.linspace(0.,2.*np.pi,100)
    
    # plotting and naming
    ax1d.plot(xx,np.sin(xx),'b',label='exact')
    ax1d.plot(x,y,'o',label='points')
    # calling interpolation function
    ax1d.plot(xx,lin1d.eval1d(xx),'r--',label='lin. interp')
    ax1d.set_title('linear interpolation in 1D')
    ax1d.legend()

    # 2D EXAMPLE
    # plotting
    fig2d = plt.figure()
    ax2d = fig2d.add_subplot(221, projection='3d')
    ax2d2 = fig2d.add_subplot(222, projection='3d')
    ax2d3 = fig2d.add_subplot(223)
    ax2d4 = fig2d.add_subplot(224)

    # points for function to be calculated, linear, square, 2d-grid N=11 [-2,2]
    x=np.linspace(-2.0,2.0,11)
    y=np.linspace(-2.0,2.0,11)
    X,Y = np.meshgrid(x,y)

    # exact function values, x*exp(-(x^2+y^2))
    Z = X*np.exp(-1.0*(X*X+Y*Y))

    # 2d plotting
    ax2d.plot_wireframe(X,Y,Z)
    ax2d3.pcolor(X,Y,Z)
    ax2d.set_title('original')

    # adding x,y to class defining linear interpolation for 2d case
    lin2d=linear_interp(x=x,y=y,f=Z,dims=2)

    # creating interpolation points
    x=np.linspace(-2.0,2.0,51)
    y=np.linspace(-2.0,2.0,51)
    X,Y = np.meshgrid(x,y)
    # 2d interpolation
    Z = lin2d.eval2d(x,y)
     
    # plotting 
    ax2d2.plot_wireframe(X,Y,Z)
    ax2d4.pcolor(X,Y,Z)
    ax2d2.set_title('interpolated')
    
    # 3D EXAMPLE
    # points for function to be calculated, linear, cube, 3d-grid N=10 [0,3]
    x=np.linspace(0.0,3.0,10)
    y=np.linspace(0.0,3.0,10)
    z=np.linspace(0.0,3.0,10)
    X,Y,Z = np.meshgrid(x,y,z)
    # exact function values, (x+y+z)*exp(-(x^2+y^2+z^2))
    F = (X+Y+Z)*np.exp(-1.0*(X*X+Y*Y+Z*Z))
    X,Y = np.meshgrid(x,y)

    # plotting
    fig3d=plt.figure()
    ax=fig3d.add_subplot(121)
    ax.pcolor(X,Y,F[...,int(len(z)/2)])
    ax.set_title('original (from 3D data)')

    # adding x,y,z to class defining linear interpolation for 3d case
    lin3d=linear_interp(x=x,y=y,z=z,f=F,dims=3)

    # creating interpolation points
    x=np.linspace(0.0,3.0,50)
    y=np.linspace(0.0,3.0,50)
    z=np.linspace(0.0,3.0,50)
    X,Y = np.meshgrid(x,y)

    # interpolating
    F=lin3d.eval3d(x,y,z)

    # plotting
    ax2=fig3d.add_subplot(122)
    ax2.pcolor(X,Y,F[...,int(len(z)/2)])
    ax2.set_title('linear interp. (from 3D data)')

    plt.show()
#end main
    
if __name__=="__main__":
    main()
