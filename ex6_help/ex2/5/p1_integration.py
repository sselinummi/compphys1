""" computational physics1, exercise2, problem1
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Uses different methods (simpson, trapezoid, quad, nquad) in scipy.integrate to numerically
 estimate integrals for given functions
"""
import numpy as np
from scipy.integrate import simpson, trapezoid, quad, nquad
import matplotlib.pyplot as plt

def plotFun(x,y,**kwargs):
    """ Plots given values, 1d

    Args:
        x : x-axis values
        y : y-axis values

    """    
    fig, ax = plt.subplots()
    ax.plot(x,y)
    ax.set(xlim=(x[0],x[-1]), ylim=(min(y),max(y)))
    if 'name' in kwargs:
        ax.set_title(kwargs.get('name'))
    plt.show()

    return True



def printResults(integrals, q, e):
    """ Help function to print results

    Args:
        integrals (dict) : names and values for methods used, number of intervals
        q : result of quad-function
        e : error in quad
    """    
    k = list(integrals.keys())
    v = list(integrals.values())

    print("{:<20} {:<20} {:<20}".format(k[0],k[1],k[2]))
    for t,s,n in zip(v[0],v[1],v[2]) :
        print("{:<20} {:<20} {:<20}".format(t,s,n))

    print("quad: {} +/- {}".format(q,e))

# 1D
def fun1(N):
    """ Calculates numerical intgrated values for 1d function given in p1 a) i) 

    Args:
        N : number of intervals
    """    
    fa1 = lambda x: x**2*np.exp(-2*x)*np.sin(2*x)
    start = 0
    stop = 6 # almost inf, see the figure
    
    # plotting to check what is inf enough
    x = np.linspace(0, 10, 100)
    fa1_v = np.array(fa1(x))
    plotFun(x,fa1_v,name="fun_a1")

    # initializing
    trap = np.zeros(len(N))
    simp = np.zeros(len(N))

    # different integration methods
    for i,n in enumerate(N):
        x = np.linspace(start, stop, n)
        fa1_v = np.array(fa1(x))
        trap[i] = trapezoid(fa1_v,x)
        simp[i] = simpson(fa1_v,x)

    integrals = {"trapezoid": trap, "simpson": simp, "num points": np.array(N)}
    q,e = quad(fa1,0,np.inf)
    printResults(integrals, q, e)

def fun2(N):
    """ Calculates numerical intgrated values for 1d function given in p1 a) ii) 

    Args:
        N : number of intervals
    """   
    fa2 = lambda x: np.sin(x)/x # not defined when x=0
    start = 0.000001 # almost 0 
    stop = 1 
    
    # plotting
    x = np.linspace(0.0001, 1, 100)
    fa2_v = np.array(fa2(x))
    plotFun(x,fa2_v,name="fun_a2")

    # initializing
    trap = np.zeros(len(N))
    simp = np.zeros(len(N))

    # different integration methods
    for i,n in enumerate(N):
        x = np.linspace(start, stop, n)
        fa2_v = np.array(fa2(x))
        trap[i] = trapezoid(fa2_v,x)
        simp[i] = simpson(fa2_v,x)

    integrals = {"trapezoid": trap, "simpson": simp, "num points": np.array(N)}
    q,e = quad(fa2,0,1)
    printResults(integrals, q, e)

def fun3(N):
    """ Calculates numerical intgrated values for 1d function given in p1 a) iii) 

    Args:
        N : number of intervals
    """   
    fa3 = lambda x: np.exp(np.sin(x**3))
    start = 0 
    stop = 5
    
    # plotting
    x = np.linspace(start, stop, 100)
    fa3_v = np.array(fa3(x))
    plotFun(x,fa3_v,name="fun_a3")

    # initializing
    trap = np.zeros(len(N))
    simp = np.zeros(len(N))

    # different integration methods
    for i,n in enumerate(N):
        x = np.linspace(start, stop, n)
        fa3_v = np.array(fa3(x))
        trap[i] = trapezoid(fa3_v,x)
        simp[i] = simpson(fa3_v,x)

    integrals = {"trapezoid": trap, "simpson": simp, "num points": np.array(N)}
    q,e = quad(fa3,start, stop)
    printResults(integrals, q, e)

def fun4(N):
    """ Calculates numerical intgrated values for 2d function given in p1 b)

    Args:
        N : number of intervals
    """   
    fa4 = lambda x, y: x*np.exp(-np.sqrt(x**2+y**2))
    x0 = 0
    x1 = 2
    y0 = -2
    y1 = 2

    # initializing
    trap = np.zeros(len(N))
    simp = np.zeros(len(N))

    # different integration methods
    for i,n in enumerate(N):
        # keeping y constant
        yl = np.linspace(y0, y1, n)
        x = np.linspace(x0, x1, n)
        trap_x = np.zeros(len(yl))
        simp_x = np.zeros(len(yl))

        for j,y in enumerate(yl): 
            fa4_x = np.array(fa4(x,y))
            trap_x[j] = trapezoid(fa4_x,x)
            simp_x[j] = simpson(fa4_x,x)

        trap[i] = trapezoid(trap_x, yl)
        simp[i] = simpson(simp_x, yl)
    integrals = {"trapezoid": trap, "simpson": simp, "num points": np.array(N)}
    q,e = nquad(fa4,[[x0,x1],[y0,y1]])

    printResults(integrals, q, e)


def fun5(N):
    """ Calculates numerical intgrated values for 3d function given in p1 c)
    Takes a while to run.

    Args:
        N : number of intervals
    """   
    rA = np.array([-1,0,0])
    rB = np.array([1,0,0])
    R = lambda x, y, z, r: np.sqrt((x-r[0])**2+(y-r[1])**2+(z-r[2])**2) 
    f5 = lambda x, y, z: np.abs(1/np.sqrt(np.pi)*(np.exp(-R(x,y,z,rA))+np.exp(-R(x,y,z,rB))))**2
    a = -10
    b = 10

    # initializing
    trap = np.zeros(len(N))
    simp = np.zeros(len(N))

    # different integration methods
    for i,n in enumerate(N):
        # keeping y, z constant
        x = np.linspace(a, b, n) # all the same

        trap_x = np.zeros(len(x))
        simp_x = np.zeros(len(x))
        trap_y = np.zeros(len(x))
        simp_y = np.zeros(len(x))

        # y constant
        for j,y in enumerate(x):
            # z constant
            for k,z in enumerate(x): 
                f5_y = np.array(f5(x,y,z))
                trap_y[k] = trapezoid(f5_y,x)
                simp_y[k] = simpson(f5_y,x)

            #f5_x = np.array(f5(x,y,z))
            trap_x[j] = trapezoid(trap_y,x)
            simp_x[j] = simpson(simp_y,x)
        
        trap[i] = trapezoid(trap_x, x)
        simp[i] = simpson(simp_x, x)

    integrals = {"trapezoid": trap, "simpson": simp, "num points": np.array(N)}
    q,e = nquad(f5,[[a,b],[a,b],[a,b]])
    printResults(integrals, q, e)


def main():
    N = [10, 20, 40, 100, 200, 400] # number of itervals
    
    # Problem 1a
    print("1a1")
    fun1(N)
    print("\n1a2")
    fun2(N)
    print("\n1a3")
    fun3(N)

    # Problem 1b 2D integration
    print("\n1b")
    fun4(N)

    # Problem 1c 3D integration
    print("\n1c")
    fun5(N)



if __name__=="__main__":
    main()

