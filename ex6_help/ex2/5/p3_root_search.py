""" computational physics1, exercise2, problem3
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Finds idices for linear interpolation using binary search (O(logn)). Works for
 linear and non-linear grid as long as it is sorted 
 """
import numpy as np
import random as rnd

def non_lin_grid_generation(r_0, h, dim):
    """Creates non-linear grid using exoponent function

    Args:
        r_0 : constant for function
        h : contant for finction
        dim : number of points in grid

    Returns:
        r : created non-linear grid
    """    
    r = np.zeros(dim)

    for i in range(1, dim):
        r[i] = r_0*np.exp((i*h)-1)
    
    return r


def binary_search(x, val):
    """ Binary search to closest indice of x whose value is smaller (or equal) than val

    Args:
        x : array of grid points
        val : searched value

    Returns:
        indice
    """ 
    # if val not in the range of x, return endpoint
    if val <= np.min(x): return 0
    elif val >= np.max(x): return (len(x)-1)
    
    n = len(x)
    L = 0
    R = n - 1
    while L <= R:
        mid = np.floor((L + R) / 2).astype(int)
        if x[mid] < val and x[mid+1] > val:
            return mid
        elif x[mid] < val:
            L = mid + 1
        elif x[mid] > val:
            R = mid - 1
        # value is a grid point
        else:
            return mid
    # not found, could do some nicer error handling
    print("Index not found")
    return 0


def test_linear_grid(a,b,N):
    """ Tests binary_search with linear grid

    Args:
        a : beginning point for grid, a < b
        b : end point for grid
        N : number of grid points
    """    
    l_grid = np.linspace(a,b,N)
    print("Linear grid with interval [{},{}], and indices between 0-{} \n".format(a,b,int(N-1)))
    print("Testing point after end point")
    print(binary_search(l_grid, b+np.abs(l_grid[1]-l_grid[0])))
    print("\nTesting before starting point")
    print(binary_search(l_grid, a-np.abs(l_grid[1]-l_grid[0])))
    print("\nTesting point {} between start and end points".format((a+b/5)))
    i = binary_search(l_grid, a+b/5)
    print("Obtained index: {}, x(i)={}, x(i+1)={}".format(i,l_grid[i],l_grid[i+1]))
    p = rnd.choice(l_grid)
    print("\nTesting point is a grid point {}".format(p))
    j = binary_search(l_grid, p)
    print("Obtained index: {}, x(i)={}, x(i+1)={}\n".format(j,l_grid[j],l_grid[j+1]))


def test_non_linear_grid():
    """ Tests binary_search() for non-linear grid
    """   
    # grid parameters 
    r_max = 100
    r_0 = 1e-5
    dim = 100
    h = np.log(r_max/r_0+1)/(dim-1)
    # creatin non-linear grid
    nl_grid = non_lin_grid_generation(r_0, h, dim)
    N = len(nl_grid)
    a = np.min(nl_grid)
    b = np.max(nl_grid)
    # printing test cases
    print("Non linear grid with interval [{},{}], and indices between 0-{} \n".format(a,b,int(N-1)))
    print("Testing point after end point")
    print(binary_search(nl_grid, b+np.abs(nl_grid[1]-nl_grid[0])))
    print("\nTesting point is end point")
    print(binary_search(nl_grid, nl_grid[-1]))
    print("\nTesting before starting point")
    print(binary_search(nl_grid, a-np.abs(nl_grid[1]-nl_grid[0])))
    print("\nTesting point {} between start and end points".format((a+b/5)))
    i = binary_search(nl_grid, a+b/5)
    print("Obtained index: {}, x(i)={}, x(i+1)={}".format(i,nl_grid[i],nl_grid[i+1]))
    p = rnd.choice(nl_grid)
    print("\nTesting point is a grid point {}".format(p))
    j = binary_search(nl_grid, p)
    print("Obtained index: {}, x(i)={}, x(i+1)={}".format(j,nl_grid[j],nl_grid[j+1]))

def main():

    test_linear_grid(-10,10,100)
    test_non_linear_grid()


if __name__=="__main__":
    main()
