""" computational physics1, exercise2, problem4
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Calculates numerical N-dimensional gradient
 """
import numpy as np
import random as rnd

def num_gradient(f,r,dx=1e-3):
    """ Calculates numerical gradient

    Args:
        f : function to be derived
        r : point on which gradient is calculated
        dx : step size. Defaults to 1e-3.

    Returns:
        grad : numerically calculated gradient
    """    
    # in 1D case value is converted to array
    if np.isscalar(r):
            r = np.array([r])
    dim = len(r)
    grad = np.zeros(dim)
    for i in range(dim):
        h = np.zeros(dim)
        h[i] = dx
        grad[i] = (f(r+h)-f(r))/(dx)
    
    return grad

def test_grad(x):
    """ Tests numerical_gradient, when dim >=3

    Args:
        x : point on which gradient is calculated
    """    
    fun = lambda x: np.sin(x[0])+np.cos(x[-1])+np.sum(np.power(x[1:-1],2))

    # analytical solution
    analytical_fun = np.zeros(len(x))
    analytical_fun[0] = np.cos(x[0])
    analytical_fun[-1] = -np.sin(x[-1])
    for i in range(1,len(x)-1):
        analytical_fun[i] = 2*x[i]
    # printing results
    print("Numerical gradient: {}".format(num_gradient(fun,x)))
    print("Analytical gradient: {}\n".format(analytical_fun))


def main():
    # 1D
    print("1D")
    x = 7
    f = lambda x: np.exp(-x)
    print("Numerical gradient: {}".format(num_gradient(f,x)))
    print("Analytical gradient: {}\n".format(-f(x)))

    # 3D
    print("3D")
    x = [1, 3, 4]
    test_grad(x)

    # dim=6
    print("dim = 6")
    dim = 6
    x = [rnd.uniform(-10,10) for i in range(dim)]
    test_grad(x)

if __name__=="__main__":
    main()