""" computational physics1, exercise2, problem5
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Calculates minimum for function, utilizes numerical gradient.
No testing.
 """
import numpy as np
from p4_numerical_gradient import num_gradient


def steepest_descent(f,xi=1,a=0.01, tol=1e-4):
    """ Estimates minimum value for given function

    Args:
        f : function
        xi : initial guess for minimum location. Defaults to 1.
        a : small constant. Defaults to 0.01.
        tol : tolerance for gradient. Defaults to 1e-4.

    Returns:
        point in which minimum is reached
    """    
    g = num_gradient(f,xi)
    g_abs = np.sqrt(np.sum(np.power(g,2)))
    gamma = a/(g_abs+1)

    it_max = 1000
    it = 0
    # continues untill change in gradient is small or too many iterations
    while g_abs > tol and it < it_max:
        xi = xi - gamma * g
        g = num_gradient(f,xi)
        g_abs = np.sqrt(np.sum(np.power(g,2)))
        gamma = a/(g_abs+1)
        it += 1

    print("Number of iterations: {}, it_max = {}".format(it, it_max))

    return xi


def main():
    # Test function, minimun is x=0
    f = lambda x: np.sum(np.power(x,2))
    
    dim = 6

    # intial guess for minimum location
    x = np.ones(dim)

    xn = steepest_descent(f,x)
    print("Estimation for minumum localtion: {}".format(xn))
    print("Function value at point: {}". format(f(xn)))


if __name__=="__main__":
    main()

