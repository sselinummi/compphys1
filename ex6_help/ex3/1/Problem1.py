"""2D integration of a function from exercise 3 """
#import needed libraries
import numpy as np
from scipy import integrate

#function from exercise
"""
def f(x,y):
    return (x+y)*np.exp((-1/4)*np.sqrt(x**2+y**2))
"""
"""define integration function to calculate the 2d integral with simps and nquad """
def integration():
    f = lambda y, x:(x+y)*np.exp((-1/4)*np.sqrt(x**2+y**2))#lambda function
    I2d, I2derr = integrate.dblquad(f, 0, 2, lambda x:(-2), lambda x: 2) #quad solution using dblquad
    print('{0:18} {1:14}'.format('simpson', 'num_points')) 
    arr1 = np.array([5, 15, 25, 50, 100]) #array of different number of integration nodes
    for a in arr1:
        x = np.linspace(0,2,a) #x variable in (0,2)
        y = np.linspace(-2,2,a)#y varibale in(-2,2)
        X,Y = np.meshgrid(x, y) #meshgrid of variables x and y
        z = (X+Y)*np.exp((-1/4)*np.sqrt(X**2+Y**2)) #itegrated function
        Is = integrate.simps(integrate.simps(z,x),y) #calculate simps for x, than y
        print('{0:12.10f} {1:12}'.format(Is, a))
    print('nquad = {0:0.5f} +/- {1:0.16f} '.format(I2d, I2derr))
#deine main function to call the integration function  
def main():
    integration()
#call main
main()
