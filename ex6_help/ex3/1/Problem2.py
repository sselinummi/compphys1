"""Spline interpolation with given experimental data to estimate the path given by y = 2x**2 for x > 0"""

#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
#from mpl_toolkits.mplot3d import axes3d, Axes3D
#from matplotlib import cm

#playing with 3D ploting
"""
def plot_data(x, y, z):
    fig = plt.figure()
    #ax = fig.gca(projection='3d')
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    plt.show()
"""
#define exp_data function to do spline interpolation with scipy
def exp_data():
    #load data from file
    xexp = np.loadtxt('x_grid.txt')#xgrid
    yexp = np.loadtxt('y_grid.txt')#ygrid
    Fexp = np.loadtxt('exp_data.txt')#experimentl data
    check_data = np.loadtxt('ref_interpolated.txt')#refernce data
    """ read-in data """
    Fexp = Fexp.reshape([len(xexp),len(yexp)])#reshape experimental data according to sizes of x and y vectors
    X,Y = np.meshgrid(xexp, yexp)
    f =  interpolate.interp2d(xexp,yexp,Fexp,kind='cubic')#call scipy interp2d cubic kind
    N = 100 #number of tetsing points
    x_check = np.linspace(0,1,100) #x for reference data
    x_vals = np.linspace(0,1,N)# generate x values interpolation
    y_vals = 2*x_vals**2 #generate y values for interpolation
    z_vals = f(x_vals,y_vals) #interpolate 
    p = np.zeros(N) #initialize vector
    for i in range(1,N):
        p[i] = z_vals[i,i] #shape vector 
    xv, yv = np.meshgrid(x_vals, y_vals) #meshgrid of interpolated values
    #plot the data
    fig, (axs1, axs2) = plt.subplots(1,2)
    #experimental data grid
    axs1.set_title('Experimental data grid')
    axs1.contourf(X.T,Y.T,Fexp)
    axs1.scatter(X.T,Y.T,color='red', label = 'XY grid')
    axs1.plot(x_vals, y_vals, '-' ,color = "black", linewidth=4, label = 'interpolation path')
    axs1.legend(loc="lower right")
    #compare interpolated data with reference data
    axs2.set_title('Interpolation with reference values for comparing')
    axs2.plot(x_check, check_data, '-' ,color = "black", linewidth=6, label = 'ref_interpolated')
    axs2.plot(x_vals, p,'x', color = 'red', label = 'interpolated')
    axs2.legend(loc="lower right")
    #set padding, height and width of figure
    fig.tight_layout(pad=3.0)
    value_height = 8
    value_width = 10
    fig.set_figheight(value_height)
    fig.set_figwidth(value_width)
    #set names to axes
    for ax in fig.get_axes():
        ax.set(xlabel='x', ylabel='y')

    for ax in fig.get_axes():
        ax.label_outer()
    #plot
    plt.show()
    
#call exp_data function
exp_data()
