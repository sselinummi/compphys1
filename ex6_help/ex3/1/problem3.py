"""Exercise 3 mistery matrix, program to create a matrix and calculate the dot prouct """
"""Matrix coefficients: As per code described below coefficents are: 
   on main diagonal, first element is -3.0 and last is 1, with 0 in between
   on upper nearest diagonal(to the main diagonal) first element is 4.0, and the rest are 1
   on lower nearest diagonal(to the main diagonal) first element is -1, and all other elements are -1 except for the last one which is -4.0
   the third element in the first row is -3.0, and the third element from last column in the last row is 3.0
   rest of elements are 0"""
""" """
#import libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse

"""Function to create a matrix using the predefined grid """
def make_matrix(x):
    N = len(x)  #length of grid
    h = x[1]-x[0] #distance between first two elements of array
    off_diag = np.ones((N-1,))#initialize vector
    """seting values of matrix to 0, and seting upper nearest diagonal to be 1, and lowest nearest diagonal to be -1"""
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0]  #set the first 3 values of first row to -3.0, 4.0, -1.0
    A[-1,-3:] = [1.0,-4.0,3.0] #set the last 3 values of last row to 1.0, -4.0, 3.0
    return A/h/2
#define main function to call the make_matrix function and plot the calculations
def main():
    N = 50 #points
    grid = np.linspace(0,np.pi,N)#set uniform grid
    #functions
    x = np.sin(grid) 
    x1 = np.arctan(grid)
    x2 = np.cos(grid)
    x3 = np.cos(grid)+np.sin(grid)
    #call function make_matrix and make matrixes from the given functions
    A = make_matrix(grid)
    
    # calculate here b=Ax(dot product) as described in the problem setup
    b = A.dot(x)
    b1 = A.dot(x1)
    b2 = A.dot(x2)
    b3 = A.dot(x3)
    #plot the dot product and functions
    fig, ((axs1, axs2), (axs3, axs4)) = plt.subplots(2,2, figsize=(12,9))
    #plot for sin function
    axs1.set_title('First derivative of sin(x)')
    axs1.plot(grid,b,'-', label='b=Ax')
    axs1.plot(grid, x, '--',label = 'sin(x)')
    axs1.legend(loc="upper right")
    #plot for cos function
    axs2.set_title('First derivative of arctan(x)')
    axs2.plot(grid,b1,'-', label = 'b=Ax')
    axs2.plot(grid,x1,'--', label = 'arctan(x)')
    axs2.legend(loc="upper right")
    #plot for tan function
    axs3.set_title('First derivative of cos(x)')
    axs3.plot(grid,b2,'-',label = 'b=Ax')
    axs3.plot(grid,x2,'--', label = 'cos(x)')
    axs3.legend(loc="upper right")
    #plot for cos(x)+sin(x)
    axs4.set_title('First derivative of cos(x)+sin(x)')
    axs4.plot(grid,b3,'-', label = 'b=Ax')
    axs4.plot(grid,x3,'--', label = 'cos(x)+sin(x)')
    axs4.legend(loc="upper right")
    #set height and width of figure
    fig.tight_layout(pad=3.0)
    value_height = 10
    value_width = 14
    fig.set_figheight(value_height)
    fig.set_figwidth(value_width)
    #set axes labels
    for ax in fig.get_axes():
        ax.set(xlabel='x', ylabel='y')

    for ax in fig.get_axes():
        ax.label_outer()
    #show plot
    plt.show()
#call main function
if __name__=="__main__":
    main()
