"""
Computational physics 1

1. Add code to function 'largest_eig'
- use the power method to obtain the largest eigenvalue and the 
  corresponding eigenvector of the provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use that to validating your 
  power method implementation

Notice: 
  dot(A,x), A.dot(x), A @ x could be helpful for performing 
  matrix operations

"""


import numpy as np
import matplotlib.pyplot as plt
""" Simple power metod to calculate the largest eigenvector and eigenvalue"""
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
from numpy import linalg as LA

def largest_eig(A,tol=1e-9):
    """
    - Add simple power method code in this function
    - Add appropriate commenting espcially in this comment section
    """
    #Power metod to calculate the eigenvalue and eigenvector
    xk1 = np.random.rand(100,1) #give the initial value, the better the initial value, the more precise the calculation
    k = 20000 #iterate over values
    for i in range(0,k):
        xk = A.dot(xk1) # dot product of vector and matrix
        xk = xk/LA.norm(xk) #devide by norm
        xk1 = xk # assign that vector to be the new eigenvector
            
    eig_vector = xk1
    #calculate the eigenvalue with lambda = A*x*x/x*x
    A1 = A.dot(xk1)
    x1 = np.squeeze(np.asarray(A1))
    x2 =np.squeeze(np.asarray(xk1))
    A2 = x1.dot(x2)
    A3 = x2.dot(x2)
    eig_value = abs(A2/A3)
    return eig_value, eig_vector #return the found values
    
#define the main function 
def main():
    grid = np.linspace(-5,5,100) #grid of matrix
    grid_size = grid.shape[0] #size of grid
    dx = grid[1]-grid[0] #distance between first two elements
    dx2 = dx*dx#square distance
    #make matrix with scipy diags, matrix has vaulues on main, upper first and lower first diagonal, the rest are zeros
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    #calculate eigenvalu and vector with scipy
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    #call the local function
    l,vec=largest_eig(H0)
    
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)

    #integrate over vectors
    psi0=evecs[:,0]
    norm_const=simps(abs(psi0)**2,x=grid)
    psi0=psi0/norm_const
    print(norm_const)

    psi0_ = vec*1.0
    norm_const=simps(abs(psi0_.T)**2,x=grid)
    psi0_=psi0_/norm_const
    print(norm_const)
    #plot the calculated vectors
    plt.plot(grid,abs(psi0)**2,label='scipy eig. vector squared')
    plt.plot(grid,abs(psi0_)**2,'r--',label='largest_eig vector squared')
    plt.legend(loc=0)
    plt.show()


if __name__=="__main__":
    main()
