"""Calculate the electric field of a charged 1D rod with size L and ositive charge Q is evenly distributed along the rod"""
#import needed libraries
import numpy as np
from scipy import integrate
from matplotlib.pyplot import quiver
import matplotlib.pyplot as plt

#electric field calculation
def electric_field(xp, yp, x):
    lam = 1 #lambda variable
    eps = np.finfo(float).eps #eps0-Vacuum permittivity
    Ex = np.zeros((20,20)) #initialize electric field vector over x 
    Ey = np.zeros((20,20)) #initialize electric field vector over y
    i = 0
    for xx in xp:
        j = 0
        for yy in yp:
             Efx = lam/(4*eps*np.pi)*(xx-x)/((xx-x)**2+yy**2)**1.5 #Ex calculated over x from Xpoint on a grid
             Efy = lam/(4*eps*np.pi)*(yy)/((xx-x)**2+yy**2)**1.5# Ey calculated over y from  Ypoint on a grid
             #Electric field calculation      
             Ex[i,j]=integrate.simps(Efx,x) 
             Ey[i,j]=integrate.simps(Efy,x)
             j = j+1
        i = i+1
    return Ex, Ey
    
def main():
    L=1 #length of rod
    xp = np.linspace(-L,L, 20) #initialize Xpoint
    yp =np.concatenate([np.linspace(-L/2,-L/10,10), np.linspace(L/10, L/2,10)])#initialize Ypoint
    x = np.linspace(-L/2, L/2, 100)#random point on rod
    Ex, Ey = electric_field(xp, yp, x)#calculate the electric field function
    Xp, Yp = np.meshgrid(xp, yp)
    fig, ax = plt.subplots(figsize=(7,7))
    #plot the field with pyplot quiver
    n = -2
    color_array = np.sqrt(((Ex-n)/2)**2 + ((Ey-n)/2)**2)
    ax.set_title('Electric field of the rod')
    ax.quiver(Xp.T,Yp.T, Ex, Ey, color_array, alpha=0.8, units = 'xy',linewidth=.5 )
    plt.show()
#call main   
main()
