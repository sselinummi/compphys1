""" Computational physics 1 - Week 3 Problem 1
Author: Niko Gullsten - niko.gullsten@tuni.fi

2D integration of a given function with Simpson rule and scipy's nquad function.
"""

import numpy as np
import pandas as pd
from scipy.integrate import simps, nquad

def function(x, y):
    # Function for the exercise  
    return (x+y)*np.exp(-(1/4)*np.sqrt(x**2+y**2))

def main():  
    # Define integration limits and number of points for Simpson rule
    x0, x1 = 0, 2
    y0, y1 = -2, 2
    NUM_OF_POINTS = [5, 15, 25, 50, 100]
        
    # Dataframe for neat printing
    results = pd.DataFrame(columns=["Simpson rule", "Number of points"])
    
    for i, points in enumerate(NUM_OF_POINTS):
        # Create x and y grids
        x = np.linspace(x0, x1, num=points)
        y = np.linspace(y0, y1, num=points)
    
        # Integrate twice using 1D Simpson rule
        x_int_simps = simps(function(x[:, None], y[None, :]), x)
        y_int_simps = simps(x_int_simps, y)
        
        # Add result to dataframe
        results.loc[i] = [y_int_simps, points]
        
    # Integrate with nquad
    result_nquad, error_nquad = nquad(function, [[x0, x1], [y0, y1]])
    
    # Print the dataframe without indexing and nquad
    print(results.to_string(index=False))
    print("nquad = {:.10f} +/- {:.10f}".format(result_nquad, error_nquad))
    print()
    

if __name__ == "__main__":
    main()