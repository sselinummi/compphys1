""" Computational physics 1 - Week 3 Problem 2
Author: Niko Gullsten - niko.gullsten@tuni.fi

2D interpolation with spline.
"""
import numpy as np
import matplotlib.pyplot as plt
from spline_class import spline

DATA_PATH = "./ex3_help/"

def read_data(path):
    """
    Reads the given experimental data.

    Parameters
    ----------
    path : string
        Path to the folder with data files.

    Returns
    -------
    x : numpy array
        x values of the grid.
    y : numpy array
        y values of the grid.
    F : numpy array
        Experimental data.
    reference_data : numpy array
        Reference data for the interpolation. 

    """
    x = np.loadtxt(path + "x_grid.txt")
    y = np.loadtxt(path + "y_grid.txt")
    F = np.loadtxt(path + "exp_data.txt")
    ref_data = np.loadtxt(path + "ref_interpolated.txt")
    
    F = F.reshape([len(x), len(y)])
    
    return x, y, F, ref_data


def main(): 
    # Reads the data
    xexp, yexp, Fexp, ref_data = read_data(DATA_PATH)
    
    # Initialize the grid and the given path
    x = np.linspace(0, 1, 100)
    y = 2*x**2
        
    # Initializes the 2D spline with given spline class
    spl2d = spline(x=xexp, y=yexp, f=Fexp, dims=2)

    # Interpolate the data
    interp_data = np.zeros_like(x)
    for i in range(len(x)):
        interp_data[i] = spl2d.eval2d(x[i], 2*x[i]**2)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    # Visualizes the experimental data with filled contours and 
    # grid with red dots. Plots the given path as well.
    X, Y = np.meshgrid(xexp, yexp)
    ax1.contourf(X.T, Y.T, Fexp)
    ax1.scatter(X.T, Y.T, color="red")
    ax1.plot(x, y, color="black", linewidth=4)
    
    # Visualize the interpolated data as well as the given reference data.
    ax2.plot(x, interp_data, label="Interpolated data")
    ax2.plot(x, ref_data, '--', label="Reference data")
    ax2.legend()
    
    plt.show()
    
if __name__ == "__main__":
    main()
