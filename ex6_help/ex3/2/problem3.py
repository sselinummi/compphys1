""" Computational physics 1 - Week 3 Problem 3
Author: Niko Gullsten - niko.gullsten@tuni.fi

Numerical derivation with a mystery matrix.
"""

import numpy as np
import matplotlib.pyplot as plt

def make_matrix(x):
    """
    Constructs a mystery matrix that is used to calculate the numerical derivative
    of a function using finite differences method.
    
    OBS! The derivation of the method is shown in a separate .pdf file found in
    my Gitlab directory. 

    Parameters
    ----------
    x : numpy array
        The grid for the data points.

    Returns
    -------
    A : numpy array
        The "mystery matrix".

    """
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag, -1) + np.diag(off_diag, 1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    
    return A/(2*h)

def main():
    N = 100
    grid = np.linspace(0, np.pi, N)
    A = make_matrix(grid)
    
    # Different functions for the exercise
    x1 = np.sin(grid)
    x2 = np.cos(grid)
    x3 = np.exp(grid)
    x4 = grid**2
        
    # b = Ax matrix product
    b1 = np.dot(A, x1)
    b2 = np.dot(A, x2)
    b3 = np.dot(A, x3)
    b4 = np.dot(A, x4)
    
    # Plotting the functions and b
    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)
    
    ax1.plot(grid, x1, "--", label="sin(x)")
    ax1.plot(grid, b1, label="b = Ax")
    ax1.legend()
    
    ax2.plot(grid, x2, "--", label="cos(x)")
    ax2.plot(grid, b2, label="b = Ax")
    ax2.legend()
    
    ax3.plot(grid, b3, label="b = Ax")
    ax3.plot(grid, x3, "--", label="$e^{x}$")
    ax3.legend()
    
    ax4.plot(grid, x4, "--", label="$x^2$")
    ax4.plot(grid, b4, label="b = Ax")
    ax4.legend()
    
    fig.tight_layout()
    plt.show()

if __name__=="__main__":
    main()

