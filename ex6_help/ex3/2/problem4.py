""" Computational physics 1 - Week 3 Problem 4
Author: Niko Gullsten - niko.gullsten@tuni.fi

Calculate the largest eigenvalue and its corresponding eigenvector
of a given matrix using the Power method.
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eig(A,tol=1e-9):
    """
    Calculates the largest eigenvalue and the corresponding eigenvector using
    the Power method. 

    Parameters
    ----------
    A : array
        The matrix.
    tol : float, optional
        Error tolerance of the calculated value. The default is 1e-9.

    Returns
    -------
    eig_value : float
        Largest eigenvalue.
    eig_vector : array
        Corresponding eigenvector.

    """
    # Initial eigenvector and eigenvalue
    eig_vector = np.random.rand(A.shape[0])
    eig_value_old = 0
    
    error = 1
    
    # Run this while the error is greater than the given tolerance
    while error > tol:
        # Calculate the matrix-vector product
        eig_vector = A@eig_vector
        
        # Update new eigenvalue with the largest eigenvalue       
        eig_value_new = np.max(np.abs(eig_vector))
        
        # Normalize eigenvector
        eig_vector /= eig_value_new
        
        # Update error
        error = np.abs(eig_value_new - eig_value_old)
        
        # Update old eigenvalue
        eig_value_old = eig_value_new
            
    eig_value = eig_value_new
    
    # Not really sure, why we have to normalize here as well, 
    # but the code doesn't actually work without this normalization...
    eig_vector /= np.linalg.norm(eig_vector)
    
    return eig_value, eig_vector

def main():
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    
    l,vec=largest_eig(H0)
    
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)
    
    psi0=evecs[:,0]
    norm_const=simps(abs(psi0)**2,x=grid)
    psi0=psi0/norm_const
    print(norm_const)

    psi0_ = vec*1.0
    norm_const=simps(abs(psi0_)**2,x=grid)
    psi0_=psi0_/norm_const
    print(norm_const)

    plt.plot(grid,abs(psi0)**2,label='scipy eig. vector squared')
    plt.plot(grid,abs(psi0_)**2,'r--',label='largest_eig vector squared')
    plt.legend(loc=0)
    plt.text(-2, 0.3, "Largest eig estimates: \nScipy: {:.5f} \nPower method: {:.5f}".format(eigs[0], l))
    plt.show()


if __name__=="__main__":
    main()
