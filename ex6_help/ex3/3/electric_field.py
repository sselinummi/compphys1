"""
Calculating the electic field for 1D rod
"""
import numpy as np
import matplotlib.pyplot as plt

from scipy.integrate import simps

def e_field(rp,x):
    """
    Calculates the net field at any point in space and the x- and y-directions of the
    field at that point.

    rp : array, point in N-dimensional space
    x : array, positions along the rod
    """

    # calculating the differential electric fields on the given point rp 
    dE=[None]*len(x)
    for i in range(0,len(x)):
        dE[i]=e_field_dif(x[i],rp)

    E=0.0
    Es=[] # list for collecting the directions of the field
    for i in range(0,len(rp)):
        Ei=simps([j[i] for j in dE],x)
        E+=Ei
        Es.append(Ei)
    
    return E,Es


def e_field_dif(xi,rp,const=1.0):
    """
    Forms the diffenetial electric field at any point in space for 1D rod
    that is on x-axis.

    xi : scalar, position along the rod
    rp : array, point in N-dimensional space
    const : float, optional, constant in the electic field's equation
    """
    r_rod=0.0*rp
    r_rod[0]=xi
    r=rp-r_rod
    r_l=np.linalg.norm(r)
    r_hat=r/r_l
    return const*r_hat/r_l**2

def e_analytical(l,d,const=1):
    # returns the analytical answer for the net field in 1D case
    return const*(1/d-1/(d+l))



def main():
    
    # for 1D case
    l=2.0
    d=1.0
    rp=np.array([l/2+d,0.0])
    x=np.linspace(-l/2,l/2,100)
    
    E,Es=e_field(rp,x)
    analytical_E=e_analytical(l,d)
    print('Numerical: ', E)
    print('Analytical: ', analytical_E)

    # for 2D case
    rp=np.array([l/2+d,l/2+d])
    E,Es=e_field(rp,x)
    print('Numerical value for 2D case: ', E)

 
    # visualizing vector field
    a=np.linspace(-3,3,20)
    b=np.linspace(-3,3,20)

    x_dir=[] # net fields to x-direction
    y_dir=[] # net fields to y-direction
    xs=[] # x-positions where the field is calculated
    ys=[] # y-positions where the field is calculated
    for i in a:
        for j in b:
            rp2=np.array([i,j])
            E2_temp,Es2=e_field(rp2,x)
            x_dir.append(Es2[0])
            y_dir.append(Es2[1])
            xs.append(i)
            ys.append(j)

    
    plt.figure()
    plt.plot(x,0.0*x,linewidth=4)
    plt.quiver(xs,ys,x_dir,y_dir)
    plt.show()

if __name__=='__main__':
    main()