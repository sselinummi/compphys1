"""
Calculating estimates for a 2D integral with different amounts of
intervals using scipy's simps function an comparing the estimates 
against the value obtained from scipy's nquad function.
"""
import numpy as np

from scipy.integrate import simps,nquad

def integral():
    """
    Function calculating estimates for the 2d integral using 
    scipy's Simpsons integral and nquad 
    functions. Estimates are calculated using different amounts of
    intervals.
    """
    num_points=[5,15,25]
    print('{0:20} {1:15}'.format('simpson','num_points'))
    for num_p in num_points:
        x=np.linspace(0,2,num_p)
        y=np.linspace(-2,2,num_p)
        X, Y = np.meshgrid(x,y)
        I_s=simps(simps(fun(X,Y), x),y)
        print('{0:10.10f} {1:10}'.format(I_s,num_p))
    
    I_nquad=nquad(fun,[[0,2],[-2,2]])
    print('nquad =  {0:10.10f}  +/-  {1:10.10f}'.format(I_nquad[0],I_nquad[1]))
    print()

def fun(x,y):
     # 2D function for testing the integral function
    return (x+y)*np.exp(-1/4*np.sqrt(x**2+y**2))

def main():
    # main function calling the integral function
    integral()

if __name__=="__main__":
    main()