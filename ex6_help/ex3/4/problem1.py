"""
Script for some numerical integrations. CompPhys2 excercise 3 problem 1
"""
import numpy as np
from scipy.integrate import simps
from scipy.integrate import nquad

f = lambda x,y:(x+y)*np.exp(-1/4*np.sqrt(x**2+y**2))
xrange = [0, 2]
yrange = [-2, 2]

num_points = [5, 15, 25, 50, 100]

print("     simpson             N")

# Simpson separately for both dimension
for n in num_points:
    x = np.linspace(xrange[0], xrange[1], n)
    y = np.linspace(yrange[0], yrange[1], n)
    z = np.zeros((n,n))
    Y_simps = np.zeros(n)
    for i in range(n):
        z[i] = f(x,y[i])
        Y_simps[i] = simps(z[i], x)
    print("{:15.10f} {:10.0f}".format(simps(Y_simps,y), n))

result_nquad = nquad(f,  [xrange, yrange])
print("nquad = "+str(result_nquad[0])+" +/- "+str(result_nquad[1]))
