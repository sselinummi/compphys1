"""
Script for numerical interpolation in 2D. CompPhys2 excercise 3 problem 2
"""
import numpy as np
import matplotlib.pyplot as plt
from spline_class import spline

def main():
    xexp = np.loadtxt("x_grid.txt")
    yexp = np.loadtxt("y_grid.txt")
    Fexp = np.loadtxt("exp_data.txt")
    Fexp = Fexp.reshape([len(xexp),len(yexp)])

    # Vizulation of experimental data
    X,Y = np.meshgrid(xexp,yexp)
    fig = plt.figure()
    fig.add_subplot(1,2,1)
    plt.gca().set_aspect(1)
    plt.contourf(X.T,Y.T,Fexp)
    plt.scatter(X.T,Y.T,color='red')
    plt.title("Experimental data")

    # Interpolation
    spl2d=spline(x=xexp,y=yexp,f=Fexp,dims=2)
    x = np.linspace(0,1,100)
    y = 2*x**2
    plt.plot(x,y, linewidth=4, color='black')
    evaluated = spl2d.eval2d(x,y)
    eval_line = np.zeros(100)
    for i in range(100):
        eval_line[i] = evaluated[i,i]

    # Comparison to the reference
    ref  = np.loadtxt("ref_interpolated.txt")
    print("Euclidean distance to reference: " + str(np.linalg.norm(eval_line - ref)))

    fig.add_subplot(1,2,2)
    plt.plot(x,eval_line,color='black',linewidth=2)
    plt.title("Interpolation along the line")

    plt.show()

if __name__ == "__main__":
    main()