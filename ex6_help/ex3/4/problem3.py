""""
Vizulation of numerical derivations using matrix formulation. CompPhys ex 3 problem 3
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse


def make_matrix(x):
    """
    Makes matrix for numerical derivative calculation.

    The numerical derivative b of x can be calculated as a product b = Ay.
    b[i] = (x[i+1]-x[i-1])/(2*h)
    x : grid
    """
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] # The first and the last values of the sequence x are special cases
    A[-1,-3:] = [1.0,-4.0,3.0]
    return A/h/2

def main():
    N = 50
    grid = np.linspace(0,np.pi,N)
    x = np.sin(grid)
    A = make_matrix(grid)
    
    fig=plt.figure()
    fig.add_subplot(2,2,1)
    b = A.dot(x)
    plt.plot(grid,b,grid,x,'--')

    fig.add_subplot(2,2,2)
    x = grid*2
    b = A.dot(x)
    plt.plot(grid,b,grid,x,'--')

    fig.add_subplot(2,2,3)
    x = grid**2
    b = A.dot(x)
    plt.plot(grid,b,grid,x,'--')

    fig.add_subplot(2,2,4)
    x = np.sinc(grid+1)
    b = A.dot(x)
    plt.plot(grid,b,grid,x,'--')

    plt.show()

if __name__=="__main__":
    main()



