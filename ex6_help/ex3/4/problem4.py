"""
Computational physics 1 exercise 3 problem 4

1. Add code to function 'largest_eig'
- use the power method to obtain the largest eigenvalue and the 
  corresponding eigenvector of the provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use that to validating your 
  power method implementation

Notice: 
  dot(A,x), A.dot(x), A @ x could be helpful for performing 
  matrix operations

"""


from cgi import print_environ_usage
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eig(A,tol=1e-9):
    """
    Simple power method code for eigenvalue and eigvector search.
    A : Matrix as np.array
    tol : tolerance of the eigenvalue estimation
    """
    x = np.ones(np.shape(A)[0])
    a = np.inf
    prev = np.inf
    while a > tol:
      eig_value = np.linalg.norm(A.dot(x))
      x = A.dot(x) / eig_value
      eig_vector = x
      a = abs(eig_value-prev)
      prev = eig_value

    return eig_value, eig_vector

def main():
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    
    l,vec=largest_eig(H0)
    
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)
    
    psi0=evecs[:,0]
    norm_const=simps(abs(psi0)**2,x=grid)
    psi0=psi0/norm_const
    print(norm_const)

    psi0_ = vec*1.0
    norm_const=simps(abs(psi0_)**2,x=grid)
    psi0_=psi0_/norm_const
    print(norm_const)

    plt.plot(grid,abs(psi0)**2,label='scipy eig. vector squared')
    plt.plot(grid,abs(psi0_)**2,'r--',label='largest_eig vector squared')
    plt.legend(loc=0)
    plt.show()


if __name__=="__main__":
    main()
