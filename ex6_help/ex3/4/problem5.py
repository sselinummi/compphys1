"""
CompPhys1 exercise 3 problem 5

Calculation of electric field of a charged rod
"""

from pydoc import render_doc
import numpy as np
from scipy.integrate import simps
import matplotlib.pyplot as plt

def calculate_E_field_of_rod(x, rod_x, q):
    """
    Returns vector of electric field of a charged rod 
    at any point in space. The rod is located on x-axis.

    x : Cartesian coordinates of the estimation point
    rod_x : Grid of x coordinate values of the rod
    q : Charge of the rod
    """
    epsilon = 8.8541878128e-12
    charge_d = q/np.linalg.norm(rod_x[0]-rod_x[-1])
    rod = np.zeros((np.shape(rod_x)[0],3))
    rod[:,0] = rod_x
    r_hat = np.zeros((np.shape(rod)[0],3))
    r_hat[:] = x
    r_hat -= rod
    r = np.zeros(np.shape(r_hat))
    l = 0
    for i in range(np.shape(r_hat)[0]):
        l = np.linalg.norm(r_hat[i])
        r_hat[i] = r_hat[i] / l
        r[i] = l
    dE = 1/(4*np.pi*epsilon)*charge_d/(r**2)*r_hat
    E_field = np.zeros(3)
    E_field[0] = simps(dE[:,0], rod_x)
    E_field[1] = simps(dE[:,1], rod_x)
    E_field[2] = simps(dE[:,2], rod_x)
    return E_field

def main():
    # Comparison to analytical value
    L = 1
    q = 5
    d = 0.5
    x0 = np.array([L/2+d,0,0])
    rod_x = np.linspace(-L/2,L/2,50)

    numerical_E = calculate_E_field_of_rod(x0,rod_x,q)
    analytical_E_x = q/(4*L*np.pi*8.8541878128e-12)*(1/d-1/(d+L))
    print("Numerical: " + str(numerical_E[0]) + " Analytical: " + str(analytical_E_x))

    # Vizulation in 2D
    n = 20
    x = np.linspace(-1,1,n)
    y = np.linspace(-1,1,n)
    xx,yy = np.meshgrid(x,y, indexing='ij')
    E_field = np.zeros([n,n,3])
    for i in range(np.size(x)):
        for j in range(np.size(y)):
            E_field[i,j] = calculate_E_field_of_rod([x[i], y[j], 0],rod_x,q)

    plt.figure()
    plt.quiver(xx, yy, E_field[:,:,0], E_field[:,:,1])
    plt.title("Electric field of a positively charged rod on x-axis, L = 1 m")
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")
    plt.show()

    return

if __name__=="__main__":
    main()

