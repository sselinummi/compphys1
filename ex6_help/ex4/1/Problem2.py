"""Determining the number of electrons and reciprocal lattice """
#import needed libraries
from numpy import *
from scipy.integrate import simps
from read_xsf_example import read_example_xsf_density

def main():
    #import dft_chargedensity1.xs
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename) #read density, lattice, grid and shift
    #find alpha space
    A = lattice.T #find lattice matrix
    alpha1 = linspace(0,1,grid[0]) 
    alpha2 = linspace(0,1,grid[1])
    alpha3 = linspace(0,1,grid[2])
    detA = linalg.det(A) #find determinant of matrix A
    Ial = simps(simps(simps(rho, alpha1, axis = 0), alpha2, axis = 0), alpha3)*detA #calculate the number of electrons in a simulation cell
    #rho.reshape(grid[0[, grid[1], grid[2])
    rho = rho.reshape((91,91,121))
    #define x, y and z points
    x = linspace(0, 4.554599997, 91)
    y = linspace(0, 4.554599997, 91)
    z = linspace(0, 5.705599998, 121)
    #calculate the number of electrons in the simulation cell
    I = simps(simps(simps(rho, x ,axis = 0), y, axis = 0),z)
    #print calculations
    print('Number of electrons in the simulation cell for chargedensity1 claculated over alpha space is %.10f:', Ial)
    print('Number of electrons in the simulation cell for chargedensity1 claculated over density times volume is %.10f:', I)
    invA = linalg.inv(A) #find inverse of matrix A
    BT = 2*pi*invA #calculate the reciprocal lattice vectors transposed
    B = BT.T #find the reciprocal lattice vector
    #print vector
    print('Reciprocal lattice vector is:')
    print(B)
    
    #import dft_chargedensity2.xs
    filename = 'dft_chargedensity2.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename)#read density, lattice, grid and shift

    A = lattice.T #find lattice matrix
    detA = linalg.det(A)#find determinant of matrix A
    
    #find alpha space
    alpha1 = linspace(0, 1,grid[0])
    alpha2 = linspace(0,1,grid[1])
    alpha3 = linspace(0,1,grid[2])
    #print calculations
    I = simps(simps(simps(rho, alpha1,axis = 0),alpha2, axis = 0),alpha3)*detA #calculate the number of electrons
    invA = linalg.inv(A)#find inverse of matrix A
    BT = 2*pi*invA #calculate the reciprocal lattice vectors transposed
    B = BT.T #find the reciprocal lattice vector
    print('Number of electrons in the simulation cell for chargedensity2 is %.10f:', I)
    print('Reciprocal lattice vector is:')
    print(B)
    
    
    
if __name__=="__main__":
    main()


