""" Electron density along a line from chargedensity1"""
#import needed libraries
from numpy import *
from read_xsf_example import read_example_xsf_density
from scipy import interpolate
from spline_class import spline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

def main():
    #import dft_chargedensity1.xs
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename) #read density, lattice, grid and shift
    A = lattice.T #lattice matrix
    invA = linalg.inv(A) #find inverse of matrix A
    N = 400 #number of interpolation points
    #alpha space
    alpha1 = linspace(0, 1,grid[0])
    alpha2 = linspace(0,1,grid[1])
    alpha3 = linspace(0,1,grid[2])
    f = zeros((N,)) #initialize vector for spline evaluation
    t = linspace(0,1,N) #curve parameter
    r0 = array([0.1, 0.1, 2.8528]) #array of r0
    r1 = array([4.45, 4.45, 2.8528])#array of r1
    spl3d = spline(x=alpha1, y=alpha2, z=alpha3, f=rho, dims=3)#spline over alpha space with rho-charge density
    for i in range(N):
        r = r0 + t[i]*(r1-r0) #parametarized line connecting r0 and r1, for t = 0, r=r0, for t = 1, r=r1
        alpha11 = invA.dot(r)%1 #calculate alpha vector from vector r
        f[i] = spl3d.eval3d(alpha11[0], alpha11[1], alpha11[2]) #find electron density with spline evaluate
    #ploting space
    X = linspace(0.1, 4.45, N)
    Y = X
    t = X
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('Electron density along the line')
    ax.plot(t, f)#t for x=y=t
    ax.set_xlabel(r'$t$')
    ax.set_ylabel(r'$f(t)$')
    plt.show()
main()
