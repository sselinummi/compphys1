""" Electron density along a line for chargedensity2 """
#import needed libraries
from numpy import *
from read_xsf_example import read_example_xsf_density
from scipy import interpolate
from spline_class import spline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

def main():
    #import dft_chargedensity1.xs
    filename = 'dft_chargedensity2.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename) #read density, lattice, grid and shift
    A = lattice.T #find lattice matrix
    invA = linalg.inv(A) #invers of matrix A
    N = 400 #number of interpolation points
    #find alpha space over grid
    alpha1 = linspace(0, 1,grid[0])
    alpha2 = linspace(0,1,grid[1])
    alpha3 = linspace(0,1,grid[2])
    spl3d = spline(x=alpha1, y=alpha2, z=alpha3, f=rho, dims=3) #spline over alpha space with rho-charge density
    #array of coordinates for the first line
    r10 = array([-1.4466, 1.3073, 3.2115])
    r11 = array([1.4361, 3.1883, 1.3542])
    #array of coordinates for the second line
    r20 = array([2.9996, 2.1733, 2.1462])
    r21 = array([8.7516, 2.1733, 2.1462])
    #initialize vectors
    r1 = zeros((3,N))
    r2 = zeros((3,N))
    f1 = zeros((N,))
    f2 = zeros((N,))
    t = linspace(0,1,N)#curve parameter

    for i in range(N):
        r1[:,i] = r10 + t[i]*(r11-r10)#parametarized line connecting r0 and r1, for frst line
        r2[:,i] = r20 + t[i]*(r21-r20)#parametarized line connecting r0 and r1, for second line
        alpha11 = invA.dot(r1[:,i])%1 #calculate alpha vector from vector r1
        alpha22 = invA.dot(r2[:,i])%1 #calculate alpha vector from vector r2
        f1[i] = spl3d.eval3d(alpha11[0], alpha11[1], alpha11[2])#interpolate over first line
        f2[i] = spl3d.eval3d(alpha22[0], alpha22[1], alpha22[2])#interpolate over second line
    
    fig ,(ax1, ax2) = plt.subplots(1,2, figsize=(12,9))
    ax1.set_title('Electron density along the line1')
    ax2.set_title('Electron density along the line2')
    ax1.plot(t, f1)
    ax2.plot(t, f2)
    ax1.set_xlabel(r'$t$')
    ax1.set_ylabel(r'$f(t)$')
    ax2.set_xlabel(r'$t$')
    ax2.set_ylabel(r'$f(t)$')
    plt.show()
    
main()
