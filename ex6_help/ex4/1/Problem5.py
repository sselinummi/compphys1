""": Fourier transform with Python’s numpy """
#import needed libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter

#define butter bandpass function
def butter_bandpass(low, high, fs, order=5):
    nyq = 0.5 * fs #Nyquist sampler for converting signal
    low1 = low / nyq
    high1 = high / nyq
    b, a = butter(order, [low1, high1], btype='band')#butter bandpass calc
    return b, a

#define bandpass function
def butter_bandpass_filter(data, low, high, fs, order=5):
    b, a = butter_bandpass(low, high, fs, order=order)
    y = lfilter(b, a, data)#filter data
    return y

# ADD CODE: read in the data here 
data = np.loadtxt('signal_data.txt')
t = data[:,0]
f = data[:,1]

dt = t[1]-t[0]
N=len(t)

# Fourier coefficients from numpy fft normalized by multiplication of dt
F = np.fft.fft(f)*dt

# frequencies from numpy fftfreq
freq = np.fft.fftfreq(len(F),d=dt)

# inverse Fourier with numpy ifft (normalization removed with division by dt)
iF = np.fft.ifft(F/dt)

# positive frequencies are given as
# freq[:N//2] from above or freq = np.linspace(0, 1.0/dt/2, N//2)
#band frequencies
c_low = 40
c_high = 60
#sample rate
fs = 1/dt
b, a = butter_bandpass(c_low, c_high, fs, order=6) 
y = butter_bandpass_filter(f, c_low, c_high, fs, order=7)#filtering
F1 = np.fft.fft(y)*dt # Fourier coefficients from numpy of filtered signal
freq1 = np.fft.fftfreq(len(F1),d=dt)# filtered frequencies from numpy 
fig, ax = plt.subplots()
# plot over positive frequencies the Fourier transform
ax.plot(freq[:N//2], np.abs(F[:N//2]), '-', label = 'Fourier')
ax.set_xlabel(r'$f$ (Hz)')
ax.set_ylabel(r'$F(\omega/2\pi)$')
ax.plot(freq1[:N//2], np.abs(F1[:N//2]), '--', color = 'red', label = 'Filtered Fourier')
ax.legend(loc="upper right")
# plot the "signal" and test the inverse transform
fig, ax =  plt.subplots()
ax.plot(t, f,t,iF.real,'r--')
ax.set_xlabel(r'$t$ (s)')
ax.set_ylabel(r'$f(t)$')

plt.show()
