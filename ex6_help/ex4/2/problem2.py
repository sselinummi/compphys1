"""
Determining the number of electrons and reciprocal lattice
vectors for 2 different structures
"""

import numpy as np

from read_xsf_example import read_example_xsf_density
from scipy.integrate import simps

def main():
    
    # reading the density and lattice info from the given files
    filename1 = 'dft_chargedensity1.xsf'
    filename2 = 'dft_chargedensity2.xsf'
    rho1, lattice1, grid1, shift1 = read_example_xsf_density(filename1)
    rho2, lattice2, grid2, shift2 = read_example_xsf_density(filename2)
    
    # forming uniformly spaced grids for the integration
    # based on the lattice size information
    x1=np.linspace(0.0, lattice1[0][0], grid1[0])
    y1=np.linspace(0.0, lattice1[1][1], grid1[1])
    z1=np.linspace(0.0, lattice1[2][2], grid1[2])
    
    # taking the transpose of the lattice vector matrix to switch
    # the rows to become the columns for a correct result
    A1=lattice1.T

    # calculating the reciprocal lattice vectors from the 
    # equation B^T*A=2piI
    invA1=np.linalg.inv(A1)
    BT1=2.0*np.pi*invA1
    B1=BT1.T
    
    # calculating the number of electrons in the simulation cell
    N1=simps(simps(simps(rho1,x1,axis=0),y1,axis=0),z1)

    # forming uniformly spaced grids for the integration
    # based on the lattice size information in alpha space
    alpha1=np.linspace(0, 1, grid2[0])
    alpha2=np.linspace(0, 1, grid2[1])
    alpha3=np.linspace(0, 1, grid2[2])
    
    A2=lattice2.T
    
    invA2=np.linalg.inv(A2)
    BT2=2.0*np.pi*invA2
    B2=BT2.T
    
    N2=simps(simps(simps(rho2, alpha1,axis=0),alpha2 ,axis=0),alpha3)*np.linalg.det(A2)

    print('The number of electrons for structure 1: ',N1)
    print('The number of electrons for structure 2: ',N2)
    print('The reciprocal lattice vectors for structure 1:')
    print(B1)
    print('The reciprocal lattice vectors for structure 2:')
    print(B2)


if __name__=='__main__':
    main()