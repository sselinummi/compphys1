"""
Interpolating the electron density of a structure along two lines
using cubic hermite splines in 3D and visualizing the results
"""

import numpy as np
import matplotlib.pyplot as plt

from read_xsf_example import read_example_xsf_density
from scipy.integrate import simps
from spline_class import spline

def main():

    # reading the density and the lattice info from the given file
    filename = 'dft_chargedensity2.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename)
    
    N=400
    # taking the transpose of the lattice vector matrix to switch
    # the rows to become the columns for a correct result
    A=lattice.T
    invA=np.linalg.inv(A)
    # t is determining how much we move along the line
    t=np.linspace(0,1,N)

    # forming uniformly spaced grids for the spline initialization
    # based on the lattice size information in alpha space
    alpha1=np.linspace(0, 1, grid[0])
    alpha2=np.linspace(0, 1, grid[1])
    alpha3=np.linspace(0, 1, grid[2])
    
    f1=np.zeros_like(t)

    # changing the starting and ending points of the first line
    # to alpha space using the equation r=A*alpha
    r0=np.array([-1.4466,1.3073,3.2115])
    alpha_0=invA.dot(r0) 
    r1=np.array([1.4361, 3.1883, 1.3542])
    alpha_1=invA.dot(r1) 

    spl3d=spline(x=alpha1,y=alpha2,z=alpha3,f=rho,dims=3) 
    # moving along the line from alpha_0 towards alpha_1 and 
    # calculating the interpolation values for each point
    for i in range(N):
        alpha = alpha_0 + t[i]*(alpha_1-alpha_0)
        f1[i]=spl3d.eval3d(alpha[0],alpha[1],alpha[2])

    f2=np.zeros_like(t)
    
    # changing the starting and ending points of the second line
    # to alpha space using the equation r=A*alpha
    r0=np.array([2.9996, 2.1733, 2.1462])
    alpha_0=invA.dot(r0) 
    r1=np.array([8.7516, 2.1733, 2.1462])
    alpha_1=invA.dot(r1) %1
    
    for i in range(N):
        alpha = alpha_0 + t[i]*(alpha_1-alpha_0)
        f2[i]=spl3d.eval3d(alpha[0],alpha[1],alpha[2])
    
    # visualizing the results
    plt.figure()
    plt.plot(f1,label='First line')
    plt.plot(f2,label='Second line')
    plt.legend()
    plt.show()


if __name__=='__main__':
    main()