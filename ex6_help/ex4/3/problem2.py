import numpy as np
from read_xsf_example import read_example_xsf_density
from scipy.integrate import simps

"""Alexandra Karppinen 6.2.2022
Computational Physics 1, EX4, PR2"""

filename1 = 'dft_chargedensity1.xsf'
filename2 = 'dft_chargedensity2.xsf'

# Read file data
rho1, lattice1, grid1, shift1 = read_example_xsf_density(filename1)
rho2, lattice2, grid2, shift2 = read_example_xsf_density(filename2)

# Reciprocal lattice vectors are obtained by equation B^T*A=2pi*I
# Matrix defining real space lattice vectors
A1 = lattice1
A2 = lattice2
# A to the right side of equation
invA1 = np.linalg.inv(A1)
invA2 = np.linalg.inv(A2)
# B^T = 2pi*I*A^-1
BT1 = 2.0*np.pi*invA1
BT2 = 2.0*np.pi*invA2
# Transpose of B
B1 = BT1.T
B2 = BT2.T


print('Reciprocal lattice vector for data 1 :')
print(B1)
print('Reciprocal lattice vector for data 2 :')
print(B2) # For some reason this is not proper vector...

# Total numbers of electrons can be calculated using density rho
# For this we need to define alpha space on the grid
alpha11 = np.linspace(0, 1, grid1[0])
alpha12 = np.linspace(0, 1, grid1[1])
alpha13 = np.linspace(0, 1, grid1[2])

alpha21 = np.linspace(0, 1, grid2[0])
alpha22 = np.linspace(0, 1, grid2[1])
alpha23 = np.linspace(0, 1, grid2[2])

# Then integral of density over alpha grid
print('Electron number on data 1: ')
print(simps(simps(simps(rho1,alpha11,axis=0),alpha12,axis=0),alpha13)*np.linalg.det(A1))

print('Electron number on data 2: ')
print(simps(simps(simps(rho2,alpha21,axis=0),alpha22,axis=0),alpha23)*np.linalg.det(A2))
