import numpy as np
from read_xsf_example import read_example_xsf_density
from CompPhys1.exercise3.ex3_help.spline_class import spline

""""Alexandra Karppinen,
CompPhys1 2022, EX4, PR3"""

# Define variables from file
filename = 'dft_chargedensity1.xsf'
rho, lattice, grid, shift = read_example_xsf_density(filename)

# Inversion of lattice matrix
inv_lattice = np.linalg.inv(lattice)

# Define alpha space over the grid: needed for the spline interpolation
alpha1 = np.linspace(0,1,grid[0])
alpha2 = np.linspace(0,1,grid[1])
alpha3 = np.linspace(0,1,grid[2])

# Spline interpolation of the density
spl = spline(x=alpha1, y=alpha2, z=alpha3, f=rho, dims=3)

# Route to define the density on
# r1 - r0 = [4.45, 4.45, 2.8528] - [0.1, 0.1, 2.8528]
# = [4.35, 4.35, 0]
alpha = np.array([4.35, 4.35, 0])
r = lattice.dot(alpha)
alpha_out = inv_lattice.dot(r) % 1

# Evaluate electron density on the route
den = spl.eval3d(alpha_out[0], alpha_out[1], alpha_out[2])
print('Density is (#/Å^3):')
print(str(den[0][0][0]))