import numpy as np
from read_xsf_example import read_example_xsf_density
from CompPhys1.exercise3.ex3_help.spline_class import spline

""""Alexandra Karppinen,
CompPhys1 2022, EX4, PR3"""

# Define variables from file
filename = 'dft_chargedensity2.xsf'
rho, lattice, grid, shift = read_example_xsf_density(filename)

# Inversion of lattice matrix
lattice = lattice
inv_lattice = np.linalg.inv(lattice)

# Define alpha space over the grid: needed for the spline interpolation
alpha1 = np.linspace(0,1,grid[0])
alpha2 = np.linspace(0,1,grid[1])
alpha3 = np.linspace(0,1,grid[2])

# Spline interpolation of the density
spl = spline(x=alpha1, y=alpha2, z=alpha3, f=rho, dims=3)

# Routes to define the density on
# r1 - r0 = [1.4361, 3.1883, 1.3542] - [-1.4466, 1.3073, 3.2115]
# = [4.35, 4.35, 0]
alpha1 = np.array([2.8827, 1.881, -1.8573])
# 2nd route
# (8.7516, 2.1733, 2.1462)-(2.9996, 2.1733, 2.1462)
# = [5.752, 0, 0]
alpha2 = np.array([5.752, 0, 0])
r1 = lattice.dot(alpha1)
r2 = lattice.dot(alpha2)
alpha_out1 = inv_lattice.dot(r1) % 1
alpha_out2 = inv_lattice.dot(r2) % 1

# Evaluate electron density on the route
den1 = spl.eval3d(alpha_out1[0], alpha_out1[1], alpha_out1[2])
den2 = spl.eval3d(alpha_out2[0], alpha_out2[1], alpha_out2[2])
print('Density on 1st route is (#/Å^3):')
print(den1[0][0][0])
print('And on 2nd route: ')
print(den2[0][0][0])