import numpy as np
import matplotlib.pyplot as plt

""""Alexandra Karppinen,
CompPhys1 2022, EX4, PR3"""

# ADD CODE: read in the data here 
def main():

    # 5 a)
    # read filedata, save to vectors t and f
    # t = time
    # f = frequency
    filename = 'signal_data.txt'
    t = []
    f = []
    with open(filename) as file:
        for line in file:
            p = line.split()
            t.append(float(p[0]))
            f.append(float(p[1]))


    dt = t[1]-t[0]
    N=len(t)

    # Fourier coefficients from numpy fft normalized by multiplication of dt
    F = np.fft.fft(f)*dt

    # frequencies from numpy fftfreq
    freq = np.fft.fftfreq(len(F),d=dt)

    # inverse Fourier with numpy ifft (normalization removed with division by dt)
    iF = np.fft.ifft(F/dt)

    # positive frequencies are given as
    # freq[:N//2] from above or freq = np.linspace(0, 1.0/dt/2, N//2)

    fig, ax = plt.subplots()
    # plot over positive frequencies the Fourier transform
    ax.plot(freq[:N//2], np.abs(F[:N//2]))
    ax.set_xlabel(r'$f$ (Hz)')
    ax.set_ylabel(r'$F(\omega/2\pi)$')

    # plot the original signal and the modified signal
    fig, ax = plt.subplots()
    ax.plot(t, f, label='Original signal')
    ax.plot(t, F.real, 'r', label='Modified signal')
    ax.set_xlabel(r'$t$ (s)')
    ax.set_ylabel(r'$f(t)$')
    plt.legend()
    plt.savefig('pr5_figure.png')


    plt.show()
if __name__=="__main__":

    main()