import numpy as np
import matplotlib.pyplot as plt


# ADD CODE: read in the data here 
data = np.loadtxt("signal_data.txt")
t = data[:, 0]
f = data[:, 1]

dt = t[1]-t[0]
N = len(t)

# Fourier coefficients from numpy fft, normalization in the forward transform
norm = "forward"
F = np.fft.fft(f, norm=norm)

# frequency bins from numpy fftfreq
freq = np.fft.fftfreq(len(F), d=dt)

# inverse Fourier with numpy ifft
iF = np.fft.ifft(F, norm=norm)


# positive frequencies are given as
# freq[:N//2] from above or freq = np.linspace(0, 1.0/dt/2, N//2)

fig, ax = plt.subplots()
# plot over positive frequencies the Fourier transform
fsize = 18
ax.plot(freq[:N//2], np.abs(F[:N//2]))
ax.set_xlabel(r'$f$ (Hz)', fontsize=fsize)
ax.set_ylabel(r'$F(\omega/2\pi)$', fontsize=fsize)
 
# plot the "signal" and test the inverse transform
fig, ax = plt.subplots()
ax.plot(t, f,t,iF.real,'r--')
ax.set_xlabel(r'$t$ (s)')
ax.set_ylabel(r'$f(t)$')

# remove frequencies below 40 Hz and above 60 Hz
F_new = np.where(freq < 40.0, 0, F)
F_new = np.where(freq > 60.0, 0, F_new)

# Transform back to time space
f_new = np.fft.ifft(F_new, norm=norm)

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(t, f, label='Original signal')
plt.plot(t, f_new, 'r--', label='Band-pass filtered signal')

ax.set_xlabel(r'$t$ (s)')
ax.set_ylabel(r'$f(t)$')
ax.legend()

plt.show()
