"""
Calculate electron densities and number of electrons in a lattice.
"""
import numpy as np
from ex4_help import read_xsf_example as read_data
from scipy.integrate import simps
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, "../exercise3")
from ex3_help import spline_class as sp


def n_electrons(l_vecs1, l_vecs2, grid1, grid2, density1, density2):
    """
    Calculate number of electrons in the unit cell

    :param l_vecs1: lattice vectors for the file number 1
    :param l_vecs2: lattice vectors for the file number 2
    :param grid1: density matrix size
    :param grid2: density matrix size
    :param density1: number density per volume
    :param density2: number density per volume
    :return:
    """

    # We need to make transformation with using determinant of the Jacobian
    # The Jacobian is now the lattice matrix
    transform2 = np.linalg.det(l_vecs2)

    # Generate data points, we do not need coordinate transform here because it is already a rectangular
    x11 = np.linspace(0, l_vecs1[2][2], grid1[2])
    x21 = np.linspace(0, l_vecs1[1][1], grid1[1])
    x31 = np.linspace(0, l_vecs1[0][0], grid1[0])

    # Here we need the transformation
    x12 = np.linspace(0, 1, grid2[2])
    x22 = np.linspace(0, 1, grid2[1])
    x32 = np.linspace(0, 1, grid2[0])

    # jacobian = lattice vectors
    nelectrons1 = simps(simps(simps(density1, x11), x21), x31)
    print("Number of electrons in the file dft_chargedensity1.xsf: ", float(nelectrons1))
    nelectrons2 = simps(simps(simps(density2, x12), x22), x32) * transform2
    print("Number of electrons in the file dft_chargedensity2.xsf: ", float(nelectrons2))


def get_reciprocal_lattice_vecs(vecs1):
    """
    B^T * A = 2*pi*I, where A is lattice vectors. Solve for B (reciprocal lattice vectors)

    :param vecs1: original lattice vectors
    :return: reciprocal lattice vectors
    """
    A = vecs1

    I = 2*np.pi*np.eye(A.shape[0])
    B = np.matmul(I, np.linalg.inv(A)).transpose()
    return B


def e_density_along_a_line(density, grid1, lattice, r0, r1, fname):
    """
    Plot an electron density along a line defined by the points r0 and r1.

    See figures 'e_density_problem3.png' and 'line_problem3.png' .

    :param density: electron density
    :param grid1: density sizes
    :param lattice: lattice vectors
    :param r0: starting point
    :param r1: end point
    :return:
    """

    # parameter presentation:
    b1 = np.array(-r0+r1)
    b0 = r0
    # generate the line
    line_values = np.zeros(400)
    line_param = np.zeros(400)
    # 4.45 = b1[0]*t+0.1
    end_t = (r1[0]-r0[0]) / b1[0]
    ts = np.linspace(0, end_t, 400)     # or t=linspace(0,1,400)

    # Initialize spline in alpha space
    alpha1 = np.linspace(0, 1, grid1[0])
    alpha2 = np.linspace(0, 1, grid1[1])
    alpha3 = np.linspace(0, 1, grid1[2])
    spline3d = sp.spline(x=alpha1, y=alpha2, z=alpha3, f=density, dims=3)

    for ind, t in enumerate(ts):
        r = np.linalg.inv(lattice).dot(b1*t+b0) % 1  # line in alpha space
        result = spline3d.eval3d(r[0], r[1], r[2])
        line_values[ind] = result

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(ts, line_values, 'r-')
    ax.set_xlabel("t (parametrization variable)")
    ax.set_ylabel("Electron density along the line")
    #plt.savefig(fname, dpi=300)
    plt.show()



def main():
    # Get data, lattice vectors are already transposed
    filename1 = 'ex4_help/dft_chargedensity1.xsf'
    density1, l_vecs1, grid1, shift1 = read_data.read_example_xsf_density(filename1)
    filename2 = 'ex4_help/dft_chargedensity2.xsf'
    density2, l_vecs2, grid2, shift2 = read_data.read_example_xsf_density(filename2)

    n_electrons(l_vecs1, l_vecs2, grid1, grid2, density1, density2)
    reciprocal1 = get_reciprocal_lattice_vecs(l_vecs1)
    reciprocal2 = get_reciprocal_lattice_vecs(l_vecs2)
    np.set_printoptions(precision=5)
    np.set_printoptions(suppress=True)
    print("Reciprocal lattice vectors for the file 1: ")
    print(reciprocal1)

    print("Reciprocal lattice vectors for the file 2: ")
    print(reciprocal2)

    # Problem 3
    r0 = np.array([0.1, 0.1, 2.8528]).transpose()
    r1 = np.array([4.45, 4.45, 2.8528]).transpose()
    fname = "problem3_figures/e_density_line_problem3.png"
    e_density_along_a_line(density1, grid1, l_vecs1, r0, r1, fname)

    # Problem 4
    r0 = np.array([-1.4466, 1.3073, 3.2115]).transpose()
    r1 = np.array([1.4361, 3.1883, 1.3542]).transpose()
    fname = "problem4_figures/e_density_line1_problem4.png"
    e_density_along_a_line(density2, grid2, l_vecs2, r0, r1, fname)

    r0 = np.array([2.9996, 2.1733, 2.1462]).transpose()
    r1 = np.array([8.7516, 2.1733, 2.1462]).transpose()
    fname = "problem4_figures/e_density_line2_problem4.png"
    e_density_along_a_line(density2, grid2, l_vecs2, r0, r1, fname)


if __name__ == '__main__':
    main()
