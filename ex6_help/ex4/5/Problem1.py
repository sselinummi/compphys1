# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 19:39:42 2022

@author: mikam
"""

from numpy import *
import numpy as np
from scipy.integrate import simps
from read_xsf_example import read_example_xsf_density

def main():
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename)

    print(lattice[0][0])
    x = np.linspace(0,lattice[0][0],grid[2])
    y = np.linspace(0,lattice[1][1],grid[1])
    z = np.linspace(0,lattice[2][2],grid[0])
    X,Y,Z = meshgrid(x,y,z)
    electrons = simps(simps(simps(rho,x),y),z)
    
    vec = [lattice[0][0],lattice[1][1],lattice[2][2]]
    vec = vec/(max(vec))
    print(electrons)
    print(vec)
    
    rho, lattice, grid, shift = read_example_xsf_density('dft_chargedensity2.xsf')

    print(lattice[0][0])
    x = np.linspace(0,lattice[0][0],grid[2])
    y = np.linspace(0,lattice[1][1],grid[1])
    z = np.linspace(0,lattice[2][2],grid[0])
    X,Y,Z = meshgrid(x,y,z)
    electrons = simps(simps(simps(rho,x),y),z)
    
    vec = [lattice[0][0],lattice[1][1],lattice[2][2]]
    vec = vec/(max(vec))
    print(electrons)
    print(vec)
    
    
    
if __name__=="__main__":
    main()