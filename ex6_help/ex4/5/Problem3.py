# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 23:50:25 2022

@author: mikam
"""
from numpy import *
from spline_class import spline as sp
import numpy as np
import scipy.ndimage
import matplotlib.pyplot as plt
#from mayavi import mlab
from scipy.integrate import simps
from read_xsf_example import read_example_xsf_density

filename = 'dft_chargedensity1.xsf'
rho, lattice, grid, shift = read_example_xsf_density(filename)

#Creating the vectors
r0 = np.array([0.1, 0.1, 2.8528])
r1 = np.array([4.45, 4.45, 2.8528])

points = 400
f = zeros(points)
t = linspace(0,1,points)

alpha1 = np.linspace(0,1,grid[0])
alpha2 = np.linspace(0,1,grid[1])
alpha3 = np.linspace(0,1,grid[2])
#alpha = [alpha1, alpha2,alpha3]


spl3d = sp(x = alpha1,y = alpha2, z = alpha3,f = rho,dims=3)

A = np.transpose(lattice)
invA = np.linalg.inv(A)

#r = A.dot(alpha)
for i in range(points):
    r = r0+t[i]*(r1-r0)
    alpha = np.dot(invA,r)%1
    
    f[i] = spl3d.eval3d(alpha[0],alpha[1],alpha[2])
plt.plot(f)
   


#x,y,z = np.linspace(x0, x1, points), np.linspace(y0, y1, points),np.linspace(z0, z1, points)
#ff = scipy.ndimage.map_coordinates(rho,np.vstack((x,y,z)))

#xyz = np.vstack((rho[0],rho[0],rho[0]))
#figure = mlab.figure('DensityPlot')
#pts = mlab.points3d(rho[0], rho[0], rho[0], rho, scale_mode='none', scale_factor=0.07)
#mlab.show()

#ax1.plot3D(rho)

#plt.plot(ff)