# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 12:20:12 2022

@author: mikam
"""

from numpy import *
import numpy as np
import scipy.ndimage
import matplotlib.pyplot as plt
from spline_class import spline as sp
from scipy.integrate import simps
from read_xsf_example import read_example_xsf_density

filename = 'dft_chargedensity2.xsf'
rho, lattice, grid, shift = read_example_xsf_density(filename)


x1 = [-1.4466%lattice[0][0], 1.3073%lattice[1][1], 3.2115%lattice[2][2]]
x2 = [1.4361%lattice[0][0], 3.1883%lattice[1][1], 1.3542%lattice[2][2]]

#x21 = [2.9996%lattice[0][0], 2.1733%lattice[1][1], 2.1462%lattice[2][2]]
#x22 = [8.7516%lattice[0][0], 2.1733%lattice[1][1], 2.1462%lattice[2][2]]

x21 = [2.9996, 2.1733, 2.1462]
x22 = [8.7516, 2.1733, 2.1462]


r00 = np.array(x1)
r01 = np.array(x2)

r21 = np.array(x21)
r22 = np.array(x22)
points = 400


f1 = zeros(points)
f2 = zeros(points)
t = linspace(0,1,points)

alpha1 = np.linspace(0,1,grid[0])
alpha2 = np.linspace(0,1,grid[1])
alpha3 = np.linspace(0,1,grid[2])
#alpha = [alpha1, alpha2,alpha3]

spl3d = sp(x = alpha1,y = alpha2, z = alpha3,f = rho,dims=3)

A = np.transpose(lattice)
invA = np.linalg.inv(A)

fig, axs = plt.subplots(2)
for i in range(points):
    r = r00+t[i]*(r01-r00)
    alpha = np.dot(invA,r)%1
    
    f1[i] = spl3d.eval3d(alpha[0],alpha[1],alpha[2])
    
for i in range(points):
    r = r21+t[i]*(r22-r21)
    alpha = np.dot(invA,r)%1
    
    f2[i] = spl3d.eval3d(alpha[0],alpha[1],alpha[2])    
axs[0].plot(f1)
axs[1].plot(f2,color = "red")
axs[0].legend(["first line"])
axs[1].legend(["Second line"])