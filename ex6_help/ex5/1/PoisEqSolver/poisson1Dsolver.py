import numpy as np


def solve_poisson1D(f, dx, bound_conds):
    """
    :param bound_conds: list of tuples tuple (index of x, value)
    """

    A = np.zeros((f.shape[0], f.shape[0]))
    A = A + np.diagflat(-2 * np.ones_like(f))
    A = A + np.diagflat(np.ones(f.shape[0]-1), 1)
    A = A + np.diagflat(np.ones(f.shape[0]-1), -1)

    for cond in bound_conds:
        A[cond[0]] = np.zeros(f.shape[0])
        A[cond[0]][cond[0]] = 1

    A /= dx**2

    z = np.linalg.solve(A, f)
    return z

