import numpy as np


def jacobi_update(phi, f, step_size=0.25):
    """
    Do one Jacobi update.
    :param phi: function to update. It should contain all the initial boundary conditions already
    :param f: right-hand side of the PDE
    :step_size: step size of the update, by default use the maximum step size
    """
    npoints = f.shape[0]
    phi_new = np.copy(phi)
    for ind_x in range(1, npoints-1):
        for ind_y in range(1, npoints-1):
            xf = ind_x + 1
            xb = ind_x - 1
            yf = ind_y + 1
            yb = ind_y - 1
            phi_new[ind_x][ind_y] = step_size * (phi[xf][ind_y] + phi[xb][ind_y] + phi[ind_x][yf] + phi[ind_x][yb]) \
                                             - f[ind_x][ind_y]
    return phi_new


def gauss_seidel_update(phi, f, step_size=0.25, omega=1):
    """
    Do one Gauss-Seidel update. If omega is provided, this method is similar to SOR.
    :param phi: function to update. It should contain all the initial boundary conditions already
    :param f: right-hand side of the PDE
    :step_size: step size of the update, by default use the maximum step size
    """

    npoints = f.shape[0]
    phi_new = np.copy(phi)
    for ind_x in range(1, npoints-1):
        for ind_y in range(1, npoints-1):
            xf = ind_x + 1
            xb = ind_x - 1
            yf = ind_y + 1
            yb = ind_y - 1
            phi_new[ind_x][ind_y] = (1 - omega)*phi[ind_x][ind_y] + omega*step_size * \
                                    (phi[xf][ind_y] + phi_new[xb][ind_y] + phi[ind_x][yf] + \
                                        phi_new[ind_x][yb]) - omega*f[ind_x][ind_y]
    return phi_new


def sor_update(phi, f, omega, step_size=0.25):
    """
    Do one simultaneous over relaxation update.
    :param phi: function to update. It should contain all the initial boundary conditions already
    :param f: right-hand side of the PDE
    :param omega: tuning parameter [1, 2]
    :step_size: step size of the update, by default use the maximum step size
    """
    return gauss_seidel_update(phi, f, step_size=step_size, omega=omega)