import unittest
from PoisEqSolver.poisson1Dsolver import solve_poisson1D
import numpy as np
import matplotlib.pyplot as plt


class Test1DPoisson(unittest.TestCase):

    def test_linear_spacing(self):

        # Initialize the picture for the results plotting
        fig = plt.figure(figsize=(10, 10))
        plt.rcParams['text.usetex'] = True      # This needs latex installation on the pc

        ns = [5, 11, 21, 40]
        # Test with different spacing
        for ind_n, n in enumerate(ns):
            x = np.linspace(0, 1, n)

            f = -np.pi**2 * np.sin(np.pi * x)
            dx = x[1] - x[0]

            # Find index for the second condition. Here we know that it should be the last element
            # but this is for the generality.
            cond2_x = 1
            # coordinate is in the array
            if cond2_x in x:
                ind = np.where(cond2_x == x)
                ind = int(ind[0])
            # find the closest index
            else:
                ind = np.digitize(cond2_x, x, right=True)

            # Boundary conditions
            bound_conds = [(0, 0), (ind, 0)]

            z = solve_poisson1D(f, dx, bound_conds)

            true_z = np.sin(np.pi * x)

            # Calculate mean absolute error
            mae = np.mean(abs(z - true_z))

            plot_number = int('22'+str(ind_n+1))
            ax = fig.add_subplot(plot_number)
            x_dense = np.linspace(0, 1, 200)
            ax.plot(x_dense, np.sin(np.pi * x_dense), 'r-', label="Analytical")
            ax.plot(x, z, 'b*', label="2nd order centered difference, N={}".format(n))
            ax.set_xlabel("$x$", fontsize=16)
            ax.set_ylabel("$\phi (x)$", fontsize=16)
            ax.legend()
            mae_txt = "MAE: {:.4f}".format(mae)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.8)
            ax.text(0.05, 0.9, mae_txt, transform=ax.transAxes, fontsize=15, bbox=props)

        plt.show()

