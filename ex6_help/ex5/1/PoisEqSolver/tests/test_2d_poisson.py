import unittest
import numpy as np
from PoisEqSolver import poisson2Dsolver
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import logging


def _run_solver(solver, phi, plot_num, x, y, f, log, fig, omega=1.0):
    """
    Solve poisson equation with a given solver and initialized result matrix phi.
    """
    etol = 10e-6
    error = 1000  # just a big number
    # Do the update until converge -> steady state value (assuming not chaotic or periodic region)
    niters = 0  # counter
    while error > etol:
        if omega != 1:
            phi_new = solver(phi, f, omega)
        else:
            phi_new = solver(phi, f)
        error = np.mean(abs(phi_new - phi))
        phi = phi_new
        niters += 1
    print("Method: {}".format(solver.__name__))
    log.debug("Method: {}".format(solver.__name__))
    print("Number of iterations: {}".format(niters))
    log.debug("Number of iterations: {}".format(niters))

    # Grid for plotting
    xx, yy = np.meshgrid(x, y, indexing='ij')

    fsize = 20
    ax = fig.add_subplot(plot_num, projection='3d')
    ax.plot_wireframe(xx, yy, phi, rstride=1, cstride=1)
    ax.set_xlabel("$x$", fontsize=fsize)
    ax.set_ylabel("$y$", fontsize=fsize)
    ax.set_zlabel("$\phi(x,y)$", fontsize=fsize)
    ax.set_title(solver.__name__)


class Test2DPoisson(unittest.TestCase):
    def test_symmetrical_grid(self):
        log = logging.getLogger("Test2DPoissson, test_symmetrical_grid")

        y = x = np.linspace(0, 1, 50)
        n = x.shape[0]
        f = np.zeros((n, y.shape[0]))
        phi = np.zeros_like(f)

        # Boundary conditions
        phi[0, :] = 1
        phi[1:, 0] = 0
        phi[1:, n-1] = 0
        phi[n-1, :] = 0

        fig = plt.figure()
        plt.rcParams['text.usetex'] = True

        _run_solver(poisson2Dsolver.jacobi_update, phi, 131, x, y, f, log, fig)
        _run_solver(poisson2Dsolver.gauss_seidel_update, phi, 132, x, y, f, log, fig)
        _run_solver(poisson2Dsolver.sor_update, phi, 133, x, y, f, log, fig, omega=1.8)
        plt.show()







