"""
Solve the trajectory of a charged particle exhibiting Lorentz force. I solved this in a bit weird and unefficient
way, but it should work.
"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as sp_int
from mpl_toolkits.mplot3d import Axes3D


def equation_of_motion(t, v, e, b):

    # velocity and acceleration and position
    vx, vy, vz, ax, ay, az, x, y, z = v
    #vx, vy, vz = v
    # Magnetic field, normalized with q/m
    bx = b[0]
    by = b[1]
    bz = b[2]
    # Electric field, normalized with q/m
    ex = e[0]
    ey = e[1]
    ez = e[2]

    # F=ma, where a=dv/dt and F=q(E + (v x B))
    # therefore solve dvdt=q/m*(E + (v x B)) for v
    # If we take derivative of both sides with respect to time, we get ODE for acceleration
    # then the position of the particle can be solved iteratively as x1 = 1/2*a*dt^2 + v*dt+ x0
    # qE/m = e
    # qB/m = b
    dvxdt = (ex + vy*bz - vz*by)
    dvydt = (ey + vz*bx - vx*bz)
    dvzdt = (ez + vx*by - vy*bx)

    # acceleration assuming the electric field and the magnetic fields are constants respect to time
    # d/dt(E + v x B) = dE/dt + dv/dt x B + v x dB/dt = 0 + dv/dt x B + o = dv/dt x B = a x B, where a is acceleration
    daxdt = (ay*bz - az*by)
    daydt = (az*bx - ax*bz)
    dazdt = (ax*by - ay*bx)

    dxdt = vx
    dydt = vy
    dzdt = vz

    dvdt = [dvxdt, dvydt, dvzdt, daxdt, daydt, dazdt, dxdt, dydt, dzdt]
    return dvdt


def solve_trajectory():

    # Electric and magnetic field
    e = 0.05
    E = np.array([e, 0, 0])
    b = 4
    B = np.array([0, b, 0])

    # Initial conditions
    t0 = 0
    v0 = [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0]
    t = np.linspace(0, 5, 201)
    dt = t[1] - t[0]
    tspan = (t0, 5)
    # solve velocities accelerations and positions
    vs = sp_int.solve_ivp(equation_of_motion, tspan, v0, t_eval=t, args=(E, B))
    vs = vs.y.transpose()
    # Find location respect to time
    r0 = np.array([0, 0, 0])
    locs = np.zeros((t.shape[0], 3))
    locs[0] = r0
    for ind, v in enumerate(vs):
        if ind > 0:
            # x_new = 1/2*a*dt^2 + v*dt + x_start
            locs[ind] = 0.5*v[3:6]*dt**2 + v[:3]*dt + locs[ind-1]

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(locs[:, 0], locs[:, 1], locs[:, 2], label="Position calculated implicitly")
    ax.plot(vs[:, 6], vs[:, 7], vs[:, 8], label="Position calculated with solve_ivp")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.legend()

    print("Coordinates at time t=5: ", vs[-1, 6], vs[-1, 7], vs[-1, 8])
    print("Velocity vector at time t=5: ", vs[-1][:3])

    plt.show()


solve_trajectory()
