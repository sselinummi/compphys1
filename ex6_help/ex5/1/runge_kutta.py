import numpy as np
from matplotlib.pyplot import *
from scipy.integrate import odeint
from scipy.integrate import solve_ivp


def runge_kutta4(x,t,dt,func,**kwargs):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    Need to complete the routine below
    where it reads '...' !!!!!!!
    
    See Computational Physics 1 lecture notes.
    """

    F1 = F2 = F3 = F4 = 0.0*x
    if ('args' in kwargs):
        args = kwargs['args']
        F1 = func(x,t,*args)
        F2 = func(x + dt * 0.5 * F1, t + dt * 0.5, *args)
        F3 = func(x + dt * 0.5 * F2, t + dt * 0.5, *args)
        F4 = func(x + dt * F3, t + dt, *args)
    else:
        F1 = func(x,t)
        F2 = func(x + dt * 0.5 * F1, t + dt * 0.5)
        F3 = func(x + dt * 0.5 * F2, t + dt * 0.5)
        F4 = func(x + dt * F3, t + dt)

    x_new = x + dt / 6 * (F1 + 2*F2 + 2*F3 + F4)

    return x_new, t+dt


def pend(y, t, b, c):
    """
    Pendulum: d^2theta/dt^2=-b*dtheta/dt-c*sin(theta)
    """
    theta, omega = y
    dydt = [omega, -b*omega - c*np.sin(theta)]
    return np.array(dydt)


def pend_ivp(t,y):
    """
    Initialize b and c for
    pendulum: d^2theta/dt^2=-b*dtheta/dt-c*sin(theta)
    """
    b = 0.25
    c = 5.0
    return pend(y,t,b,c)


def odeint_test(ax):
    """
    Solve pendulum with odeint
    """
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    # odeint uses lsode solver from the FORTRAN by default
    sol = odeint(pend, y0, t, args=(b, c))
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    return sol


def runge_kutta_test(ax):
    """
    Solve pendulum with a custom made RK4 method
    """
    b = 0.25
    c = 5.0    
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    # use fixed step size
    dt = t[1]-t[0]
    sol=[]
    x=1.0*np.array(y0)
    for i in range(len(t)):
        sol.append(x)
        x, tp = runge_kutta4(x,t[i],dt,pend,args=(b,c))
    sol=np.array(sol)
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    return sol


def solve_ivp_test(ax):
    """
    Use solve_ivp method to solve the pendulum.
    """
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    # uses RK45 (RK5 where 4th order is used fpr the variable step selection -> adaptive step, explicit method) by default
    # explicit method = use the state at the current time to calculate a new state
    # implicit method = use also the new state to calculate the new state fully
    sol = solve_ivp(pend_ivp, (0,10), y0, t_eval=t)
    ax.plot(sol.t, sol.y[0,:], 'b', label='theta(t)')
    ax.plot(sol.t, sol.y[1,:], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    return sol


def main():

    fig=figure()
    ax1=fig.add_subplot(131)
    ax2=fig.add_subplot(132)
    ax3=fig.add_subplot(133)
    sol_rk45 = solve_ivp_test(ax1)
    ax1.set_title('solve_ivp')
    sol_lsoda = odeint_test(ax2)
    ax2.set_title('odeint')
    sol_rk4 = runge_kutta_test(ax3)
    ax3.set_title('own Runge-Kutta 4')

    print("Largest absolute error (rk4 vs rk45): ", np.amax(abs(sol_rk4 - sol_rk45.y.transpose())))
    print("Largest absolute error (rk4 vs lsoda): ", np.amax(abs(sol_rk4 - sol_lsoda)))
    show()
    


if __name__=="__main__":
    main()
