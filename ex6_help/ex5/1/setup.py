from setuptools import setup

setup(name="PoissonEqSolver",
      version='0.1',
      description="Solver for the 1D Poisson Equation",
      url="https://gitlab.com/IiroAhokainen/computationalphysics1",
      author="Iiro Ahokainen",
      author_email="iiro.ahokainen@tuni.fi",
      license="MIT",
      packages=["PoisEqSolver"],
      install_requires=["numpy"],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'])
