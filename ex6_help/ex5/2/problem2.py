import numpy as np
import problem_1_runge_kutta
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


'''
This script simulates the trajectory of a charged particle 
in EM field, i.e. under the influence of Lorentz force:

F = ma -> a = F/m
Then if we set
u = [r_i, v_i],
we have
du/dt = [v_i, a_i] = [v_i, F_i/m],
where F = q(E + v x B)

In the case of qE/m and qB/m having known values q and m cancel out
'''


def lorentz_force(u,t,args):
    """ Caluculate Lorentz force for particle.
    u = r and v matrix
    t = time
    args = E and B field
    """

    r = u[:3]
    v = u[3:]
    E, B = args
    eq = np.empty(len(u))
    eq[:3] = v
    eq[3:] = E + np.cross(v, B)
    return eq


def main():

    # Set system electric field and magnetic field
    # qE/m = 0.05  and qB/m = 4.0
    # q and m cancel out

    E = 0.05
    B = 4.0
    E = E * np.array([1.0, 0.0, 0.0])   # In x direction
    B = B * np.array([0.0, 1.0, 0.0])   # In y direction

    args = E, B

    # Initialize simulation
    N = 100
    t_0 = 0
    t_f = 5
    t = np.linspace(t_0, t_f, N)
    v_0 = np.array([0.1, 0.1, 0.1])
    r_0 = np.array([0.0, 0.0, 0.0])
    # Convert  r_0 and v_0 into matrix array
    u = np.hstack((r_0, v_0))

    # Solve diff equation with RK4 implementation from problem 1
    sol = np.empty((len(u),N))
    for i in range(N-1):
        sol[:,i] = u
        dt = t[i+1] - t[i]
        u, tp = problem_1_runge_kutta.runge_kutta4(u,t[i],dt,lorentz_force,args=(args,))
    sol[:, len(t)-1] = u

    # Print velocity components at last time step
    v_x = sol[3][N-1]
    v_y = sol[4][N-1]
    v_z = sol[5][N-1]
    print('Velocity vector at t = 5:')
    print('x:',v_x)
    print('y:',v_y)
    print('z:',v_z)


    # Plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot(sol[0,:], sol[1,:], sol[2,:], label='Trajectory')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.legend(loc='best')
    fig.suptitle('Charged particle in magnetic field')
    plt.show()

main()
