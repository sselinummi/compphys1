import numpy as np
import matplotlib.pyplot as plt


def numerical_solution(a,b,N):
    '''
    Solves 1d Poisson probelm with finite difference method
    epsilon_0 cancels out
    a = start point x_a
    b = end point x_b
    N = number of points
    returns x values and phi as sol
    '''

    # Initialize problem
    dx = (b-a) / N
    x = np.linspace(a,b,N+1)
    x_calc = x[1:-1]    # Leave boundary points out of matrix calc

    # Construct matrix for 1d case
    A1 = np.diagflat(-2*np.ones(N-1),0)
    A2 = np.diagflat(1*np.ones(N-2),1)
    A3 = np.diagflat(1*np.ones(N-2),-1)
    A = A1+A2+A3
    A = A/(dx**2)

    rho = -np.pi**2 * np.sin(np.pi*x_calc)  # known solution
    b = np.linalg.solve(A,rho)              # solve matrix eq
    sol = [0]                               # Add boundary point
    sol.extend(b.copy())                    # Add solution
    sol.append(0)                           # Add second bp
    return x,sol


def analytical_solution(a,b,N=100):
    '''
    Calculates analytical solution values for text case 
    a = start point x_a
    b = end point x_b
    N = number of points to be calculated, default 100
    returns x grid and corresponding phi values
    '''

    x = np.linspace(a,b,N)
    y = np.sin(np.pi*x)
    return x,y


def mean_absolute_error(N,x_numerical,numerical_solution):
    '''
    Calculates mean value error (MAE)
    N = number of values
    x_numerical = x values to be considered
    numerical_solution = phi values at x_numerical points
    returns MAE
    '''

    analytical_values = np.sin(np.pi*x_numerical)
    mae = np.sum(np.absolute((numerical_solution - analytical_values)))
    print()
    return mae


def convergence_test(a,b,N_list):
    '''
    Convergence test for fun
    a = start point x_a
    b = end point x_b
    N_list = list of N values to be calculated
    returns a list of MAE values for each N
    '''

    mae_list = []
    for i in range(len(N_list)):
        x,sol = numerical_solution(a,b,N_list[i])
        x1,y1 = analytical_solution(a,b)
        mae_list.append(mean_absolute_error(N_list[i],x,sol))
        i+=1
    return mae_list


def main():
    a = 0.0
    b = 1.0

    # Run convergence test. Note: start at N=2 minimum
    N_list = np.arange(2,101)
    maes = convergence_test(a,b,N_list)

    # Print wanted MAE values from the list calculated previously
    N = [5,11,21,40]
    print("Mean absolute errors:")
    print("{0:<5} {1:<15}".format('N','MAE'))
    for i in range(len(N)):
       print("{0:<5} {1:<15}".format(N[i],maes[i]))
    
    fig = plt.figure()
    plt.plot(N_list,maes)
    plt.xlabel('N')
    plt.ylabel('Mean absolute error')
    plt.title('Convergence test')
    
    # Plot N=11 case as an example
    N = 11
    x,sol = numerical_solution(a,b,N)
    x1,y1 = analytical_solution(a,b)
    
    fig = plt.figure()
    plt.scatter(x,sol,s=15,label='Finite difference: $N={}$'.format(N))
    plt.plot(x1,y1,'r--' ,label='Analytical')
    plt.legend()
    plt.xlabel('x')
    plt.ylabel(r'$\Phi(x)$')
    plt.title('Poisson')
    plt.show()

    plt.show()

    # For some reason running this script clears previous output from terminal
    # At least for me but runs well anyways :)


if __name__=="__main__":
    main()
