import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


def jacobi_next(phi, rho, h):
    """
    Iterates using Jacobi method.
    phi = initial phi value to be used in i+1
    rho = right hand side step
    h = step size
    returns system on next time step
    """
    phi_next = np.full_like(phi, fill_value=float("nan"))
    nx = phi.shape[0]
    ny = phi.shape[0]
    phi_next[[0,nx-1],:] = phi[[0,nx-1],:]
    phi_next[:, 0] = phi[:, 0]
    phi_next[:, ny-1] = phi[:, ny-1]
    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi_next[i,j] = 1/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1] + phi[i,j-1] + h**2 * rho[i,j])
    return phi_next


def gauss_seidel_next(phi, rho, h):
    """
    Iterates using gauss seidel method. See above for parameters
    """
    nx = phi.shape[0]
    ny = phi.shape[0]
    phi = phi.copy()
    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi[i,j] = 1/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1] + phi[i,j-1] + h**2 * rho[i,j])
    return phi


def sor_next(phi, rho, h, w):
    """
    Iterates using SOR method. Parameters as above except:
    w = omega, value given
    """
    phi = phi.copy()
    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi[i,j] = (1 - w) * phi[i,j] + w/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1]+ phi[i,j-1] + h**2 * rho[i,j])
    return phi


def test_jacobi_method(tol, initial, rho, h):
    '''
    Solves the Poisson equation problem using jacobi method.
    tol = tolerance option to determine accuracy
    initial = initial state
    rho = right hand side step
    h = step size
    returns solution and number of iterations
    '''
    state = initial.copy()
    state_prev = initial.copy() - 1
    counter = 0
    while tol > (np.abs(state_prev - state) < tol).all():
        state_prev = state
        state = jacobi_next(state, rho, h)
        counter = counter + 1
        if counter > 1e6:
            print("Tolerance option not met")
            break
    print("Jacobi method iterations: {}".format(counter))
    return state, counter


def test_gauss_seidel_method(tol, initial, rho, h):
    '''
    Solve the probelm using gauss seidel method. Similar parameters as above
    '''
    state = initial.copy()
    state_old = initial.copy() - 1
    counter = 0
    while tol > (np.abs(state_old - state) < tol).all():
        state_old = state
        state = gauss_seidel_next(state, rho, h)
        counter = counter + 1
        if counter > 1e6:
            print("Tolerance option not met")
            break
    print("Gauss-Seidel method iterations: {}".format(counter))
    return state, counter


def test_sor(tol, initial, rho, h, w = 1.8):
    """
    Solve the probelm using simultaneous over relazation (SOR) method
    Similar parameters as above except
    w = omega, given value
    """
    state = initial.copy()
    state_prev = initial.copy() - 1
    counter = 0
    while tol > (np.abs(state_prev - state) < tol).all():
        state_prev = state
        state = sor_next(state, rho, h, w)
        counter += 1
        if counter > 1e6:
            print("Tolerance option not met")
            break
    print("SOR interations: {}".format(counter))
    return state, counter


def main():
    # Initialize parameters
    nx, ny = 25, 25
    x_0, y_0 = 0.0, 0.0
    x_L, y_L = 1.0, 1.0
    h = x_L / nx
    x = np.linspace(x_0, x_L, nx)
    y = np.linspace(y_0, y_L, ny)
    X, Y = np.meshgrid(x, y)
    tol = 1e-6

    # Define initial condition
    state_0 = np.zeros((nx,ny))
    state_0[[0,nx-1],:] = 0.0
    state_0[:, 0] = 0.0
    state_0[:, ny-1] = 1.0
    rho = - np.ones((nx,ny))

    # Run all test functions
    state_jacobi, iterations_jacobi= test_jacobi_method(tol, state_0, rho, h)
    state_gauss_seidel, iterations_gs = test_gauss_seidel_method(tol, state_0, rho, h)
    state_sor, iterations_sor = test_sor(tol, state_0, rho, h)

    # Plot initial condition and all three relaxation solutions
    fig = plt.figure(figsize=(7,7))
    ax1 = fig.add_subplot(221, projection="3d")
    ax1.plot_wireframe(X, Y, state_0, rstride=1, cstride=1)
    ax1.set_title("Initial condition")
    ax2 = fig.add_subplot(222, projection="3d")
    ax2.plot_wireframe(X, Y, state_jacobi, rstride=1, cstride=1)
    ax2.set_title("Jacobi solution ({} iterations)".format(iterations_jacobi))
    ax3 = fig.add_subplot(223, projection="3d")
    ax3.plot_wireframe(X, Y, state_gauss_seidel, rstride=1, cstride=1)
    ax3.set_title("Gauss Seidel solution ({} iterations)".format(iterations_gs))
    ax4 = fig.add_subplot(224, projection="3d")
    ax4.plot_wireframe(X, Y, state_sor, rstride=1, cstride=1)
    ax4.set_title("SOR solution ({} iterations)".format(iterations_sor))
    fig.suptitle('Poisson equation relaxation methods')
    plt.show()

if __name__ == "__main__":
    main()
