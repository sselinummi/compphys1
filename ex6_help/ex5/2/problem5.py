from matplotlib import pyplot as plt
from matplotlib import image as image
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

'''
Copy pasted the functions for Jacobi method to be used in solving equation
Something wrong with the implementation in the plate placing but somewhat ok
'''

def jacobi_next(phi, rho, h):
    phi_next = np.full_like(phi, fill_value=float("nan"))
    nx = phi.shape[0]
    ny = phi.shape[0]
    phi_next[[0,nx-1],:] = phi[[0,nx-1],:]
    phi_next[:, 0] = phi[:, 0]
    phi_next[:, ny-1] = phi[:, ny-1]
    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi_next[i,j] = 1/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1] + phi[i,j-1] + h**2 * rho[i,j])
    return phi_next

def jacobi_method(tol, initial, rho, h):
    state = initial.copy()
    state_prev = initial.copy() - 1
    counter = 0
    while tol > (np.abs(state_prev - state) < tol).all():
        state_prev = state
        state = jacobi_next(state, rho, h)
        counter = counter + 1
        if counter > 1e6:
            print("Tolerance option not met")
            break
    print("Jacobi method iterations: {}".format(counter))
    return state, counter


def main():
    # Create grid
    nx, ny = 21, 21
    x_0, y_0 = -1.0, -1.0
    x_L, y_L = 1.0, 1.0
    h = x_L / nx
    x = np.linspace(x_0, x_L, nx)
    y = np.linspace(y_0, y_L, ny)
    X, Y = np.meshgrid(x, y)
    tol = 1e-6

    # initial grid with the points
    initial = np.zeros((nx,ny))

    # Boundary condition to set edge to zero
    initial[[0,nx-1],:] = 0.0
    initial[:, [0, ny-1]] = 0.0
    rho = np.zeros((nx,ny))

    # Create plates with charge +/-1 in 21x21 grid at wanted location
    initial[7:13, 5] = -1.0
    initial[7:13, 15] = 1.0

    #print(initial)

    state_sor, iters = jacobi_method(tol, initial, rho, h)

    # Plot all
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # Take the gradient for E field
    grad_y, grad_x = np.gradient(state_sor)
    ax.quiver(X, Y, -grad_x, -grad_y)
    ax.plot([-0.3, -0.3], [-0.5, 0.5], c="b", linewidth=3)
    ax.plot([0.3, 0.3], [-0.5, 0.5], c="r", linewidth=3)
    ax.set_title("Electric field")
    plt.show()

if __name__ == "__main__":
    main()

