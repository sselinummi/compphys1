"""
Solving the trajectory of a charged particle under the influence of 
the Lorenz force using scipy's odeint and visualizing the result.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D


def func(x,t,E,B):

    """
    Function for forming the equations of motion of the particle in
    a way that the trajectory can be solved using an ordinary differential
    equation solver.
    Equations of motion: dv/dt=1/m*F=E+v×B and dr/dt=v
    => xx=[dv/dt,dr/dt]=[dv_x/dt,dv_y/dt,dv_z/dt,v_x,v_y,v_z]


    x : array, initial state vector
    t : array, time
    E : array, electric field(multiplied by q/m)
    B : array, magnetic field(multiplied by q/m)
    """
    xx=np.zeros_like(x)
    xx[0:3]=E+np.cross(x[0:3],B)
    xx[3:]=x[0:3]
    return xx


def main():
    E=[0.05,0,0] # is actually E multiplied by q/m
    B=[0,4.0,0] # is actually B multiplied by q/m
    
    # initial condition in a form of [v_x,v_y,v_z,r_x,r_y,r_z]
    x=[0.1,0.1,0.1,0.0,0.0,0.0] 
    
    t = np.linspace(0, 5, 101)
    sol = odeint(func, x, t, args=(E, B))
    print('Velocity vector at time t=5: ', sol[-1,0:3])
    
    # visualizing the solution
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(sol[:,3],sol[:,4],sol[:,5])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

    
if __name__=="__main__":
    main()