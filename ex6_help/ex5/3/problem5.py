"""
Solving the potential profile of a 2D box containing two capacitor plates
and calculating the electric field inside the box. Visualizing of the results
is included.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def capacitor_plates():
    # setting boundary conditions
    L=2.0
    h=0.1
    N=int(L/h+1)
    x=y=np.linspace(-L/2,L/2,N)
    Phi=np.zeros((N,N))
    rho=np.zeros((N,N))

    # adding the plates in the grid
    Phi[7,5:15]=-1.0
    Phi[13,5:15]=1.0
    X,Y=np.meshgrid(x,y)
    
    good_accurary=False
    tolerance=1.0e-4
    i=1
    omega=1.8
    # performing the SOR method untill
    # wanted accuracy has been achieved
    while not good_accurary:
        old_Phi=1.0*Phi
        Phi=sor_method(Phi,rho,h,N,omega)
        if np.amax(abs(Phi-old_Phi)) < tolerance:
            good_accurary=True
        i+=1
    
    print('Loop amount:',i)
    
    # visualizing the solution
    fig=plt.figure()
    ax=fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(X.T,Y.T,Phi,rstride=1,cstride=1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('Solution')

    # calculating the electric field
    Ex,Ey=np.multiply(-1,np.gradient(Phi))

    # visualizing the electric field as a vector field
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.quiver(X.T,Y.T,Ex,Ey)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('Vector field')
    plt.show()

def sor_method(Phi,rho,h,N,omega):
    # a function for performing the SOR method
    new_Phi=1.0*Phi
    for i in range(1,N-1):
        for j in range(1,N-1):  
            if (i==7 or i==13) and 5<j<15:    
                pass
            else:
                new_Phi[i,j]=(1-omega)*Phi[i,j]+omega/4*(Phi[i+1,j]+new_Phi[i-1,j]+Phi[i,j+1]+new_Phi[i,j-1]+h**2*rho[i,j])
            

    return new_Phi

def main():
    capacitor_plates()

if __name__=="__main__":
    main()