""" computational physics1, exercise5, problem2
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Trajectory of a charged particle influenced by lorentz force
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def runge_kutta4(x,t,dt,func):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)
    The method is based on using functions with free parameters,
     instead of derivatives as in Taylor expansion. The variables are then set to match 
     the ones in Taylor series. The method is lighter to calculate as it only utilizes 
     function evaluations and no derivatives.

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    """
    F1 = F2 = F3 = F4 = 0.0*x

    F1 = dt*func(x,t)
    F2 = dt*func(x+F1/2,t+dt/2)
    F3 = dt*func(x+F2/2,t+dt/2)
    F4 = dt*func(x+F3,t+dt)

    return x+1/6*(F1+2*F2+2*F3+F4), t+dt


def fun(values, t):
    """ Describes the lorentz force acting on a charged particle.
    Differential equations are described as 
    F = ma = m dv/dt, dx/dt=v
    where F = qE+qvxB,
    so the equations are of form
    dv/dt = q/mE+q/mvxB, dx/dt=v

    Args:
        values : values for v and x
        t : current time 
    """     

    v = values[0:3]
    E = np.array([1, 0, 0])
    B = np.array([0, 1, 0])
    qE_m = 0.05*E
    qB_m = 4.0*B

    f0 = qE_m+np.cross(v,qB_m)
    f1 = v

    return np.concatenate((f0,f1))

def main():
    # intial values
    v0 = np.ones(3)*0.1
    x0 = np.zeros(3)

    # time interval
    t0 = 0
    t1 = 5
    t = np.linspace(t0, t1, 101)
    dt = t[1]-t[0]
    sol_x= []
    sol_v = []
    # intialize all to starting point value
    x = 1*np.concatenate((v0,x0))
    # solving value for different values in t
    for i in range(len(t)):
        sol_v.append(x[0:3])
        sol_x.append(x[3:7])
        x, tp = runge_kutta4(x,t[i],dt,fun)
    sol_x=np.array(sol_x)
    sol_v=np.array(sol_v)
    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(sol_x[:,0],sol_x[:,1],sol_x[:,2])

    print("Velocity vector at t=5: {}".format(sol_v[-1]))
    plt.show()

if __name__=="__main__":
    main()