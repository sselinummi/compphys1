""" computational physics1, exercise5, problem3
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Matrix from 1D poisson eq
"""
import numpy as np
from matplotlib.pyplot import *


def matrix(x, n):
    """ Creates matrix for the 1D poisson eq.

    Args:
        x : grid vector
        n : number of grid points

    Returns:
        matrix for poisson eq
    """    
    h = np.abs(x[1]-x[0])
    A = np.zeros([n,n])
    np.fill_diagonal(A,2)
    # above diagonal
    np.fill_diagonal(A[:,1:],-1)
    # belov diagonal
    np.fill_diagonal(A[1:,:],-1)

    return A/h**2

def galerkin(x,a,i):
    """ garlekin method for the basis functions, NOT USED

    Args:
        x : grid points
        a : point to evalate
        i : index of function

    Returns:
        basis function u_i(x)
    """    
    h = x[1]-x[0]
    if  a >=x[i-1] and a <= x[i]:
        return (a - x[i-1])/h
    elif a >=x[i] and a <= x[i+1]:
        return (x[i+1] - a)/h
    else: 
        return 0

def MAE(sol, sola):
    """ Calculates mean absolute error

    Args:
        sol : esitmated solutin
        sola : analytical solutin

    Returns:
        mae
    """    
    return np.sum(np.abs(sol-sola))/len(sol)

def main():
    e0 = 8.8541878182e-12
    phi = lambda x: np.sin(np.pi*x)
    rho = lambda x: np.pi**2*np.sin(np.pi*x)
    N = [5, 11, 21, 40]

    xe = np.linspace(0,1,100)
    phi_e = phi(xe)

    for n in N:
        x = np.linspace(0,1,n)
        b = rho(x)
        b[0] = 0
        b[-1] = 0

        A = matrix(x,n)
        sol = np.linalg.solve(A,b)

        mae = MAE(sol-sol[0],phi(x))
        
        #print(sol-sol[0])
        #print(np.sin(np.pi*x))

        # plotting
        fig = figure()
        plot(x, sol-sol[0], 'b', label='finite difference N={}'.format(n))
        plot(xe, phi_e, 'r--', label='analytical')
        legend(loc='best')
        ylabel('$\phi(x)$')
        xlabel('x')
        title("Mean absolute error: {}".format(mae))
        show()

if __name__=="__main__":
    main()