""" computational physics1, exercise5, problem1
Veera Kaatrasalo, veera.kaatrasalo@tuni.fi
Fourth order Runge-Kutta and some scipy solvers solving ODEs.
"""
import numpy as np
from matplotlib.pyplot import *
from scipy.integrate import odeint
from scipy.integrate import solve_ivp

def runge_kutta4(x,t,dt,func,**kwargs):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)
    The method is based on using functions with free parameters,
     instead of derivatives as in Taylor expansion. The variables are then set to match 
     the ones in Taylor series. The method is lighter to calculate as it only utilizes 
     function evaluations and no derivatives.

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    Need to complete the routine below
    where it reads '...' !!!!!!!
    
    See Computational Physics 1 lecture notes.
    """
    F1 = F2 = F3 = F4 = 0.0*x
    if ('args' in kwargs):
        args = kwargs['args']
        F1 = dt*func(x,t,*args)
        F2 = dt*func(x+F1/2,t+dt/2,*args)
        F3 = dt*func(x+F2/2,t+dt/2,*args)
        F4 = dt*func(x+F3,t+dt,*args)
    else:
        F1 = dt*func(x,t)
        F2 = dt*func(x+F1/2,t+dt/2)
        F3 = dt*func(x+F2/2,t+dt/2)
        F4 = dt*func(x+F3,t+dt)

    return x+1/6*(F1+2*F2+2*F3+F4), t+dt

def pend(y, t, b, c):
    theta, omega = y
    dydt = [omega, -b*omega - c*np.sin(theta)]
    return np.array(dydt)

def pend_ivp(t,y):
    b = 0.25
    c = 5.0
    return pend(y,t,b,c)

def odeint_test(ax):
    """  Numpy's odeint is uses lsoda from the FORTRAN library odepack.
    It's uses stiff (Backward Differentation Formula) and nonstiff methods. Basically uses Jacobian
    matrices to solve the equation. 

    """    
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    sol = odeint(pend, y0, t, args=(b, c))
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    
    return sol

def runge_kutta_test(ax):
    # parameters for test function
    b = 0.25
    c = 5.0    
    y0 = [np.pi - 0.1, 0.0]
    # time interval
    t = np.linspace(0, 10, 101)
    dt = t[1]-t[0]
    sol=[]
    # intialize all to starting point value
    x=1.0*np.array(y0)
    # solving value for different values in t
    for i in range(len(t)):
        sol.append(x)
        x, tp = runge_kutta4(x,t[i],dt,pend,args=(b,c))
    sol=np.array(sol)
    # plotting
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()

    return sol

def solve_ivp_test(ax):
    """ 
    Numpy's solve_ivp uses numerical integration to solve the differential eq.

    """    
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    sol = solve_ivp(pend_ivp, (0,10), y0,t_eval=t)
    ax.plot(sol.t, sol.y[0,:], 'b', label='theta(t)')
    ax.plot(sol.t, sol.y[1,:], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()

    return sol

def main():
    # plotting
    fig=figure()
    ax1=fig.add_subplot(131)
    ax2=fig.add_subplot(132)
    ax3=fig.add_subplot(133)
    sol_ivp = solve_ivp_test(ax1)
    ax1.set_title('solve_ivp')
    sol_ode = odeint_test(ax2)
    ax2.set_title('odeint')
    sol_runge = runge_kutta_test(ax3)
    ax3.set_title('own Runge-Kutta 4')

    print("Max error between runge-kutta and odeint {}".format((np.amax(abs(sol_runge-sol_ode)))))
    show()


if __name__=="__main__":
    main()
