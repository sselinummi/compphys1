"""Program to find the solution of ordinary differetial of pendulum equation using forth order Runge_Kutta"""
#import needed libraries
import numpy as np
from matplotlib.pyplot import *
from scipy.integrate import odeint
from scipy.integrate import solve_ivp

#runge_kutta function for solving differential equations
def runge_kutta4(x,t,dt,func,**kwargs):
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    Need to complete the routine below
    where it reads '...' !!!!!!!
    
    See Computational Physics 1 lecture notes.
    """
    F1 = F2 = F3 = F4 = 0.0*x #initialize the aproximations
    if ('args' in kwargs): #if positive constants present
        args = kwargs['args']
        F1 = func(x,t,*args) #slope at the beginig of the time step
        F2 = func(x+(dt/2)*F1, t+dt/2, *args)#estimate slope at midpoint
        F3 = func(x+(dt/2)*F2, t+dt/2, *args)#another estimate slope at midpoint
        F4 = func(x+dt*F3, t+dt, *args)#estimate slope at endpoint
    else: #else no positive constants given
        F1 = func(x,t)
        F2 = func(x+(dt/2)*F1, t+dt/2)
        F3 = func(x+(dt/2)*F2, t+dt/2)
        F4 = func(x+dt*F3, t+dt)

    return x+(dt/6)*(F1+2*F2+2*F3+F4), t+dt

#pendulum function used in Runge-kutta and odeint  
def pend(y, t, b, c):
    theta, omega = y #angles to convert the function to first order equation with angular velocity omega(t) = theta'(t), where y is vector of [theta,omega]
    dydt = [omega, -b*omega - c*np.sin(theta)]
    return np.array(dydt)

#pendulum function used in Solve_ivp function
def pend_ivp(t,y):
    b = 0.25 #equation constat
    c = 5.0#equation contant
    return pend(y,t,b,c)

#calculating the maximum absolute difference between odeint and runge-kutta functions
def max_abs_dif():
    b = 0.25 #equation const
    c = 5.0 #equation const    
    y0 = [np.pi - 0.1, 0.0] #initial condition of theta and omega
    t = np.linspace(0, 10, 101)#time grid
    dt = t[1]-t[0]#find dt
    sol=[] #initialize solution array
    x=1.0*np.array(y0) #init x
    for i in range(len(t)):#in range of time find solution
        sol.append(x) 
        x, tp = runge_kutta4(x,t[i],dt,pend,args=(b,c)) #call runge-kutta
    sol = np.array(sol) #array of solution
    sol1 = odeint(pend, y0, t, args=(b, c)) #call odeint 
    print("Maximum absolute difference is: ",np.amax(abs(sol1-sol)))#find absolute difference

#odeint from scipy.integrate library solving equations using lsoda from the FORTRAN library odepack 
def odeint_test(ax):
    #equation const
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0] #initial condition of theta and omega
    t = np.linspace(0, 10, 101)#time grid
    sol = odeint(pend, y0, t, args=(b, c))#find solution
    #plot solution
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    
#runge_kutta function test
def runge_kutta_test(ax):
    #equation const
    b = 0.25
    c = 5.0    
    y0 = [np.pi - 0.1, 0.0]#initial condition of theta and omega
    t = np.linspace(0, 10, 101)#time grid
    dt = t[1]-t[0]#find dt
    sol=[] #initialize solution array
    x=1.0*np.array(y0)
    for i in range(len(t)):#in range of time find solution
        sol.append(x)
        x, tp = runge_kutta4(x,t[i],dt,pend,args=(b,c))#call runge_kutta
    sol=np.array(sol) #create an array of solution
    #plot
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()

#solve_ivp from scipy.integrate library solving equations supporting different solvers
def solve_ivp_test(ax):
    #equation const
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0] #initial condition of theta and omega
    t = np.linspace(0, 10, 101)#time grid
    sol = solve_ivp(pend_ivp, (0,10), y0,t_eval=t) #call solve_ivp ot solve the equation
    #plot solution
    ax.plot(sol.t, sol.y[0,:], 'b', label='theta(t)')
    ax.plot(sol.t, sol.y[1,:], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()
    
#main
def main():
    max_abs_dif()#calculate max abs difference and plot the solutions 
    fig=figure()
    ax1=fig.add_subplot(131)
    ax2=fig.add_subplot(132)
    ax3=fig.add_subplot(133)
    solve_ivp_test(ax1)
    ax1.set_title('solve_ivp')
    odeint_test(ax2)
    ax2.set_title('odeint')
    runge_kutta_test(ax3)
    ax3.set_title('own Runge-Kutta 4')
    show()
    

#call main
if __name__=="__main__":
    main()
