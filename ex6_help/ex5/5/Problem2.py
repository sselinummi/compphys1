"""Trajectory of a charged particle influenced by both electric and magnetic field F = q(E + v × B)"""
#import needed libraies
from numpy import *
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

#X - state vector (position, velocity)
#q - charge
#m - mass
#B - magnetic field
#E - electric field
#t - time

#convert the system in the first order equations for x = ([vx,vy,vz,rx,ry,rz]) where dx/dt = f(x,t)
def func(x,t, E, B):
    dxdt = zeros_like(x) #initialize dx/dt
    dxdt[0] = x[3] #dx/dt[0]=vx
    dxdt[1] = x[4] #dx/dt[1]=vy
    dxdt[2] = x[5] #dx/dt[2]=vz
    dxdt[3] = E[0]+(x[4]*B[2]-x[5]*B[1])#dx/dt[3] = Ex + (vx*Bz-vz*By) from vector product v x B
    dxdt[4] = E[1]+(x[5]*B[0]-x[3]*B[2])#dx/dt[4] = Ey + (vz*Bx-vx*Bz) from vector product v x B
    dxdt[5] = E[2]+(x[3]*B[1]-x[4]*B[0])#dx/dt[5] = Ez + (vx*By-vy*Bx) from vector product v x B
    return dxdt #return first order equation

#function plot to plot the solution
def plot(x,y,z):
    
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111, projection='3d')
    ax.text2D(0.05, 0.95, "Charged particle trajectory",color = 'blue', transform=ax.transAxes)
    ax.set_xlabel('x axis', color = 'blue')
    ax.set_ylabel('y axis', color = 'blue')
    ax.set_zlabel('z axis', color = 'blue')
    ax.plot(x,y,z)
    plt.show()
    
def odeint_test():
    #set initial values
    t0 = 0
    v0 = [0.1, 0.1, 0.1]
    r0 = [0.0, 0.0, 0.0]
    x = array([0.0, 0.0, 0.0, 0.1, 0.1, 0.1])
    E = array([0.05, 0,0]) #from qE/m = 0.05
    B = array([0, 4.0, 0]) #from qB/m = 4.0
    t = linspace(t0,5, 100)#grid of time
    dt = t[1]-t[0]
    sol = odeint(func, x, t, args=(E, B)) #call odeint to find the solution

    #extract r from solution
    x = sol[:, 0]
    y = sol[:, 1]
    z = sol[:, 2]
    #print the solution in T=5 and r[0.01, 0.5, 0.1]
    print("The velocity for t=5 in [0.01, 0.5, 0.1] is: ", [sol[-1,3], sol[-1,4], sol[-1,5]])
    #call plot function
    plot(x,y,z)
   
def main():
    odeint_test()

if __name__=="__main__":
    main()
