""" Matrix form of 1D Poisson equation - solving Poisson 1D equation using Az = b"""
#import needed libraries
from numpy import *
import matplotlib.pyplot as plt
from decimal import Decimal

#function to make the matrix used in calculation
def make_matrix(x):
    N = len(x) #num of matrix rows, columns
    h = x[1]-x[0]
    A = zeros((N, N))#init matrix in 0 for N-row, N-column
    off_diag = -ones((N-1,))#init off diagonal in all -1
    A = diag(off_diag, 1) + diag(off_diag, -1)#place off diagonals 
    fill_diagonal(A, 2)#fill the main diagonal with 2
    return A #return A

#PLOT THE ANALYTICAL SOLUTION AND CALCULATED SOLUTION
def plot(x, phi_t, phi_sol): 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('1D Poisson equation')
    ax.plot(x, phi_t, 'o', label = "Finite difference, N=11")
    ax.plot(x, phi_sol,'--', color = 'red', label = "Analytical")
    ax.legend(loc="lower center")
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$phi(x)$')
    plt.show()

#pission function 
def pission(x, N):
    a = 0
    b = 1
    #phi0 and phi1 = 0
    phi0 = ([0]) 
    phi1 = ([0])
    h =(b-a)/(N-1)#mesh spacing in range (0,1)
    A = make_matrix(x[1:N-1])#make matrix of N
    phi = zeros((N-2,1))#init phi to N-2 since phi0 and phi1 = 0
    rho = pi**2*sin(pi*x[1:N-1])*h**2 #equation of rho
    phi = linalg.solve(A, rho.T) #solve dote product of matrix
    phi_t = concatenate((phi0, phi, phi1)) #add phi0 and phi1
    return phi_t

#main function
def main():
    arr = array([5, 11, 21, 41]) #array of points
    print('{0:20} {1:20}'.format( 'Mean absolute value', 'Points')) 
    for N in arr: #for spcific number of points calculate MAE
        x = linspace(0,1,N) # x=([0,1])
        phi_sol = sin(pi*x)#analytic solution
        phi = pission(x, N) #call pission solver
        for i in range(0,N):
            MAE = 0
            MAE = MAE + abs(phi_sol[i] - phi[i])
            MAE = MAE/(N-2)
        print('{0:2.2E}{1:17}'.format(Decimal(MAE), N))
    #find solution for 11 points
    x1 = linspace(0,1,11)
    phi1 = pission(x1, 11)
    phi_sol = sin(pi*x1)
    solver_err = max(abs(phi_sol-phi1))#max absolute error for N=11
    print("Maximum absolute error for N=11 points is: ", solver_err)
    #call plot function
    plot(x1, phi1, phi_sol)
    
#call main
if __name__=="__main__":
    main()
