""" Poisson equation and relaxation methods for solving 2D electric potential"""
#imprt needed libraries
from numpy import *
import matplotlib.pyplot as plt
from decimal import Decimal
from mpl_toolkits.mplot3d import Axes3D

#SOR test function
def gauss_seidel_test(x,y,N,ds):
   #define charge density
   rho0 = 0.0
   rho = zeros((N,N))
   rho[int(round(N/2.0)),int(round(N/2.0))] = rho0
   #set predicted value
   phi = zeros((N,N))
   #set boundry conditions for Φ(x = 0, y) = Φ0, Φ(x > 0, y = Ly ) = Φ(x > 0, y = 0) = Φ(x = Lx , y) = 0, where Φ0 = 1, and Lx = Ly = 1.
   phi[0,0:N] = 1
   phi[1:N-1,0] = 0
   phi[1:N-1,-1] =0
   phi[-1,0:N] = 0
   iterate = 0 #init iteration counter
   eps = 1e-10 #set min error
   error = 1e10#set max error
   phik1 = phi;#init phik1
   while error > eps: 
      for j in range(1,N-1):
         for i in range(1,N-1):
            phi[i,j] = 0.25*(phik1[i+1,j] + phi[i-1,j] +phik1[i,j+1] + phi[i,j-1] + rho[i,j]*ds**2)#update phi while error is larger than min error
            error = error + abs(phi[i,j]-phik1[i,j])
      iterate = iterate+1
      error = error/float(N)
      phik1 = phi
   print("iterations =",iterate)
   
   return phi

def SOR_test(x, y, N, ds):
   omega = 1.8
   #define charge density
   rho0 = 0.0
   rho = zeros((N,N))
   rho[int(round(N/2.0)),int(round(N/2.0))] = rho0
   #set predicted value
   phi = zeros((N,N))
   #set boundry conditions for Φ(x = 0, y) = Φ0, Φ(x > 0, y = Ly ) = Φ(x > 0, y = 0) = Φ(x = Lx , y) = 0, where Φ0 = 1, and Lx = Ly = 1.
   phi[0,0:N] = 1
   phi[1:N-1,0] = 0
   phi[1:N-1,-1] =0
   phi[-1,0:N] = 0
   iterate = 0 #init iteration counter
   eps = 1e-10 #min error
   error = 1e10#max error
   phik1 = phi; #init phik1
   while error > eps:
      for j in range(1,N-1):
         for i in range(1,N-1):
            phi[i,j] = ((1-omega)*phik1[i,j])+((omega/4)*(phik1[i+1,j] + phi[i-1,j] +phik1[i,j+1] + phi[i,j-1] + rho[i,j]*ds**2))#update phi while error is larger than min error
            error = error + abs(phi[i,j]-phik1[i,j])
      iterate = iterate+1
      error = error/float(N)
      phik1 = phi
   print("iterations =",iterate)
   
   return phi

def jacobi_test(x, y,N,ds):
   #define charge density
   rho0 = 0.0
   rho = zeros((N,N))
   rho[int(round(N/2.0)),int(round(N/2.0))] = rho0
   #set predicted value
   phi = zeros((N,N))
   #set boundry conditions for Φ(x = 0, y) = Φ0, Φ(x > 0, y = Ly ) = Φ(x > 0, y = 0) = Φ(x = Lx , y) = 0, where Φ0 = 1, and Lx = Ly = 1.
   phi[0,0:N] = 1
   phi[1:N-1,0] = 0
   phi[1:N-1,-1] =0
   phi[-1,0:N] = 0
   iterate = 0 #init iteration counter
   eps = 1e-10#min err
   error = 1e10#max err
   phik1 = phi;#init phik1
   while error > eps:
      for j in range(1,N-1):
         for i in range(1,N-1):
            phi[i,j] = 0.25*(phik1[i+1,j] + phik1[i-1,j] +phik1[i,j-1] + phik1[i,j+1] + rho[i,j]*ds**2)#update phi while error is larger than min error
            error = error + abs(phi[i,j]-phik1[i,j])
      iterate = iterate+1
      error = error/float(N)
      phik1 = phi
   print("iterations =",iterate)
   
   return phi

def main():
   #define initial values for the solvers
   N = 15
   L = 1.0
   h = L/N
   #define grid x and y
   x = linspace(0,L, N)
   y = linspace(0,L, N)
   X,Y = meshgrid(x, y)
   #call tests
   phi = jacobi_test(x, y,N,h)
   phi1 = SOR_test(x, y, N, h)
   phi2 = gauss_seidel_test(x,y,N,h)
   #plot 3D wireframe
   fig = plt.figure("Jacobi PDE solver")
   fig1  = plt.figure("SOR PDE solver ")
   fig2  = plt.figure("Gauss-Seidel PDE solver")
   ax = fig.add_subplot(111, projection='3d')
   ax.set_xlabel('x axis', color = 'blue')
   ax.set_ylabel('y axis', color = 'blue')
   ax.set_zlabel('z axis', color = 'blue')
   ax.plot_wireframe(X.T,Y.T,phi,rstride=1,cstride=1)
   ax1 = fig1.add_subplot(111, projection='3d')
   ax1.set_xlabel('x axis', color = 'blue')
   ax1.set_ylabel('y axis', color = 'blue')
   ax1.set_zlabel('z axis', color = 'blue')
   ax1.plot_wireframe(X.T,Y.T,phi1,rstride=1,cstride=1)
   ax2 = fig2.add_subplot(111, projection='3d')
   ax2.set_xlabel('x axis', color = 'blue')
   ax2.set_ylabel('y axis', color = 'blue')
   ax2.set_zlabel('z axis', color = 'blue')
   ax2.plot_wireframe(X.T,Y.T,phi2,rstride=1,cstride=1)
   plt.show()

#call main
if __name__=="__main__":
    main()
