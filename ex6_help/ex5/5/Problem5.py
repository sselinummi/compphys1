"""Visualize electric field of two capacitor plates of L=1"""
#import needed libraries
from numpy import *
import matplotlib.pyplot as plt
from decimal import Decimal
from mpl_toolkits.mplot3d import Axes3D

def el_field():
    N = 21 #number of points on the grid
    L = 1 #length of capacitor lines
    h = L/N 
    x = linspace(-1,1, N) #x grid
    dx = x[1]-x[0]#distance of x elem
    y = linspace(-1,1, N) #ygrid
    dy = y[1]-y[0]#distance of y elem
    X, Y = meshgrid(x,y)
    
    Lbox = 2 #lbox width
    #charge density
    rho0 = 0.0
    rho = zeros((N,N))
    rho[int(round(N/2.0)),int(round(N/2.0))] = rho0
    
    phi = zeros((N,N))#init phi to zeros
    #set boundry conditions
    phi[7,5:15] = 1
    phi[13,5:15] = -1
    #set errors
    eps = 1e-15
    error = 1e15
    phik1 = phi
    iterate = 0
    while error > eps:
        for j in range(1,N-1):
            for i in range(1,N-1):
                phi[i,j] = 0.25*(phik1[i+1,j] + phi[i-1,j] +phik1[i,j+1] + phi[i,j-1] + rho[i,j]*h**2) #call Gauss-Seidel method
                error = error + abs(phi[i,j]-phik1[i,j])
        iterate = iterate+1
        error = error/float(N)
        phik1 = phi 
    print("iterations =",iterate)
    #calculate the deltaphi
    dphix = zeros((N, N))
    dphiy = zeros((N, N))
    for i in range(1, N-1):
        for j in range(1, N-1):
                dphix[i,j] = (phi[i,j]-phi[i-1,j])/dx
                dphiy[i,j] = (phi[i,j]-phi[i,j-1])/dy
    #electric field            
    Ex = -dphix
    Ey = -dphiy
    #plot electric field
    fig, ax = plt.subplots(figsize=(7,7))
    #plot the field with pyplot quiver
    n = -2
    color_array = sqrt(((Ex-n)/2)**2 + ((Ey-n)/2)**2)
    ax.set_title('Electric field of the capacitor')
    ax.quiver(X.T,Y.T, Ex,Ey,color_array, alpha=1.0, units = 'xy',linewidth=.05 )
    plt.show()
    
def main():
    el_field()

#call main
if __name__=="__main__":
    main()
