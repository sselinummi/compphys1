import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative
from num_calculus import trapezoid


plt.rcParams['legend.handlelength'] = 2
plt.rcParams['legend.numpoints'] = 1
#plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 12
fig = plt.figure()
# - or, e.g., fig = plt.figure(figsize=(width, height))
# - so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0
# - width and height are given in inches (1 inch is roughly 2.54 cm)
ax = fig.add_subplot(111)
x = np.linspace(0,np.pi/2,100)
f = np.sin(x)
g = np.exp(-x)
# plot and add label if legend desired
ax.plot(x,f,label=r'$f(x)=\sin(x)$')
ax.plot(x,g,'--',label=r'$f(x)=\exp(-x)$')
# include legend (with best location, i.e., loc=0)
ax.legend(loc=0)
# set axes labels and limits
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$f(x)$')
ax.set_xlim(x.min(), x.max())
fig.tight_layout(pad=1)
# save figure as pdf with 200dpi resolution
fig.savefig('testfile.pdf',dpi=200)
plt.show()
