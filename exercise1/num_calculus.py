"""
Numerically estimates the first and the second derivatives of a function
and compares the results with the exact values of those derivatives
"""

# import needed packages, e.g., import numpy as np
import numpy as np

def first_derivative( function, x, h ):
    """
    First derivative with O(h²) error term.

    function : 1-variable input function
    x : scalar
    h : displacement
    """

    return (function(x+h)-function(x-h))/(2*h)
    
def test_first_derivative():
    # testing the function first_derivative


    x = 0.9
    h = 0.01
    
    num_estimate = first_derivative(fun1, x , h)
    exact = derivative_of_fun1(x)

    print('first derivative')
    print(num_estimate)
    print(exact)

def second_derivative( function, x, h ):
    """
    Second derivative with O(h²) error term.

    function : 1-variable input function
    x : scalar
    h : displacement
    """

    return (function(x+h)+function(x-h)-2*function(x))/(h**2)
    
def test_second_derivative():
    # testing the function second_derivative


    x = 0.9
    h = 0.01
    
    num_estimate2 = second_derivative(fun1, x , h)
    exact2 = second_derivative_of_fun1(x)

    print('second derivative')
    print(num_estimate2)
    print(exact2)

    
    

def fun1(x):
    # function which derivative will be calculated
    return 3*x**2

def derivative_of_fun1(x):
    # known derivative of function1
    return 6*x

def second_derivative_of_fun1(x):
    # known derivative of function1
    return 6

def riemann_sum(function, x):
    """
    Estimating the integral of a function using the left-hand Riemann sum 

    function : array of function values at the interval changing points (x)
    x : array, points where the interval changes
    """
    
    deltax = x[1]-x[0]
    N = len(x)
    return deltax*sum(function[:N-1])

def simpson_int(function, x):
    """
    Estimating the integral of a function using the Simpson integration 

    function : array of function values at the interval changing points (x)
    x : array, points where the interval changes
    """
    
    h = x[1]-x[0]
    N = len(x)

    # checking if there is an odd number of intervals / if the last slice needs to be separated
    if (N%2 != 0):
        evenN = N-1
        I = (h/3)*(sum(function[0:evenN-2:2])+4*sum(function[1:evenN-1:2])+sum(function[2:evenN:2]))
        deltaI = h/12*(-function[N-2]+8*function[N-1]+5*function[N])
        return I+deltaI
    else:
        I = (h/3)*(sum(function[0:N-2:2])+4*sum(function[1:N-1:2])+sum(function[2:N:2]))
        return I
        

def trapezoid(function, x):
    """
    Estimating the integral of a function using the Trapezoid rule 

    function : array of function values at the interval changing points (x)
    x : array, points where the interval changes
    """
    
    deltax = x[1]-x[0]
    N = len(x)
    return (deltax/2)*(sum(function[:N-1])+sum(function[1:N]))

def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
    """
    Estimating the integral of a function using Monte Cartlo integration 

    fun : 1-variable input function
    xmin: scalar, starting point of the integration
    xmax : scalar, ending point of the integration
    blocks : scalar, amount of independent measurements
    iters : scalar, iteration rounds
    """
    
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI

def sin_func(x):
    return np.sin(x)

def test_riemann_trapezoid_simpson_monte_carlo():
    # testing the riemann_sum, trapezoid and simpson_int functions
    
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)
    I_R = riemann_sum(f,x)
    I_T = trapezoid(f,x)
    I_S = simpson_int(f,x)
    I_M,dI = monte_carlo_integration(sin_func,0.,np.pi/2,10,100)
    exact = 1
    print('Riemann sum')
    print(I_R)
    print('Trapezoid')
    print(I_T)
    print('Simpson')
    print(I_S)
    print('Monte Carlo')
    print('Integrated value: {0:0.5f} +/- {1:0.5f}'.format(I_M,2*dI))
    print('Exact')
    print(exact)

    
    
def main():
    # main function that calls the tests
    test_first_derivative()
    test_second_derivative()
    test_riemann_trapezoid_simpson_monte_carlo()
    
if __name__=="__main__":
    main()
