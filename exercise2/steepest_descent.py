"""
Calculating the minimum of a given n-variable function using 
the steepest/gradient descent method.
"""


import numpy as np
from ndim_gradient import gradient

def steepest_descent(f,x,a=0.1):
    """
    Steepest/gradient descent function for finding a minimum of a
    function.
    f : N-variable input function
    x : array
    a : float, optional, small number for determining gamma
    """

    x_length=np.sqrt(np.sum([i**2 for i in x]))
    gamma=a/(x_length+1)
    accuracy=1e-4
    x_N=[1.0]*10 # initial guess for the minimum
    condition=False # is the wanted accuracy achieved
    while condition==False:
        old_x=x_N.copy()
        grad=[gamma*i for i in gradient(f,old_x,0.001)]
        x_N=np.array(old_x)-np.array(grad)
        if np.abs(f(x_N)-f(old_x))<accuracy:
            condition=True
    result=x_N
    return result

def test_steepest_descent():
    # testing the numerical value for the minimum
    x=np.linspace(0,10,10)
    y=np.linspace(-10,10,10)
    result=steepest_descent(fun,x)
    print(result)

def fun(x):
    # n-variable function used for testing
    return np.sum([i**2 for i in x])
    
def main():
    # main function that calls the tests
    test_steepest_descent()
    
if __name__=="__main__":
    main()