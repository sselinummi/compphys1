"""
Visualizing and interpolating the experimental data along a path
"""

import numpy as np
import matplotlib.pyplot as plt

from spline_class import spline

def interpolation():
    """
    Visualizing the experimental data, the grid and a desired path.
    Interpolating the data along the desired path using cubic hermite splines in 2D
    and visualizing the interpolated data with the given reference data.
    """

    # reading and reshaping the experimental data and reading the reference data
    xexp = np.loadtxt('x_grid.txt')
    yexp = np.loadtxt('y_grid.txt')
    Fexp = np.loadtxt('exp_data.txt')
    iref = np.loadtxt('ref_interpolated.txt')
    Fexp = Fexp.reshape([len(xexp),len(yexp)])

    # visualizing the experimental data and grid
    X,Y = np.meshgrid(xexp,yexp)
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax1.contourf(X.T,Y.T,Fexp)
    ax1.scatter(X.T,Y.T,color='red')

    # finding smallest and largest y-value in the grid
    smallest_y=0
    largest_y=0
    for i in range(0,len(yexp)):
        if i==0:
            smallest_y=yexp[i]
            largest_y=yexp[i]
        elif yexp[i] < smallest_y:
            smallest_y=yexp[i]
        elif yexp[i] > largest_y:
            largest_y=yexp[i]

    # finding smallest and largest x-value in the grid
    smallest_x=0
    largest_x=0
    for i in range(0,len(xexp)):
        if i==0:
            smallest_x=xexp[i]
            largest_x=xexp[i]
        elif xexp[i] < smallest_x:
            smallest_x=xexp[i]
        elif xexp[i] > largest_x:
            largest_x=xexp[i]

    x=np.linspace(0,largest_x,100)

    # deleting values from the plotting grids if they are outside the experimental data grid
    fun_data=fun(x).copy()
    fun_datax=x.copy()
    del_yvalues=[]
    del_xvalues=[]
    for j in range(0,len(fun_data)):
        if fun_data[j]<smallest_y or fun_data[j]>largest_y:
            del_yvalues.append(fun_data[j])
            del_xvalues.append(fun_datax[j])
    
    for value in del_yvalues:
        fun_data=np.delete(fun_data,np.where(fun_data==value))

    for value in del_xvalues:
        fun_datax=np.delete(fun_datax,np.where(fun_datax==value))

    # forming a new uniformly spaced grid to stay inside the region of the experimental data
    new_x=np.linspace(0,fun_datax[-1],100)
            

    # plotting y=2x² in the experimental data area
    ax1.plot(fun_datax,fun_data,color='blue', linewidth=4)

    
    # interpolation
    ax2 = fig.add_subplot(122)
    spl2d=spline(x=xexp,y=yexp,f=Fexp,dims=2)
    
    f = np.zeros_like(new_x)
    for i in range(len(new_x)):
        f[i]=spl2d.eval2d(new_x[i],fun(new_x[i]))

    # plotting the interpolated values and the reference data
    ax2.plot(new_x,f)
    ax2.plot(new_x,iref,'--')
    ax2.legend(['splined data','ref data'])
    
    plt.show()


def fun(x):
    # path that the values will be estimated along
    return 2*x**2

def main():
    interpolation()

if __name__=="__main__":
    main()