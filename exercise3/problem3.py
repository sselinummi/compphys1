import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse


def make_matrix(x):
    """
    Makes a matrix that derivates x when the matrix-vector product b=Ax is calculated

    Taylor expansion for a function:
    => f(x+h)=f(x)+hf'(x)+(h²/2)*f''(x)+...
    and to other direction:
    => f(x-h)=f(x)-hf'(x)+(h²/2)*f''(x)+...
    Multiplying the second equation by -1 adding that to the first equation we get:
    => f(x+h)-f(x-h)=2hf'(x)+O(h²) where O is an error term
    => f'(x)=(f(x+h)-f(x-h))/2h
    From the last equation we get the -1 and 1 off diagonal values and the 
    division by 2h in the return call.
    
    In the beginning of x we can not go back (x-h) so this time we get:
    => f(x+h)=f(x)+hf'(x)+(h²/2)*f''(x)+...
    and
    => f(x+2h)=f(x)+2hf'(x)+((2h)²/2)*f''(x)+...
    Multiplying the first equation by 4 and substracting the second equation from it:
    => 4f(x+h)-f(x+2h)=3f(x)+2hf'(x)+O(h²)
    => f'(x)=(-3f(x)+4f(x+h)-f(x+2h))/2h
    From this we get the -3,4 and -1 on the first row and the 
    division by 2h in the return call.

    At the end of x we can not go forward (x+h) so this time we get:
    => f(x-h)=f(x)-hf'(x)+(h²/2)*f''(x)+...
    and
    => f(x-2h)=f(x)-2hf'(x)+((2h)²/2)*f''(x)+...
    Multiplying the first equation by 4 and substracting the second equation from it:
    => 4f(x-h)-f(x-2h)=3f(x)-2hf'(x)+O(h²)
    => f'(x)=(3f(x)-4f(x+h)+f(x+2h))/2h
    From this we get the 3,-4 and 1 on the last row and the 
    division by 2h in the return call.

    """
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    A = np.zeros((N,N)) - np.diag(off_diag,-1) + np.diag(off_diag,1)
    A[0,0:3] = [-3.0,4.0,-1.0] 
    A[-1,-3:] = [1.0,-4.0,3.0] 
    return A/h/2

def main():
    N = 50
    grid = np.linspace(0,np.pi,N)
    x = np.sin(grid)
    x2 = np.sin(grid)**2
    x3 = np.cos(grid)
    x4 = np.cos(grid)**2
    
    A = make_matrix(grid)
    
    # calculate here b=Ax as described in the problem setup
    b=np.dot(A,x)
    b2=np.dot(A,x2)
    b3=np.dot(A,x3)
    b4=np.dot(A,x4)

    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)
    ax1.plot(grid,b,grid,x,'--')
    ax2.plot(grid,b2,grid,x2,'--')
    ax3.plot(grid,b3,grid,x3,'--')
    ax4.plot(grid,b4,grid,x4,'--')
    ax1.legend(['b=Ax','sin(x)'])
    ax2.legend(['b=Ax','sin(x)²'])
    ax3.legend(['b=Ax','cos(x)'])
    ax4.legend(['b=Ax','cos(x)²'])

    plt.show()

if __name__=="__main__":
    main()





