"""
Computational physics 1

1. Add code to function 'largest_eig'
- use the power method to obtain the largest eigenvalue and the 
  corresponding eigenvector of the provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use that to validating your 
  power method implementation

Notice: 
  dot(A,x), A.dot(x), A @ x could be helpful for performing 
  matrix operations

"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eig(A,tol=1e-9):
    """
    - Add simple power method code in this function
    - Add appropriate commenting espcially in this comment section

    Calculating the largest eigenvalue and the corresponding eigenvector 
    of matrix A using the power method.

    A : matrix
    tol : float, optional, tolerance for the error of the result
    """
    A = A.toarray()
    
    l=A.shape[0]

    # creating a randomized initial guess vector
    v=np.random.rand(l)
    error=100
    eig_value=100
    eig_value_old=100
    
    # repeating the power method until the wanted accuracy is achieved
    while error>tol:
      v=np.dot(A,v)
      eig_value=max(abs(v))
      v=v/eig_value
      error=abs(eig_value-eig_value_old)
      eig_value_old=eig_value     

    # normalizing the eigenvector
    eig_vector=v/np.linalg.norm(v)
    return eig_value, eig_vector

def main():
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.5),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    
    l,vec=largest_eig(H0)
    
    # comparing the numerical estimate against scipy's eigsh
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)
    
    psi0=evecs[:,0]
    norm_const=simps(abs(psi0)**2,x=grid)
    psi0=psi0/norm_const
    print(norm_const)

    psi0_ = vec*1.0
    norm_const=simps(abs(psi0_)**2,x=grid)
    psi0_=psi0_/norm_const
    print(norm_const)

    textstr = '\n'.join((
    r'$power method=%.4f$' % (l),
    r'$scipy=%.4f$' % (eigs)))

    plt.plot(grid,abs(psi0)**2,label='scipy eig. vector squared')
    plt.plot(grid,abs(psi0_)**2,'r--',label='largest_eig vector squared')
    plt.text(-3,0.4,textstr, fontsize=15)
    plt.legend(loc=0)
    plt.show()


if __name__=="__main__":
    main()
