"""
Interpolating the electron density of a structure along a line using
cubic hermite splines in 3D and visualizing the results
"""

import numpy as np
import matplotlib.pyplot as plt

from read_xsf_example import read_example_xsf_density
from scipy.integrate import simps
from spline_class import spline

def main():
    
    # reading the density and the lattice info from the given file
    filename1 = 'dft_chargedensity1.xsf'
    rho1, lattice1, grid1, shift1 = read_example_xsf_density(filename1)
    
    # a line from r1 to r0 is the line we want to interpolate the 
    # electron density along
    r0=np.array([0.1,0.1,2.8528])
    r1=np.array([4.45,4.45,2.8528])
    
    N=400
    # t is determining how much we move along the line
    t=np.linspace(0,1,N)
    f=np.zeros_like(t)

    # forming uniformly spaced grids based on the lattice information
    # for initializing the spline
    x=np.linspace(0.0, lattice1[0][0], grid1[0])
    y=np.linspace(0.0, lattice1[1][1], grid1[1])
    z=np.linspace(0.0, lattice1[2][2], grid1[2])

    
    spl3d=spline(x=x,y=y,z=z,f=rho1,dims=3) 
    # moving along the line from r0 towards r1 and calculating 
    # the interpolation values for each point
    for i in range(N):
        r = r0 + t[i]*(r1-r0)
        f[i]=spl3d.eval3d(r[0],r[1],r[2])

    # visualizing the result
    plt.figure()
    plt.plot(f)
    plt.show()


if __name__=='__main__':
    main()