"""
Performing Fourier transform for a data which is read from a file.
Visualizing the data, Fourier transform and modified Fourier transform.
"""

import numpy as np
import matplotlib.pyplot as plt

# ADD CODE: read in the data here 
# reading signal data
data = np.loadtxt('signal_data.txt')
# time data from the file
t = data[:,0]
# signal from the file
f = data[:,1]


dt = t[1]-t[0]
N=len(t)

# Fourier coefficients from numpy fft normalized by multiplication of dt
F = np.fft.fft(f)*dt

# frequencies from numpy fftfreq
freq = np.fft.fftfreq(len(F),d=dt)

# inverse Fourier with numpy ifft (normalization removed with division by dt)
iF = np.fft.ifft(F/dt)

# positive frequencies are given as
# freq[:N//2] from above or freq = np.linspace(0, 1.0/dt/2, N//2)

# removing undesired frequencies that are below 40 Hz or above 60 Hz
# by making their Fourier coefficients zeros
F2=F.copy()
for i in range(0, len(freq[:N//2])):
    if freq[i] < 40 or freq[i] > 60:
        F2[i]=0



fig, ax = plt.subplots()
# plot over positive frequencies the Fourier transform
ax.plot(freq[:N//2], np.abs(F[:N//2]), label='Original signal')
ax.plot(freq[:N//2], np.abs(F2[:N//2]),'r--', label='Modified signal')
ax.legend()
ax.set_xlabel(r'$f$ (Hz)')
ax.set_ylabel(r'$F(\omega/2\pi)$')
 
# plot the "signal" and test the inverse transform
fig, ax = plt.subplots()
ax.plot(t, f,t,iF.real,'r--')
ax.set_xlabel(r'$t$ (s)')
ax.set_ylabel(r'$f(t)$')

plt.show()
