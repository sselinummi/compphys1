"""
Numerically solving the 1D Poisson equation using the matrix form of the 
second derivative and comparing the result against the analytical solution.
Numerical and analytical solutions are visualized and the mean absolute error
is calculated.
"""

import numpy as np
import matplotlib.pyplot as plt

def der_matrix(x):
    """
    Forms the second derivative matrix based on the Taylor expansion and 
    its size is decided by the input grid:

    f''(x)=(f(x+h)+f(x-h)-2f(x))/h²

    x : array
    """
    N = len(x)
    h = x[1]-x[0]
    off_diag = np.ones((N-1,))
    diag=-2*np.ones((N,))
    A = np.zeros((N,N)) + np.diag(off_diag,-1) + np.diag(off_diag,1)+ np.diag(diag)
    # modifying the matrix to fulfill the boundary conditions
    A[0,0]=1.0
    A[-1,-1]=1.0
    A[0,1]=0.0
    A[-1,-2]=0.0
    return A/(h**2)

def solve_poisson():
    
    # numerically solves the 1D Poisson equation
    num_points=[5,11,21,40]
    for num_p in  num_points:
        x=np.linspace(0.0,1.0,num_p)
        A=der_matrix(x)
        b=-rho(x)
        z=np.linalg.solve(A,b)
        analytical_z=analytical_sol(x)

        # calculating the mean absolute error for each case
        error_sum=0.0
        for i in range(num_p):
            error_sum+=abs(z[i]-analytical_z[i])
        mae=error_sum/num_p
        print('The mean absolute error with',num_p,'grid points is:',mae)

        # visualizing the numerical and analytical solutions
        plt.figure()
        plt.plot(x,z,'o', label='Finite difference: N='+str(num_p))
        plt.plot(x,analytical_z,'r--', label='Analytical')
        plt.xlabel('x')
        plt.ylabel(r'$\Phi(x)$')
        plt.legend()
        plt.show()

def rho(x):
    # test case for rho
    return np.pi**2*np.sin(np.pi*x)

def analytical_sol(x):
    # analytical solution
    return np.sin(np.pi*x)

def main():
    solve_poisson()

if __name__=="__main__":
    main()