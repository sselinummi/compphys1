"""
Solving the Poisson equation in 2D using Jacobi, Gauss-Seidel, and SOR update
schemes.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def solve_poisson():
    # setting boundary conditions
    L=1.0
    h=0.05
    N=int(L/h+1)
    x=y=np.linspace(0.0,L,N)
    rho=np.zeros((N,N))
    Phi=np.zeros((N,N))

    Phi[0,:]=1.0
    X,Y=np.meshgrid(x,y)

    # visualizing the initial condition
    fig=plt.figure()
    ax=fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(X.T,Y.T,Phi,rstride=1,cstride=1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    
    good_accurary=False
    tolerance=1.0e-4
    i=1
    omega=1.8
    # performing the jacobi, gauss-seidel or SOR method untill
    # wanted accuracy has been achieved
    while not good_accurary:
        old_Phi=1.0*Phi
        #Phi=jacobi(Phi,rho,h,N)
        #Phi=gauss_seidel(Phi,rho,h,N)
        Phi=sor_method(Phi,rho,h,N,omega)
        if np.amax(abs(Phi-old_Phi)) < tolerance:
            good_accurary=True
        i+=1
    
    print('Loop amount:',i)
    
    # visualizing the solution
    fig=plt.figure()
    ax=fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(X.T,Y.T,Phi,rstride=1,cstride=1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    plt.show()


def jacobi(Phi,rho,h,N):
    # a function for performing the jacobi method
    new_Phi=1.0*Phi
    for i in range(1,N-1):
        for j in range(1,N-1):
            new_Phi[i,j]=1/4*(Phi[i+1,j]+Phi[i-1,j]+Phi[i,j+1]+Phi[i,j-1]+h**2*rho[i,j])

    return new_Phi

def gauss_seidel(Phi,rho,h,N):
    # a function for performing the gauss-seidel method
    new_Phi=1.0*Phi
    for i in range(1,N-1):
        for j in range(1,N-1):
            new_Phi[i,j]=1/4*(Phi[i+1,j]+new_Phi[i-1,j]+Phi[i,j+1]+new_Phi[i,j-1]+h**2*rho[i,j])

    return new_Phi

def sor_method(Phi,rho,h,N,omega):
    # a function for performing the SOR method
    new_Phi=1.0*Phi
    for i in range(1,N-1):
        for j in range(1,N-1):
            new_Phi[i,j]=(1-omega)*Phi[i,j]+omega/4*(Phi[i+1,j]+new_Phi[i-1,j]+Phi[i,j+1]+new_Phi[i,j-1]+h**2*rho[i,j])

    return new_Phi

def main():
    solve_poisson()

if __name__=="__main__":
    main()