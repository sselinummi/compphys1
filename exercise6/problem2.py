"""
Solving 1D Poisson equation using the finite element method with the
analytical formulas for the hat function basis (A_ij and b_i)
and comparing the numerical result against the analytical result
"""

import numpy as np
import matplotlib.pyplot as plt

def make_b(x):
    """
    Forms the b vector based on the analytical formula for b_i.

    x : array
    """
    
    N = len(x)
    h = x[1]-x[0]
    b = np.zeros_like(x)
    for i in range(1,N-1):
        b[i]=np.pi/h*(x[i-1]+x[i+1]-2*x[i])*np.cos(np.pi*x[i])+1/h*(2*np.sin(np.pi*x[i])-np.sin(np.pi*x[i-1])-np.sin(np.pi*x[i+1]))
    return b

def make_A(x):
    """
    Forms the A matrix based on the analytical formula for A_ij.
    Matrix elements for A:
    A_ij = 2/h      for i=j
    A_ij = -1/h     for i=j±1
    A_ij = 0        otherwise

    x : array
    """
    N = len(x)
    h = x[1]-x[0]
    off_diag = -1/h*np.ones((N-1,))
    diag=2/h*np.ones((N,))
    A = np.zeros((N,N)) + np.diag(off_diag,-1) + np.diag(off_diag,1) + np.diag(diag)
    # modifying the matrix to fulfill the boundary conditions
    A[0,0]=1.0
    A[-1,-1]=1.0
    A[0,1]=0.0
    A[-1,-2]=0.0
    return A


def solve_poisson():
    
    # numerically solves the 1D Poisson equation
    num_points=[5,11,21,40]
    for num_p in  num_points:
        x=np.linspace(0.0,1.0,num_p)
        A=make_A(x)
        b=make_b(x)
        phi=np.linalg.solve(A,b)
        analytical_z=analytical_sol(x)

        # calculating the mean absolute error for each case
        error_sum=0.0
        for i in range(num_p):
            error_sum+=abs(phi[i]-analytical_z[i])
        mae=error_sum/num_p
        print('The mean absolute error with',num_p,'grid points is:',mae)

        # visualizing the numerical and analytical solutions
        plt.figure()
        plt.plot(x,phi,'o', label='FEM: N='+str(num_p))
        plt.plot(x,analytical_z,'r--', label='Analytical')
        plt.xlabel('x')
        plt.ylabel(r'$\Phi(x)$')
        plt.legend()
        plt.show()

def rho(x):
    # test case for rho
    return np.pi**2*np.sin(np.pi*x)

def analytical_sol(x):
    # analytical solution
    return np.sin(np.pi*x)

def main():
    solve_poisson()

if __name__=="__main__":
    main()