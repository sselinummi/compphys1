"""
Solving 1D Poisson equation using the finite element method. In part a
A is formed with the analytical formula but b is formed by performing
the integrals for each b_i. In part b both A and b are formed by performing
the integrals. Comparing the numerical result against the analytical 
result included.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

def make_b_integral(x):
    """
    Forms the b vector by performing the integral for each b_i.

    x : array
    """

    N = len(x)
    h = x[1]-x[0]
    b = np.zeros_like(x)
    for i in range(1,N-1):
        b[i]=quad(right_hand_side,x[i-1],x[i+1],args=(x[i],h))[0]
    return b

def make_A(x):
    """
    Forms the A matrix based on the analytical formula for A_ij.
    Matrix elements for A:
    A_ij = 2/h      for i=j
    A_ij = -1/h     for i=j±1
    A_ij = 0        otherwise

    x : array
    """

    N = len(x)
    h = x[1]-x[0]
    off_diag = -1/h*np.ones((N-1,))
    diag=2/h*np.ones((N,))
    A = np.zeros((N,N)) + np.diag(off_diag,-1) + np.diag(off_diag,1) + np.diag(diag)
    A[0,0]=1.0
    A[-1,-1]=1.0
    A[0,1]=0.0
    A[-1,-2]=0.0
    return A

def make_A_integral(x):
    """
    Forms the A matrix by performing the integral for each A_ij.

    x : array
    """

    N = len(x)
    h = x[1]-x[0]
    A = np.zeros((N,N))
    for i in range(1,N-1):
        for j in range(1,N-1):
            if (abs(i-j)<2):
                xmin = min([x[max([0,i-2])],x[max([0,j-2])]])
                xmax = max([x[min([len(x)-1,i+2])],x[min([len(x)-1,j+2])]])
                A[i,j]=quad(left_hand_side,xmin,xmax,args=(x[i],x[j],h))[0]
    # modifying the matrix to fulfill the boundary conditions
    A[0,0]=1.0
    A[-1,-1]=1.0
    return A

def ui(x,xi,h):
    # hat function u_i
    if (xi-h)<=x<=xi:
        return (x-(xi-h))/h
    elif xi<=x<=(xi+h):
        return ((xi+h)-x)/h
    else:
        return 0

def ui_der(x,xi,h):
    # derivative of the hat function
    if (xi-h)<=x<=xi:
        return 1/h
    elif xi<=x<=(xi+h):
        return -1/h
    else:
        return 0

def left_hand_side(x,xi,xj,h):
    # integrand for A_ij
    return ui_der(x,xj,h)*ui_der(x,xi,h)

def right_hand_side(x,xi,h):
    # integrand for b_i
    return rho(x)*ui(x,xi,h)


def solve_poisson():
    
    # numerically solves the 1D Poisson equation
    num_points=[5,11,21,40]
    for num_p in  num_points:
        x=np.linspace(0.0,1.0,num_p)
        
        # first A is for part a and second A is for part b
        A=make_A(x)
        #A=make_A_integral(x)
        
        b=make_b_integral(x)
        phi=np.linalg.solve(A,b)
        analytical_z=analytical_sol(x)

        # calculating the mean absolute error for each case
        error_sum=0.0
        for i in range(num_p):
            error_sum+=abs(phi[i]-analytical_z[i])
        mae=error_sum/num_p
        print('The mean absolute error with',num_p,'grid points is:',mae)
        
        # visualizing the numerical and analytical solutions
        plt.figure()
        plt.plot(x,phi,'o', label='FEM: N='+str(num_p))
        plt.plot(x,analytical_z,'r--', label='Analytical')
        plt.xlabel('x')
        plt.ylabel(r'$\Phi(x)$')
        plt.legend()
        plt.show()

def rho(x):
    # test case for rho
    return np.pi**2*np.sin(np.pi*x)

def analytical_sol(x):
    # analytical solution
    return np.sin(np.pi*x)

def main():
    solve_poisson()

if __name__=="__main__":
    main()