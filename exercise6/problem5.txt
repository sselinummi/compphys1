a) Some people were using different libraries than I was using, such as Pandas and Sympy. Also the naming of variables got me thinking that I could also try to name my variables better to describe them to the reader.

b) It has been a few years since I wrote code with Python so it was a good time to refresh my memory and also learn new stuff about the language. It was also nice to get to involve physics in programming since I have not had a chance to do so during the regular programming courses.

I think that the subjects of the course will benefit me in some way or another in the future especially if I will apply for the summer job positions for the University's physics research groups. But I think that it is still useful to know how to code some simple mathematical operations instead of always depending on libraries.

The most challenging part in the course for me has been Python. Sometimes I know what I'm supposed to do during the exercises but I don't know how to implement it using Python or I'm getting stuck on something simple because I don't remember how the syntax works. Beacuse of these reasons, the exercise problems took some time for me to finish but usually I was able to schedule a suitable amount of time for them.

I think that the course had really interesting topics and the implementation was great. Also the computer labs were really useful especially if you got stuck on some problems or didn't know how to start doing a certain problem. For me personally, it would've been nice if the computer labs were a bit earlier during the week so I wouldn't have had to finish some of the problems during the weekend.
